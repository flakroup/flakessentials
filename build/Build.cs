using FEx.Extensions;
using FlakEssentials.Build;
using Nuke.Common;
using Nuke.Common.Execution;
using Nuke.Common.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using static Nuke.Common.IO.PathConstruction;

namespace _build
{
    [UnsetVisualStudioEnvironmentVariables]
    public class Build : FlakBuild
    {
        [Parameter("Packages to publish")]
        public string PackagesToPublish { get; protected set; }

        public string NuGetApiKey => KeyVault.NuGetApiKey;

        public string NuGetApiUrl { get; }
        public HashSet<string> ExcludedPackageNames { get; }

        public Build()
        {
            NuGetApiUrl = "https://api.nuget.org/v3/index.json";
            ExcludedPackageNames = new HashSet<string>(new[] { "FlakEssentials.Tests" });
        }

        public static int Main()
        {
            return Execute<Build>(x => x.Compile);
        }

        protected override void OnBuildInitialized()
        {
            if (IsPublishableTarget)
            {
                GeneratePackageOnBuild = true;
                BuildConfiguration = "Release";
            }

            base.OnBuildInitialized();
            PrepareNuGetProjectsToPublish();
        }

        protected override (Version version, string suffix) GetLatestProjectVersion()
        {
            var propsFile = new FileInfo(Path.Combine(RootDirectory, "Directory.Build.props"));

            if (propsFile.Exists)
            {
                var doc = new XmlDocument();
                doc.Load(propsFile.FullName);

                XmlNode node = doc.DocumentElement?.SelectSingleNode("//Version");

                if (node != null)
                    return ParseVersionString(node.InnerText);
            }

            return (null, null);
        }

        protected override Task PreparePackageAsync()
        {
            return Task.CompletedTask;
        }

        protected override async Task PublishAsync()
        {
            string[] packagesToPublish = PackagesToPublish?.Split(';');

            string buildPath = Path.Combine(Solution.Directory, "build");
            FileInfo[] allNuGets = Solution.AllProjects.SelectMany(x => x.Directory.GlobDirectories($"**/bin/{BuildConfiguration}"))
                .Where(x => !IsDescendantPath(buildPath, x) && x.DirectoryExists())
                .SelectMany(x => x.GlobFiles($"{Solution.Name}.*.nupkg"))
                .OrderBy(x => Path.GetFileNameWithoutExtension(x))
                .Where(x => x.FileExists())
                .Select(x => new FileInfo(x))
                .ToArray();

            FileInfo[] nuGets = await NuGetMgr.GetNuGetsToPublishOnNuGetOrgAsync(allNuGets, ExcludedPackageNames, packagesToPublish);

            if (nuGets.Length > 0)
                await nuGets.RunWithWhenAllAsync(DotNetNuGetPush);
        }

        protected void DotNetNuGetPush(FileInfo file)
        {
            DotNetNuGetPush(NuGetApiUrl, NuGetApiKey, file);
        }
    }
}