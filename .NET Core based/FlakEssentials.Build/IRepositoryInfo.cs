﻿using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.MSBuild;

namespace FlakEssentials.Build;

public interface IRepositoryInfo
{
    string CommitHash { get; }
    string BranchName { get; }
    string Type { get; }
    string Url { get; }

    MSBuildSettings UpdateRepositoryInfo(MSBuildSettings settings);
    DotNetPackSettings UpdateRepositoryInfo(DotNetPackSettings settings);
}