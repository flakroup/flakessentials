﻿using FlakEssentials.Build.AssemblyInfo;
using FlakEssentials.MSBuild;
using JetBrains.Annotations;
using Microsoft.Build.Construction;
using NuGet.Packaging;
using NuGet.Versioning;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace FlakEssentials.Build;

public class BuildProject
{
    public string BuildConfiguration { get; }
    public MSProject MSBuildProject { get; }
    public DirectoryInfo PublishDir { get; }
    public string AppDesignerFolder { get; }
    public string VerFileName { get; }
    public FileInfo AssemblyInfoFile { get; }
    public string Copyright { get; }
    public string Company { get; }
    public string AssemblyVersion { get; }

    public string Description { get; }
    public string Title { get; }
    public string NuspecFile { get; }
    public AbsolutePath Directory => NukeProject.Directory;
    public Solution Solution => NukeProject.Solution;
    public Guid ProjectId => NukeProject.ProjectId;
    public string Name => NukeProject.Name;
    public Guid TypeId => NukeProject.TypeId;
    public AbsolutePath Path => NukeProject.Path;
    public IDictionary<string, string> Configurations => NukeProject.Configurations;
    public bool IsNetCore3 => MSBuildProject.IsNetCore3;
    public IDictionary<string, string> PublishDirs => MSBuildProject.PublishDirs;
    public bool IsAspNetCore => MSBuildProject.IsAspNetCore;
    public bool IsNetCore => MSBuildProject.IsNetCore;

    public SolutionFolder SolutionFolder
    {
        get => NukeProject.SolutionFolder;
        set => NukeProject.SolutionFolder = value;
    }

    public string Author => MSBuildProject.Company;
    public string PackageId => MSBuildProject.PackageId;
    public bool IsPackable => MSBuildProject.IsPackable;
    public string Version => MSBuildProject.Version;

    protected Project NukeProject { get; }

    public BuildProject(Project project, string configuration, bool is64Bit = false)
    {
        NukeProject = project;

        MSBuildProject = new(NukeProject.GetMSBuildProject(configuration),
            SolutionProjectType.KnownToBeMSBuildFormat,
            is64Bit: is64Bit);

        BuildConfiguration = MSBuildProject.Configurations.FirstOrDefault(x => x == configuration)
                             ?? MSBuildProject.Configurations[0];

        PublishDir = new(MSBuildProject.GetProjectOutputPath(BuildConfiguration));
        AppDesignerFolder = GetProperty<string>("AppDesignerFolder");
        VerFileName = GetProperty<string>("VersionFileName") ?? "AssemblyInfo";

        AssemblyInfoFile = new DirectoryInfo(Directory).GetFiles($"{VerFileName}.cs", SearchOption.AllDirectories)
            .SingleOrDefault();

        Copyright = GetProperty<string>("Copyright");
        Company = GetProperty<string>("Company");
        AssemblyVersion = GetProperty<string>("AssemblyVersion");

        if (AssemblyInfoFile is not null)
        {
            Description = CSharpUpdater.GetAssemblyProperty(AssemblyInfoFile.FullName, "AssemblyDescription");
            Title = CSharpUpdater.GetAssemblyProperty(AssemblyInfoFile.FullName, "AssemblyTitle");
        }

        NuspecFile = GetProperty<string>("NuspecFile");
    }

    public static implicit operator string(BuildProject project) => project.Path;

    public override string ToString() => Path;

    public bool Is(ProjectType projectType) => projectType.Guids.Any(x => x.Equals(TypeId));

    [CanBeNull]
    public string GetProperty(string propertyName) => NukeProject.GetProperty(propertyName);

    [CanBeNull]
    public T GetProperty<T>(string propertyName) => NukeProject.GetProperty<T>(propertyName);

    public IEnumerable<string> GetItems(string itemGroupName) => NukeProject.GetItems(itemGroupName);

    public IEnumerable<T> GetItems<T>(string itemGroupName) => NukeProject.GetItems<T>(itemGroupName);

    public IEnumerable<string> GetItemMetadata(string itemGroupName, string metadataName) =>
        NukeProject.GetItemMetadata(itemGroupName, metadataName);

    public IEnumerable<T> GetItemMetadata<T>(string itemGroupName, string metadataName) =>
        NukeProject.GetItemMetadata<T>(itemGroupName, metadataName);

    public IReadOnlyCollection<string> GetTargetFrameworks() => NukeProject.GetTargetFrameworks();

    public string GetOutputType() => NukeProject.GetOutputType();

    public void UpdateAssemblyInfo(string buildVersionString)
    {
        if (AssemblyInfoFile?.Exists == true)
            CSharpUpdater.Execute(AssemblyInfoFile.FullName,
                buildVersionString,
                buildVersionString,
                Copyright,
                Company);
    }

    public void UpdateNuspec(IRepositoryInfo repositoryInfo, NuGetVersion packageVersion)
    {
        if (NuspecFile is not null
            && File.Exists(NuspecFile))
        {
            _ = new NuspecReader(NuspecFile); //checks if file is valid nuspec

            // Load the main and incremental xml files into XDocuments
            var fullFile = XDocument.Load(NuspecFile);

            // For each Person in the incremental file
            foreach (XElement node in fullFile.Descendants("package").Descendants("metadata").First().Descendants())
            {
                switch (node.Name.LocalName)
                {
                    case "id":
                        node.SetValue(PackageId);

                        break;
                    case "title":
                        node.SetValue(Title);

                        break;
                    case "version":
                        node.SetValue(packageVersion);

                        break;
                    case "authors":
                    case "owners":
                        node.SetValue(Author);

                        break;
                    case "description":
                        node.SetValue(Description);

                        break;
                    case "copyright":
                        node.SetValue(Copyright);

                        break;
                    case "repository":
                        foreach (XAttribute attr in node.Attributes())
                        {
                            switch (attr.Name.LocalName)
                            {
                                case "type":
                                    attr.SetValue(repositoryInfo.Type);

                                    break;
                                case "url":
                                    attr.SetValue(repositoryInfo.Url);

                                    break;
                                case "branch":
                                    attr.SetValue(repositoryInfo.BranchName);

                                    break;
                                case "commit":
                                    attr.SetValue(repositoryInfo.CommitHash);

                                    break;
                            }
                        }

                        break;
                }
            }

            // Save the changes to the full file
            fullFile.Save(NuspecFile);
        }
    }

    public void SetPackageVersion(string version) => MSBuildProject.SetPackageVersion(version);
}