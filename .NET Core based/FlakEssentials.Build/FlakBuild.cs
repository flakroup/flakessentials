using FEx.Asyncx.Extensions;
using FEx.AzureStorage;
using FEx.Common.Abstractions.Interfaces;
using FEx.Common.Extensions;
using FEx.Common.Models;
using FEx.DependencyInjection;
using FEx.DI.Abstractions;
using FEx.Extensions;
using FEx.Extensions.Collections.Dictionaries;
using FEx.Json.Extensions;
using FExApps.Shared.Abstractions.Models;
using FlakEssentials.Build.AssemblyInfo;
using FlakEssentials.KeyVault;
using FlakEssentials.NuGetEx;
using Microsoft.Build.Evaluation;
using Microsoft.Build.Locator;
using Newtonsoft.Json;
using NuGet.Versioning;
using Nuke.Common;
using Nuke.Common.Execution;
using Nuke.Common.Git;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.GitVersion;
using Nuke.Common.Tools.MSBuild;
using Nuke.Common.Utilities.Collections;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using static Nuke.Common.IO.PathConstruction;
using static Nuke.Common.Tools.GitVersion.GitVersionTasks;
using static Serilog.Log;
using Project = Nuke.Common.ProjectModel.Project;

namespace FlakEssentials.Build;

[UnsetVisualStudioEnvironmentVariables]
[SuppressMessage("ReSharper", "TemplateIsNotCompileTimeConstantProblem")]
public class FlakBuild : NukeBuild
{
    [Solution(SuppressBuildProjectCheck = true)]
    public readonly Solution Solution;

    [GitRepository] public readonly GitRepository GitRepository;

    [Parameter("Version to be put in DLL files", Name = "BuildVersion")]
    public string BuildVersionString;

    [Parameter("Enables Costura.Fody")] public bool EnableCostura;

    [Parameter("Enables NuGet packages generation")]
    public bool GeneratePackageOnBuild;

    [Parameter("Cleans all build directories before build")]
    public bool ShouldClean;

    [Parameter("Appends repository information to DLLs")]
    public bool AppendRepoInfo;

    [Parameter("Configuration to build")]
    public string BuildConfiguration { get; protected set; }

    public bool Is64Bit { get; protected set; }

    public MSBuildTargetPlatform BuildTargetPlatform { get; protected set; }

    public Version BuildVersion { get; protected set; }

    public string BuildVersionSuffix { get; protected set; }

    [Parameter("Name of build project")]
    public string BuildProjectName { get; protected set; }

    public GitVersion GitVersionResult { get; private set; }
    public IList<Output> GitVersionOut { get; private set; }
    public Task GitVersionTask { get; private set; }
    public BuildProject StartupProject { get; private set; }
    public IRepositoryInfo RepositoryInfo { get; private set; }
    public PackageInfo PublishPackage { get; protected set; }

    public Target Clean =>
        _ => _.OnlyWhenDynamic(() => ShouldClean || InvokedTargets.Contains(Clean))
            .Before(Restore)
            .Executes(CleanWholeSolution);

    public Target Restore =>
        _ => _.DependsOn(Clean)
            .Executes(() =>
            {
                MSBuildTasks.MSBuild(s => s.SetTargetPath(Solution)
                    .SetTargets("Restore")
                    .SetMSBuildVerbosity()
                    .SetConfiguration(BuildConfiguration)
                    .SetMaxCpuCount(Environment.ProcessorCount)
                    .SetMSBuildPlatform(MSBuildPlatform.x64)
                    .SetTargetPlatform(BuildTargetPlatform)
                    .SetNodeReuse(IsLocalBuild)
                    .SetProcessArgumentConfigurator(arguments =>
                    {
                        arguments.Add("/bl /m");

                        if (IsLocalBuild)
                            arguments.Add("/nowarn:NU1701");

                        return arguments;
                    }));

                if (File.Exists(RootDirectory / "msbuild.binlog"))
                    (RootDirectory / "msbuild.binlog").DeleteFile();
            });

    public Target Compile => _ => _.DependsOn(Restore).Executes(() => WaitWithoutThreadLock(CompileAsync));

    public Target PreparePackage =>
        _ => _.DependsOn(Compile).Executes(() => WaitWithoutThreadLock(PreparePackageAsync));

    public Target Publish => _ => _.DependsOn(PreparePackage).Executes(() => WaitWithoutThreadLock(PublishAsync));

    protected static Credentials KeyVault => CredentialsService.Creds;

    protected SemaphoreSlim SyncSemaphore { get; }
    protected SemaphoreSlim OutputSemaphore { get; }
    protected List<Target> UploadableTargets { get; }
    protected List<Target> PublishableTargets { get; }
    protected List<Target> CompilableTargets { get; }
    protected NuGetManager NuGetMgr { get; private set; }
    protected bool IsPublishableTarget => ScheduledTargets.Any(x => PublishableTargets.Contains(x.Factory));
    protected bool IsCompilableTarget => ScheduledTargets.Any(x => CompilableTargets.Contains(x.Factory));
    protected bool IsUploadableTarget => ScheduledTargets.Any(x => UploadableTargets.Contains(x.Factory));

    public FlakBuild()
    {
        SyncSemaphore = new(1, 1);
        OutputSemaphore = new(1, 1);
        BuildTargetPlatform = MSBuildTargetPlatform.MSIL;
        BuildConfiguration = "Debug";

        CompilableTargets =
        [
            Compile,
            PreparePackage,
            Publish
        ];

        PublishableTargets =
        [
            PreparePackage,
            Publish
        ];

        UploadableTargets =
        [
            PreparePackage,
            Publish
        ];

        string msBuildExtensionPath = Environment.GetEnvironmentVariable("MSBuildExtensionsPath");
        string msBuildExePath = Environment.GetEnvironmentVariable("MSBUILD_EXE_PATH");
        string msBuildSdkPath = Environment.GetEnvironmentVariable("MSBuildSDKsPath");

        MSBuildLocator.RegisterDefaults();
        TriggerAssemblyResolution();

        Environment.SetEnvironmentVariable("MSBuildExtensionsPath", msBuildExtensionPath);
        Environment.SetEnvironmentVariable("MSBUILD_EXE_PATH", msBuildExePath);
        Environment.SetEnvironmentVariable("MSBuildSDKsPath", msBuildSdkPath);

        FExServiceProvider.Initialize<BuildContainer, FExStrongInjectServiceProvider>();
    }

    protected static async Task SetPackageBlobMetadataAsync(CloudBlockBlobInfo blob, PackageInfo package)
    {
        blob.Metadata.AddOrUpdateValue("Version", package.Version.ToString);
        await blob.Blob.SetMetadataAsync();
        package.SetUri(blob.Uri.AbsoluteUri.ToUri());
        package.EnsureChecksum(blob.Checksum);
    }

    protected static void DeleteEmptyDirs(string binDir)
    {
        DirectoryInfo[] dirs = [new(binDir)];
        DirectoryInfo[] emptyDirs;

        do
        {
            emptyDirs = dirs.SelectMany(GetEmptyDirs).ToArray();
            emptyDirs.ForEach(x => x.Delete());
            dirs = dirs.SelectMany(x => x.GetDirectories()).ToArray();
        } while (emptyDirs.Length > 0);
    }

    protected static DirectoryInfo[] GetEmptyDirs(DirectoryInfo binDir) =>
        binDir.EnumerateDirectories().Where(x => !x.EnumerateFileSystemInfos().Any()).ToArray();

    protected static void DotNetNuGetPush(string nuGetApiUrl, string nuGetApiKey, FileInfo file)
    {
        try
        {
            Information($"{file.Name}:");

            DotNetTasks.DotNetNuGetPush(s => s.SetSource(nuGetApiUrl)
                .SetApiKey(nuGetApiKey)
                .EnableSkipDuplicate()
                .SetTargetPath(file.FullName));
        }
        catch (Exception ex)
        {
            Error(ex, "");
        }
    }

    protected static void WaitWithoutThreadLock(Func<Task> func) => func.WaitWithoutThreadLock();

    protected static T WaitWithoutThreadLock<T>(Func<Task<T>> func) => func.WaitWithoutThreadLock();

    protected static IList<Output> ProcessOutput(IEnumerable<Output> output)
    {
        var result = new List<Output>();

        foreach (Output o in output)
        {
            if (result.Count == 0)
            {
                result.Add(o);
            }
            else
            {
                Output last = result[^1];

                if (last.Type == o.Type)
                    result[^1] = new()
                    {
                        Text = last.Text + o.Text
                    };
                else
                    result.Add(o);
            }
        }

        return result;
    }

    protected virtual void SetupStartupProject()
    {
        Project projectName = GetProjectName();

        if (projectName is not null)
        {
            StartupProject = new(projectName, BuildConfiguration, Is64Bit);

            if (StartupProject is not null)
            {
                if (StartupProject.Company is null)
                    throw new($"{nameof(StartupProject.Company)} is null");

                if (BuildVersionString is null)
                {
                    if (StartupProject.AssemblyInfoFile?.Exists == true)
                        BuildVersionString = CSharpUpdater.GetAssemblyVersion(StartupProject.AssemblyInfoFile.FullName);

                    BuildVersionString ??= StartupProject.AssemblyVersion;

                    (Version version, string suffix) = ParseVersionString(BuildVersionString);

                    (Version latest, string latestSuffix) = GetLatestProjectVersion();

                    if (latest is not null)
                        BuildVersionString = version is null || latest.FirstIsHigherThanSecond(version)
                            ? GetVersionString(latest, latestSuffix)
                            : GetVersionString(version, suffix);
                }
            }
        }

        if (BuildVersionString is not null)
        {
            (Version version, string suffix) = ParseVersionString(BuildVersionString);
            BuildVersion = version;
            BuildVersionSuffix = suffix;
        }
        else
        {
            (Version version, string suffix) = GetLatestProjectVersion();
            BuildVersion = version;
            BuildVersionSuffix = suffix;
        }

        if (BuildVersion is not null
            && BuildVersionString is null)
            BuildVersionString = GetVersionString(BuildVersion, BuildVersionSuffix);
    }

    protected virtual (Version version, string suffix) ParseVersionString(string buildVersionString)
    {
        if (buildVersionString is null)
            return (null, null);

        if (!buildVersionString.Contains("-"))
            return (Version.Parse(buildVersionString), null);

        var semVer = NuGetVersion.Parse(buildVersionString);

        return (new(semVer.Major, semVer.Minor, semVer.Patch), semVer.Release);
    }

    protected virtual Project GetProjectName() =>
        Solution.AllProjects.FirstOrDefault(x => x.Name == (BuildProjectName ?? Solution.Name));

    protected virtual void BeforeStartupProjectInitialization()
    {
    }

    protected virtual IAppInfo GetApplicationMock() =>
        new AppInfo
        {
            Name = BuildProjectName ?? Solution.Name
        };

    protected virtual (Version version, string suffix) GetLatestProjectVersion() => (null, null);

    protected virtual Task PreparePackageAsync() => Task.CompletedTask;

    protected virtual Task PublishAsync() => Task.CompletedTask;

    protected override void OnBuildInitialized()
    {
        Information("╬════════════════ Using Solution File ════════════════");
        Information($"║\t{Solution.Path}");
        Information("╬═════════════════════════════════════════════════════");

        JsonExtensions.ConfigureDefaultSettings(defaultSettings =>
            defaultSettings.NullValueHandling = NullValueHandling.Ignore);

        string[] availableConfigurations = Solution.Configurations.Keys.Select(x => x.Split('|')[0]).ToArray();

        if (availableConfigurations.All(x => x != BuildConfiguration))
            throw new(
                $"Invalid build configuration: {BuildConfiguration}. Available: {string.Join(';', availableConfigurations)}");

        if (IsPublishableTarget)
        {
            EnableCostura = true;
            ShouldClean = true;
        }

        //GetApplicationMock();

        BeforeStartupProjectInitialization();

        SetupStartupProject();

        if (IsCompilableTarget)
            GitVersionTask = Task.Run(EnsureGitVersion);

        base.OnBuildInitialized();
    }

    protected void PrepareNuGetProjectsToPublish()
    {
        NuGetMgr = FExServiceProvider.Get<NuGetManager>();

        if (BuildVersionString is not null && GeneratePackageOnBuild)
        {
            IList<Project> projectsToUpdate = Solution.AllProjects.Where(HasLowerBuildVersion).ToArray();

            foreach (Project project in projectsToUpdate)
            {
                Information(
                    $"Updating package version:\t{project.Name}\t{projectsToUpdate.IndexOf(project) + 1}/{projectsToUpdate.Count}");

                var msProject = new BuildProject(project, BuildConfiguration);
                msProject.SetPackageVersion(BuildVersionString);
                Information("OK");
            }
        }
    }

    protected void EnsureGitVersion()
    {
        GitVersionSettings settings = new GitVersionSettings().EnableNoCache() //todo catch the whole output to console
            .DisableProcessLogOutput();

        (GitVersion Result, IReadOnlyCollection<Output> Output) gv = GitVersion(settings);
        GitVersionResult = gv.Result;
        RepositoryInfo = new RepositoryInfo(GitRepository, GitVersionResult);
        GitVersionOut = ProcessOutput(gv.Output);
    }

    protected void CleanWholeSolution()
    {
        Information($"Cleanup of {Solution.Name}");
        string buildPath = Path.Combine(Solution.Directory, "build");

        AbsolutePath[] dirsToClean = Solution.AllProjects
            .SelectMany(
                x => x.Directory.GlobDirectories($"**/obj/{BuildConfiguration}", $"**/bin/{BuildConfiguration}"))
            .Where(x => !IsDescendantPath(buildPath, x) && x.DirectoryExists())
            .ToArray();

        dirsToClean.ForEach(x =>
        {
            Directory.Delete(x, true);
            Information($"Deleted\t{x}");
        });

        Information("Cleanup finished");
    }

    protected async Task PrintOutputAsync(IList<Output> output, string prePrint = null)
    {
        await OutputSemaphore.WaitAsync();

        if (prePrint is not null)
            Information(prePrint);

        try
        {
            if (output.Count == 1
                && output[0].Type == OutputType.Std)
                Information(output[0].Text.PrettyPrintJson());
            else
                foreach (Output o in output)
                {
                    if (o.Type == OutputType.Err)
                        Error(o.Text);
                    else
                        Information(o.Text);
                }
        }
        finally
        {
            OutputSemaphore.Release();
        }
    }

    private static void TriggerAssemblyResolution() => _ = new ProjectCollection();

    private static string GetVersionString(Version version, string suffix) =>
        suffix is null
            ? version.ToString()
            : $"{version}-{suffix}";

    private bool HasLowerBuildVersion(Project project)
    {
        var doc = new XmlDocument();
        doc.Load(project.Path);

        XmlNode isPackableNode = doc.DocumentElement?.SelectSingleNode("//IsPackable");

        if (isPackableNode?.InnerText.IsEqual("false") == true)
            return false;

        XmlNode versionNode = doc.DocumentElement?.SelectSingleNode("//Version");

        return versionNode is null || HasLowerBuildVersion(versionNode.InnerText);
    }

    private bool HasLowerBuildVersion(string versionString)
    {
        SemanticVersion version = NuGetVersion.Parse(versionString);

        return version.CompareTo(NuGetVersion.Parse(BuildVersionString)) < 0;
    }

    private async Task CompileAsync()
    {
        StartupProject?.UpdateAssemblyInfo(BuildVersionString);

        MSBuildSettings settings = new MSBuildSettings().SetTargetPath(Solution)
            .SetTargets("Rebuild")
            .SetMSBuildVerbosity()
            .SetConfiguration(BuildConfiguration)
            .SetMaxCpuCount(Environment.ProcessorCount)
            .SetMSBuildPlatform(MSBuildPlatform.x64)
            .SetTargetPlatform(BuildTargetPlatform)
            .SetNodeReuse(IsLocalBuild)
            .SetProcessArgumentConfigurator(arguments =>
            {
                arguments.Add("/bl /m");

                if (IsLocalBuild)
                    arguments.Add("/nowarn:\"NU1701;NETSDK1106;NETSDK1086;CS1591\"");

                return arguments;
            });

        if (BuildVersionString is not null
            && !GeneratePackageOnBuild)
            settings = settings.AddProperty("Version", BuildVersion.ToString())
                .AddProperty("PackageVersion", BuildVersion.ToString())
                .SetAssemblyVersion(BuildVersion.ToString())
                .SetFileVersion(BuildVersion.ToString());

        if (EnableCostura)
            settings = settings.AddProperty("DisableFody", false);

        if (GeneratePackageOnBuild)
            settings = settings.AddProperty("GeneratePackageOnBuild", true).AddProperty("IsPackable", true);

        await GitVersionTask;
        await PrintGitVersionOutputAsync();

        if (AppendRepoInfo && RepositoryInfo is not null)
            settings = settings.UpdateRepositoryInfo(RepositoryInfo);

        //todo set relevant properties in AssemblyInfo
        MSBuildTasks.MSBuild(settings);

        if (File.Exists(RootDirectory / "msbuild.binlog"))
            (RootDirectory / "msbuild.binlog").DeleteFile();

        if (StartupProject is not null && EnableCostura)
            DeleteEmptyDirs(StartupProject.MSBuildProject.OutDirPath);
    }

    private async Task PrintGitVersionOutputAsync() => await PrintOutputAsync(GitVersionOut, "GitVersion:");
}