﻿using FEx.Abstractions.Interfaces;
using FEx.Fundamentals;
using FEx.Fundamentals.Helpers;
using FEx.Json;
using FEx.Logging;
using FEx.Logging.Abstractions.Interfaces;
using FEx.MVVM;
using FEx.MVVM.Abstractions;
using FEx.MVVM.Abstractions.Interfaces;
using FEx.MVVM.Rx;
using FEx.MVVM.Rx.Abstractions.Interfaces;
using StrongInject;

namespace FlakEssentials.Build;

[RegisterModule(typeof(FExLoggingModule))]
[RegisterModule(typeof(FExMvvmRxModule))]
[RegisterModule(typeof(FExMvvmModule))]
[RegisterModule(typeof(FExJsonModule))]
[Register(typeof(DefaultDispatcher), typeof(IFExDispatcher))]
[Register(typeof(DummyPopupService), typeof(IMessagePopupService))]
public partial class BuildContainer : FExDefaultFundamentalsModule, IFExFundamentalsContainer, IFExLoggingContainer,
    IFExMvvmRxContainer, IFExMvvmContainer, IFExJsonContainer
{
}