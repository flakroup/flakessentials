﻿using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.MSBuild;

namespace FlakEssentials.Build;

public static class NukeExtensions
{
    public static MSBuildSettings SetMSBuildVerbosity(this MSBuildSettings toolSettings) =>
        toolSettings.SetVerbosity(MSBuildVerbosity.Minimal);

    public static MSBuildSettings UpdateRepositoryInfo(this MSBuildSettings toolSettings,
                                                       IRepositoryInfo repositoryInfo) =>
        repositoryInfo.UpdateRepositoryInfo(toolSettings);

    public static DotNetPackSettings UpdateRepositoryInfo(this DotNetPackSettings toolSettings,
                                                          IRepositoryInfo repositoryInfo) =>
        repositoryInfo.UpdateRepositoryInfo(toolSettings);
}