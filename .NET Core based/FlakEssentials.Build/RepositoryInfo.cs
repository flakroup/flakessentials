﻿using Nuke.Common.Git;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.GitVersion;
using Nuke.Common.Tools.MSBuild;

namespace FlakEssentials.Build;

public class RepositoryInfo : IRepositoryInfo
{
    public string CommitHash { get; }
    public string BranchName { get; }
    public string Type { get; }
    public string Url { get; }

    public RepositoryInfo(GitRepository repo, GitVersion gitVersion)
    {
        CommitHash = gitVersion.Sha;
        BranchName = repo.Branch;
        Type = "git";
        Url = repo.HttpsUrl;
    }

    public MSBuildSettings UpdateRepositoryInfo(MSBuildSettings settings) =>
        settings.AddProperty("RepositoryCommit", CommitHash)
            .AddProperty("RepositoryBranch", $"\"{BranchName}\"")
            .SetRepositoryType(Type)
            .SetRepositoryUrl(Url);

    public DotNetPackSettings UpdateRepositoryInfo(DotNetPackSettings settings) =>
        settings.AddProperty("CommitHash", CommitHash)
            .AddProperty("RepositoryBranch", BranchName)
            .AddProperty("RepositoryType", Type)
            .AddProperty("RepositoryUrl", Url);
}