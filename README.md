| Build Server | Type         | Status                                                                                                                                                                                 |
|--------------|--------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| VSTS         | Build        | [![VSTS Build Status](https://flakroup.visualstudio.com/FlakEssentials/_apis/build/status/FlakEssentials-GitLab-Pipeline)](https://flakroup.visualstudio.com/FlakEssentials/_build/latest?definitionId=11)|
| VSTS         | Dev Build    | [![Dev VSTS Build Status](https://flakroup.visualstudio.com/FlakEssentials/_apis/build/status/FlakEssentials-Dev-GitLab-Pipeline)](https://flakroup.visualstudio.com/FlakEssentials/_build/latest?definitionId=10)|

