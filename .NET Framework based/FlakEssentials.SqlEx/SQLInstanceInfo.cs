﻿using FEx.Basics.Extensions;
using FEx.Common.Extensions;
using FlakEssentials.SqlEx.Enums;
using FlakEssentials.SqlEx.Extensions;
using Microsoft.Data.SqlClient;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Smo.Wmi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlakEssentials.SqlEx;

public class SQLInstanceInfo
{
    public string SQLInstance { get; }

    /// <summary>
    ///     Version of the Microsoft.NET Framework common language runtime (CLR) that was used while building the instance of
    ///     SQL Server.
    /// </summary>
    public Version BuildClrVersion { get; protected set; }

    /// <summary>
    ///     Name of the default collation for the server.
    /// </summary>
    public string Collation { get; protected set; }

    /// <summary>
    ///     ID of the SQL Server collation.
    /// </summary>
    public int? CollationID { get; protected set; }

    /// <summary>
    ///     Windows comparison style of the collation.
    /// </summary>
    public int? ComparisonStyle { get; protected set; }

    /// <summary>
    ///     NetBIOS name of the local computer on which the instance of SQL Server is currently running.
    /// </summary>
    /// <remarks>
    ///     <para>
    ///         For a clustered instance of SQL Server on a failover cluster, this value changes as the instance of SQL
    ///         Server fails over to other nodes in the failover cluster.
    ///     </para>
    ///     <para>
    ///         On a stand-alone instance of SQL Server, this value remains constant and returns the same value as the
    ///         MachineName property.
    ///     </para>
    ///     <para>
    ///         Note: If the instance of SQL Server is in a failover cluster and you want to obtain the name of the failover
    ///         clustered instance, use the MachineName property.
    ///     </para>
    /// </remarks>
    public string ComputerNamePhysicalNetBIOS { get; protected set; }

    /// <summary>
    ///     Installed product edition of the instance of SQL Server. Use the value of this property to determine the features
    ///     and the limits, such as Compute Capacity Limits by Edition of SQL Server. 64-bit versions of the Database Engine
    ///     append (64-bit) to the version.
    /// </summary>
    public string Edition { get; protected set; }

    /// <summary>
    ///     EditionID represents the installed product edition of the instance of SQL Server. Use the value of this property to
    ///     determine features and limits, such as Compute Capacity Limits by Edition of SQL Server.
    /// </summary>
    public string EditionID { get; protected set; }

    /// <summary>
    ///     Database Engine edition of the instance of SQL Server installed on the server.
    /// </summary>
    public string EngineEdition { get; protected set; }

    /// <summary>
    ///     The configured level of FILESTREAM access.
    /// </summary>
    public FileStreamEffectiveLevel? FilestreamConfiguredLevel { get; protected set; }

    /// <summary>
    ///     The effective level of FILESTREAM access. This value can be different than the FilestreamConfiguredLevel if the
    ///     level has changed and either an instance restart or a computer restart is pending.
    /// </summary>
    public FileStreamEffectiveLevel? FilestreamEffectiveLevel { get; protected set; }

    /// <summary>
    ///     The name of the share used by FILESTREAM.
    /// </summary>
    public string FilestreamShareName { get; protected set; }

    /// <summary>
    ///     Indicates whether the Always On availability groups manager has started.
    /// </summary>
    public HadrManagerStatus? HadrManagerStatus { get; protected set; }

    /// <summary>
    ///     Name of the default path to the instance backup files.
    /// </summary>
    public string InstanceDefaultBackupPath { get; protected set; }

    /// <summary>
    ///     Name of the default path to the instance data files.
    /// </summary>
    public string InstanceDefaultDataPath { get; protected set; }

    /// <summary>
    ///     Name of the default path to the instance log files.
    /// </summary>
    public string InstanceDefaultLogPath { get; protected set; }

    /// <summary>
    ///     Name of the instance to which the user is connected.
    /// </summary>
    public string InstanceName { get; protected set; }

    /// <summary>
    ///     <c>true</c> if the Advanced Analytics feature was installed during setup.
    /// </summary>
    public bool? IsAdvancedAnalyticsInstalled { get; protected set; }

    /// <summary>
    ///     <c>true</c> if is SQL Server Big Data Cluster.
    /// </summary>
    public bool? IsBigDataCluster { get; protected set; }

    /// <summary>
    ///     <c>true</c> if server instance is configured in a failover cluster.
    /// </summary>
    public bool? IsClustered { get; protected set; }

    /// <summary>
    ///     <c>true</c> if  full-text and semantic indexing components are installed on the current instance of SQL Server.
    /// </summary>
    public bool? IsFullTextInstalled { get; protected set; }

    /// <summary>
    ///     <c>true</c> if Always On availability groups is enabled on this server instance.
    /// </summary>
    public bool? IsHadrEnabled { get; protected set; }

    /// <summary>
    ///     <c>true</c> if server is in integrated security mode.
    /// </summary>
    public bool? IsIntegratedSecurityOnly { get; protected set; }

    /// <summary>
    ///     <c>true</c> if server is an instance of SQL Server Express LocalDB.
    /// </summary>
    public bool IsLocalDB { get; protected set; }

    /// <summary>
    ///     <c>true</c> if server instance has the PolyBase feature installed.
    /// </summary>
    public bool? IsPolyBaseInstalled { get; protected set; }

    /// <summary>
    ///     <c>true</c> if server is in single-user mode.
    /// </summary>
    public bool? IsSingleUser { get; protected set; }

    /// <summary>
    ///     <c>true</c> if tempdb has been enabled to use memory-optimized tables for metadata; 0 if tempdb is using regular,
    ///     disk-based tables for metadata.
    /// </summary>
    public bool? IsTempDbMetadataMemoryOptimized { get; protected set; }

    /// <summary>
    ///     <c>true</c> if server supports In-Memory OLTP.
    /// </summary>
    public bool? IsXTPSupported { get; protected set; }

    /// <summary>
    ///     Windows locale identifier (LCID) of the collation.
    /// </summary>
    public int? LCID { get; protected set; }

    /// <summary>
    ///     Unused. License information is not preserved or maintained by the SQL Server product. Always returns DISABLED.
    /// </summary>
    public string LicenseType { get; protected set; }

    /// <summary>
    ///     Windows computer name on which the server instance is running.
    /// </summary>
    public string MachineName { get; protected set; }

    /// <summary>
    ///     Unused. License information is not preserved or maintained by the SQL Server product. Always returns NULL.
    /// </summary>
    public string NumLicenses { get; protected set; }

    /// <summary>
    ///     Process ID of the SQL Server service. ProcessID is useful in identifying which Sqlservr.exe belongs to this
    ///     instance.
    /// </summary>
    public int? ProcessID { get; protected set; }

    /// <summary>
    ///     The build number.
    /// </summary>
    public string ProductBuild { get; protected set; }

    /// <summary>
    ///     Type of build of the current build.
    /// </summary>
    public string ProductBuildType { get; protected set; }

    /// <summary>
    ///     Level of the version of the instance of SQL Server.
    /// </summary>
    public string ProductLevel { get; protected set; }

    /// <summary>
    ///     The major version.
    /// </summary>
    public string ProductMajorVersion { get; protected set; }

    /// <summary>
    ///     The minor version.
    /// </summary>
    public string ProductMinorVersion { get; protected set; }

    /// <summary>
    ///     Update level of the current build. CU indicates a cumulative update.
    /// </summary>
    public string ProductUpdateLevel { get; protected set; }

    /// <summary>
    ///     KB article for that release.
    /// </summary>
    public string ProductUpdateReference { get; protected set; }

    /// <summary>
    ///     Version of the instance of SQL Server
    /// </summary>
    public Version ProductVersion { get; protected set; }

    /// <summary>
    ///     Returns the date and time that the Resource database was last updated.
    /// </summary>
    public DateTime? ResourceLastUpdateDateTime { get; protected set; }

    /// <summary>
    ///     Returns the version Resource database.
    /// </summary>
    public Version ResourceVersion { get; protected set; }

    /// <summary>
    ///     Both the Windows server and instance information associated with a specified instance of SQL Server.
    /// </summary>
    public string ServerName { get; protected set; }

    /// <summary>
    ///     The SQL character set ID from the collation ID.
    /// </summary>
    public short? SqlCharSet { get; protected set; }

    /// <summary>
    ///     The SQL character set name from the collation.
    /// </summary>
    public string SqlCharSetName { get; protected set; }

    /// <summary>
    ///     The SQL sort order ID from the collation
    /// </summary>
    public short? SqlSortOrder { get; protected set; }

    /// <summary>
    ///     The SQL sort order name from the collation.
    /// </summary>
    public string SqlSortOrderName { get; protected set; }

    public ServerLoginMode LoginMode => Server?.LoginMode ?? ServerLoginMode.Unknown;

    protected Server Server { get; set; }

    public SQLInstanceInfo(ServerInstance serverInstance, ManagedComputer comp)
    {
        const string defaultInstance = "MSSQLSERVER";

        SQLInstance = serverInstance.Name == defaultInstance
            ? comp.Name
            : $"{comp.Name}\\{serverInstance.Name}";
    }

    public SQLInstanceInfo(string instanceName)
    {
        SQLInstance = instanceName;
    }

    public async Task<bool> LoadInfoAsync()
    {
        Dictionary<ServerProp, string> props = null;

        try
        {
            var connStr =
                $"Data Source={SQLInstance};Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
#if NETFRAMEWORK
            using var conn = new SqlConnection(connStr);
#else
            await using var conn = new SqlConnection(connStr);
#endif
            await conn.OpenAsync();
            IDictionary<string, object>[] result = await conn.RunSqlAsync(SqlConnectionExtensions.PropsSQL);

            props = result.ToDictionary(
                x => (ServerProp)Enum.Parse(typeof(ServerProp), Convert.ToString(x["propertyname"])),
                x => Convert.ToString(x["propertyvalue"]));
        }
        catch
        {
            //ignored
        }

        try
        {
            Server = new(SQLInstance);
        }
        catch
        {
            //ignored
        }

        if (props is not null)
            ProcessProps(props);

        return true;
    }

    public bool LoadInfo()
    {
        Dictionary<ServerProp, string> props = null;

        try
        {
            using var conn =
                new SqlConnection(
                    $"Data Source={SQLInstance};Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

            conn.Open();
            IDictionary<string, object>[] result = conn.RunSql(SqlConnectionExtensions.PropsSQL);

            props = result.ToDictionary(
                x => (ServerProp)Enum.Parse(typeof(ServerProp), Convert.ToString(x["propertyname"])),
                x => Convert.ToString(x["propertyvalue"]));
        }
        catch
        {
            //ignored
        }

        try
        {
            Server = new(SQLInstance);
        }
        catch
        {
            //ignored
        }

        if (props is not null)
            ProcessProps(props);

        return true;
    }

    private void ProcessProps(IDictionary<ServerProp, string> props)
    {
        try
        {
            SafePropertySet(x => BuildClrVersion = x,
                () => Version.Parse(props.TryGetKeyValue(ServerProp.BuildClrVersion).TrimStart('v', '.')),
                () => Server?.BuildClrVersion);

            SafePropertySet(x => Collation = x,
                () => props.TryGetKeyValue(ServerProp.Collation),
                () => Server?.Collation);

            SafePropertySet(x => CollationID = x,
                () => GetInt(props, ServerProp.CollationID),
                () => Server?.CollationID);

            SafePropertySet(x => ComparisonStyle = x,
                () => GetInt(props, ServerProp.ComparisonStyle),
                () => Server?.ComparisonStyle);

            SafePropertySet(x => ComputerNamePhysicalNetBIOS = x,
                () => props.TryGetKeyValue(ServerProp.ComputerNamePhysicalNetBIOS),
                () => Server?.ComputerNamePhysicalNetBIOS);

            SafePropertySet(x => Edition = x, () => props.TryGetKeyValue(ServerProp.Edition), () => Server?.Edition);
            SafePropertySet(x => EditionID = x, () => GetServerEditionID(GetLong(props, ServerProp.EditionID)));

            SafePropertySet(x => EngineEdition = x,
                () => GetServerEngineEdition(GetInt(props, ServerProp.EngineEdition)),
                () => GetServerEngineEdition((int?)Server?.EngineEdition));

            SafePropertySet(x => FilestreamConfiguredLevel = x,
                () => (FileStreamEffectiveLevel?)GetInt(props, ServerProp.FilestreamConfiguredLevel));

            SafePropertySet(x => FilestreamEffectiveLevel = x,
                () => (FileStreamEffectiveLevel?)GetInt(props, ServerProp.FilestreamEffectiveLevel),
                () => Server?.FilestreamLevel);

            SafePropertySet(x => FilestreamShareName = x,
                () => props.TryGetKeyValue(ServerProp.FilestreamShareName),
                () => Server?.FilestreamShareName);

            SafePropertySet(x => HadrManagerStatus = x,
                () => (HadrManagerStatus?)GetInt(props, ServerProp.HadrManagerStatus),
                () => Server?.HadrManagerStatus);

            SafePropertySet(x => InstanceDefaultBackupPath = x,
                () => props.TryGetKeyValue(ServerProp.InstanceDefaultBackupPath));

            SafePropertySet(x => InstanceDefaultDataPath = x,
                () => props.TryGetKeyValue(ServerProp.InstanceDefaultDataPath));

            SafePropertySet(x => InstanceDefaultLogPath = x,
                () => props.TryGetKeyValue(ServerProp.InstanceDefaultLogPath));

            SafePropertySet(x => InstanceName = x,
                () => props.TryGetKeyValue(ServerProp.InstanceName),
                () => Server?.InstanceName);

            SafePropertySet(x => IsAdvancedAnalyticsInstalled = x,
                () => GetBoolFromInt(props, ServerProp.IsAdvancedAnalyticsInstalled));

            SafePropertySet(x => IsBigDataCluster = x, () => GetBoolFromInt(props, ServerProp.IsBigDataCluster));

            SafePropertySet(x => IsClustered = x,
                () => GetBoolFromInt(props, ServerProp.IsClustered),
                () => Server?.IsClustered);

            SafePropertySet(x => IsFullTextInstalled = x,
                () => GetBoolFromInt(props, ServerProp.IsFullTextInstalled),
                () => Server?.IsFullTextInstalled);

            SafePropertySet(x => IsHadrEnabled = x,
                () => GetBoolFromInt(props, ServerProp.IsHadrEnabled),
                () => Server?.IsHadrEnabled);

            SafePropertySet(x => IsIntegratedSecurityOnly = x,
                () => GetBoolFromInt(props, ServerProp.IsIntegratedSecurityOnly),
                () => Server?.LoginMode == ServerLoginMode.Integrated);

            SafePropertySet(x => IsLocalDB = x ?? false, () => GetBoolFromInt(props, ServerProp.IsLocalDB));

            SafePropertySet(x => IsPolyBaseInstalled = x,
                () => GetBoolFromInt(props, ServerProp.IsPolyBaseInstalled),
                () => Server?.IsPolyBaseInstalled);

            SafePropertySet(x => IsSingleUser = x,
                () => GetBoolFromInt(props, ServerProp.IsSingleUser),
                () => Server?.IsSingleUser);

            SafePropertySet(x => IsTempDbMetadataMemoryOptimized = x,
                () => GetBoolFromInt(props, ServerProp.IsTempDbMetadataMemoryOptimized));

            SafePropertySet(x => IsXTPSupported = x,
                () => GetBoolFromInt(props, ServerProp.IsXTPSupported),
                () => Server?.IsXTPSupported);

            SafePropertySet(x => LCID = x, () => GetInt(props, ServerProp.LCID));
            SafePropertySet(x => LicenseType = x, () => props.TryGetKeyValue(ServerProp.LicenseType));
            SafePropertySet(x => MachineName = x, () => props.TryGetKeyValue(ServerProp.MachineName));
            SafePropertySet(x => NumLicenses = x, () => props.TryGetKeyValue(ServerProp.NumLicenses));
            SafePropertySet(x => ProcessID = x, () => GetInt(props, ServerProp.ProcessID));
            SafePropertySet(x => ProductBuild = x, () => props.TryGetKeyValue(ServerProp.ProductBuild));
            SafePropertySet(x => ProductBuildType = x, () => props.TryGetKeyValue(ServerProp.ProductBuildType));

            SafePropertySet(x => ProductLevel = x,
                () => props.TryGetKeyValue(ServerProp.ProductLevel),
                () => Server?.ProductLevel);

            SafePropertySet(x => ProductMajorVersion = x, () => props.TryGetKeyValue(ServerProp.ProductMajorVersion));
            SafePropertySet(x => ProductMinorVersion = x, () => props.TryGetKeyValue(ServerProp.ProductMinorVersion));

            SafePropertySet(x => ProductUpdateLevel = x,
                () => props.TryGetKeyValue(ServerProp.ProductUpdateLevel),
                () => Server?.ProductUpdateLevel);

            SafePropertySet(x => ProductUpdateReference = x,
                () => props.TryGetKeyValue(ServerProp.ProductUpdateReference));

            SafePropertySet(x => ProductVersion = x, () => GetVersion(props, ServerProp.ProductVersion));

            SafePropertySet(x => ResourceLastUpdateDateTime = x,
                () => GetDateTime(props, ServerProp.ResourceLastUpdateDateTime),
                () => Server?.ResourceLastUpdateDateTime);

            SafePropertySet(x => ResourceVersion = x,
                () => GetVersion(props, ServerProp.ResourceVersion),
                () => Server?.ResourceVersion);

            SafePropertySet(x => ServerName = x, () => props.TryGetKeyValue(ServerProp.ServerName));

            SafePropertySet(x => SqlCharSet = x,
                () => GetShort(props, ServerProp.SqlCharSet),
                () => Server?.SqlCharSet);

            SafePropertySet(x => SqlCharSetName = x,
                () => props.TryGetKeyValue(ServerProp.SqlCharSetName),
                () => Server?.SqlCharSetName);

            SafePropertySet(x => SqlSortOrder = x,
                () => GetShort(props, ServerProp.SqlSortOrder),
                () => Server?.SqlSortOrder);

            SafePropertySet(x => SqlSortOrderName = x,
                () => props.TryGetKeyValue(ServerProp.SqlSortOrderName),
                () => Server?.SqlSortOrderName);
        }
        catch (Exception ex)
        {
            ex.HandleException(false);
        }
    }

    private static void SafePropertySet<T>(Action<T> propSet, Func<T> valueGet, Func<T> serverValueGet = null)
    {
        var failed = false;
        T result = default;

        if (serverValueGet is not null)
            try
            {
                result = serverValueGet();
                propSet(result);
            }
            catch
            {
                failed = true;
                //ignored
            }

        if (serverValueGet is null
            || failed
            || result is null)
            try
            {
                propSet(valueGet());
            }
            catch
            {
                //ignored
            }
    }

    private static string GetServerEngineEdition(int? value) =>
        value.HasValue
            ? SqlConnectionExtensions.ServerEngineEditions.ForwardIndex[value.Value]
            : null;

    private static string GetServerEditionID(long? value) =>
        value.HasValue
            ? SqlConnectionExtensions.ServerEditionIDs.ForwardIndex[value.Value]
            : null;

    private static short? GetShort(IDictionary<ServerProp, string> props, ServerProp sP)
    {
        string stringValue = props.TryGetKeyValue(sP);

        return stringValue.IsNotNullOrEmptyString()
            ? Convert.ToInt16(stringValue)
            : null;
    }

    private static int? GetInt(IDictionary<ServerProp, string> props, ServerProp sP)
    {
        string stringValue = props.TryGetKeyValue(sP);

        return stringValue.IsNotNullOrEmptyString()
            ? Convert.ToInt32(stringValue)
            : null;
    }

    private static long? GetLong(IDictionary<ServerProp, string> props, ServerProp sP)
    {
        string stringValue = props.TryGetKeyValue(sP);

        return stringValue.IsNotNullOrEmptyString()
            ? Convert.ToInt64(stringValue)
            : null;
    }

    private static Version GetVersion(IDictionary<ServerProp, string> props, ServerProp sP)
    {
        string stringValue = props.TryGetKeyValue(sP);

        return stringValue is not null
            ? Version.Parse(stringValue)
            : null;
    }

    private static bool? GetBoolFromInt(IDictionary<ServerProp, string> props, ServerProp sP)
    {
        int? intValue = GetInt(props, sP);

        return intValue.HasValue
            ? Convert.ToBoolean(intValue.Value)
            : null;
    }

    private static DateTime? GetDateTime(IDictionary<ServerProp, string> props, ServerProp sP)
    {
        string stringValue = props.TryGetKeyValue(sP);

        return stringValue is not null
            ? DateTime.Parse(stringValue)
            : null;
    }
}