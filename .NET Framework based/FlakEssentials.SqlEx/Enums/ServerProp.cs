﻿using System.ComponentModel;

namespace FlakEssentials.SqlEx.Enums;

public enum ServerProp
{
    /// <summary>
    ///     Version of the Microsoft.NET Framework common language runtime (CLR) that was used while building the instance of
    ///     SQL Server.
    /// </summary>
    [Description("BuildClrVersion")] BuildClrVersion,

    /// <summary>
    ///     Name of the default collation for the server.
    /// </summary>
    [Description("Collation")] Collation,

    /// <summary>
    ///     ID of the SQL Server collation.
    /// </summary>
    [Description("CollationID")] CollationID,

    /// <summary>
    ///     Windows comparison style of the collation.
    /// </summary>
    [Description("ComparisonStyle")] ComparisonStyle,

    /// <summary>
    ///     NetBIOS name of the local computer on which the instance of SQL Server is currently running.
    /// </summary>
    /// <remarks>
    ///     <para>
    ///         For a clustered instance of SQL Server on a failover cluster, this value changes as the instance of SQL
    ///         Server fails over to other nodes in the failover cluster.
    ///     </para>
    ///     <para>
    ///         On a stand-alone instance of SQL Server, this value remains constant and returns the same value as the
    ///         MachineName property.
    ///     </para>
    ///     <para>
    ///         Note: If the instance of SQL Server is in a failover cluster and you want to obtain the name of the failover
    ///         clustered instance, use the MachineName property.
    ///     </para>
    /// </remarks>
    [Description("ComputerNamePhysicalNetBIOS")]
    ComputerNamePhysicalNetBIOS,

    /// <summary>
    ///     Installed product edition of the instance of SQL Server. Use the value of this property to determine the features
    ///     and the limits, such as Compute Capacity Limits by Edition of SQL Server. 64-bit versions of the Database Engine
    ///     append (64-bit) to the version.
    /// </summary>
    [Description("Edition")] Edition,

    /// <summary>
    ///     EditionID represents the installed product edition of the instance of SQL Server. Use the value of this property to
    ///     determine features and limits, such as Compute Capacity Limits by Edition of SQL Server.
    /// </summary>
    [Description("EditionID")] EditionID,

    /// <summary>
    ///     Database Engine edition of the instance of SQL Server installed on the server.
    /// </summary>
    [Description("EngineEdition")] EngineEdition,

    [Description("FilestreamConfiguredLevel")]
    FilestreamConfiguredLevel,

    [Description("FilestreamEffectiveLevel")]
    FilestreamEffectiveLevel,
    [Description("FilestreamShareName")] FilestreamShareName,
    [Description("HadrManagerStatus")] HadrManagerStatus,

    [Description("InstanceDefaultBackupPath")]
    InstanceDefaultBackupPath,

    [Description("InstanceDefaultDataPath")]
    InstanceDefaultDataPath,

    [Description("InstanceDefaultLogPath")]
    InstanceDefaultLogPath,
    [Description("InstanceName")] InstanceName,

    [Description("IsAdvancedAnalyticsInstalled")]
    IsAdvancedAnalyticsInstalled,
    [Description("IsBigDataCluster")] IsBigDataCluster,
    [Description("IsClustered")] IsClustered,
    [Description("IsFullTextInstalled")] IsFullTextInstalled,
    [Description("IsHadrEnabled")] IsHadrEnabled,

    [Description("IsIntegratedSecurityOnly")]
    IsIntegratedSecurityOnly,
    [Description("IsLocalDB")] IsLocalDB,
    [Description("IsPolyBaseInstalled")] IsPolyBaseInstalled,
    [Description("IsSingleUser")] IsSingleUser,

    [Description("IsTempDbMetadataMemoryOptimized")]
    IsTempDbMetadataMemoryOptimized,
    [Description("IsXTPSupported")] IsXTPSupported,
    [Description("LCID")] LCID,
    [Description("LicenseType")] LicenseType,
    [Description("MachineName")] MachineName,
    [Description("NumLicenses")] NumLicenses,
    [Description("ProcessID")] ProcessID,
    [Description("ProductBuild")] ProductBuild,
    [Description("ProductBuildType")] ProductBuildType,
    [Description("ProductLevel")] ProductLevel,
    [Description("ProductMajorVersion")] ProductMajorVersion,
    [Description("ProductMinorVersion")] ProductMinorVersion,
    [Description("ProductUpdateLevel")] ProductUpdateLevel,

    [Description("ProductUpdateReference")]
    ProductUpdateReference,
    [Description("ProductVersion")] ProductVersion,

    [Description("ResourceLastUpdateDateTime")]
    ResourceLastUpdateDateTime,
    [Description("ResourceVersion")] ResourceVersion,
    [Description("ServerName")] ServerName,
    [Description("SqlCharSet")] SqlCharSet,
    [Description("SqlCharSetName")] SqlCharSetName,
    [Description("SqlSortOrder")] SqlSortOrder,
    [Description("SqlSortOrderName")] SqlSortOrderName
}