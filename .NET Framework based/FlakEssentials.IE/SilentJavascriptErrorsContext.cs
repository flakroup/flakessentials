﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace FlakEssentials.IE;

public class SilentJavascriptErrorsContext : IDisposable
{
    private readonly WebBrowser _webBrowser;
    private bool? _silent;

    private bool _isDisposed;

    public SilentJavascriptErrorsContext(WebBrowser webBrowser)
    {
        _silent = new bool();

        _webBrowser = webBrowser;
        _webBrowser.Loaded += OnWebBrowserLoaded;
        _webBrowser.Navigated += OnWebBrowserNavigated;
    }

    private void OnWebBrowserLoaded(object sender, RoutedEventArgs e)
    {
        if (!_silent.HasValue)
            return;

        SetSilent();
    }

    private void OnWebBrowserNavigated(object sender, NavigationEventArgs e)
    {
        var webBrowser = (WebBrowser)sender;

        _silent ??= WebBrowserBehavior.GetDisableJavascriptErrors(webBrowser);

        if (webBrowser.IsLoaded)
            SetSilent();
    }

    /// <summary>
    ///     Solution by Simon Mourier on StackOverflow
    ///     http://stackoverflow.com/a/6198700/741414
    /// </summary>
    private void SetSilent()
    {
        _webBrowser.Loaded -= OnWebBrowserLoaded;
        _webBrowser.Navigated -= OnWebBrowserNavigated;

        // get an IWebBrowser2 from the document
        if (_webBrowser.Document is not IOleServiceProvider sp)
            return;

        var iidIWebBrowserApp = new Guid("0002DF05-0000-0000-C000-000000000046");
        var iidIWebBrowser2 = new Guid("D30C1661-CDAF-11d0-8A3E-00C04FC9E26E");

        sp.QueryService(ref iidIWebBrowserApp, ref iidIWebBrowser2, out object webBrowser2);

        webBrowser2?.GetType()
            .InvokeMember("Silent", BindingFlags.Instance | BindingFlags.Public | BindingFlags.PutDispProperty,
                null, webBrowser2, [_silent]);
    }

    ~SilentJavascriptErrorsContext()
    {
        Dispose(false);
    }

    #region IDisposable
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (_isDisposed)
            return;

        if (disposing)
            if (_webBrowser is not null)
            {
                _webBrowser.Loaded -= OnWebBrowserLoaded;
                _webBrowser.Navigated -= OnWebBrowserNavigated;
            }

        _isDisposed = true;
    }
    #endregion

    [ComImport]
    [Guid("6D5140C1-7436-11CE-8034-00AA006009FA")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    private interface IOleServiceProvider
    {
        [PreserveSig]
        int QueryService([In] ref Guid guidService,
                         [In] ref Guid riid,
                         [MarshalAs(UnmanagedType.IDispatch)] out object ppvObject);
    }
}