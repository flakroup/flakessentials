﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlakEssentials.IE;

/// <summary>
///     MessageLoopApartment
///     STA thread with message pump for serial execution of tasks
///     by Noseratio - https://stackoverflow.com/a/22262976/1768303
/// </summary>
public class MessageLoopApartment : IDisposable
{
    private Thread _thread; // the STA thread

    private bool _isDisposed;

    public TaskScheduler TaskScheduler { get; private set; }

    /// <summary>MessageLoopApartment constructor</summary>
    [SuppressMessage("Usage", "VSTHRD002:Avoid problematic synchronous waits")]
    public MessageLoopApartment()
    {
        var tcs = new TaskCompletionSource<TaskScheduler>();

        // start an STA thread and gets a task scheduler
        _thread = new(_ =>
        {
            void IdleHandler(object s, EventArgs e)
            {
                // handle Application.Idle just once
                Application.Idle -= IdleHandler;
                // return the task scheduler
                tcs.SetResult(TaskScheduler.FromCurrentSynchronizationContext());
            }

            // handle Application.Idle just once
            // to make sure we're inside the message loop
            // and SynchronizationContext has been correctly installed
            Application.Idle += IdleHandler;
            Application.Run();
        });

        _thread.SetApartmentState(ApartmentState.STA);
        _thread.IsBackground = true;
        _thread.Start();
        TaskScheduler = tcs.Task.Result;
    }

    /// <summary>Task.Factory.StartNew wrappers</summary>
    [SuppressMessage("Usage", "VSTHRD002:Avoid problematic synchronous waits")]
    public void Invoke(Action action)
    {
        Task.Factory.StartNew(action, CancellationToken.None, TaskCreationOptions.None, TaskScheduler).Wait();
    }

    public async Task RunAsync(Action action, CancellationToken token = default)
    {
        await Task.Factory.StartNew(action, token, TaskCreationOptions.None, TaskScheduler);
    }

    public async Task RunAsync(Func<Task> action, CancellationToken token = default)
    {
        await Task.Factory.StartNew(action, token, TaskCreationOptions.None, TaskScheduler).Unwrap();
    }

    public async Task<TResult> RunAsync<TResult>(Func<Task<TResult>> action, CancellationToken token = default) =>
        await Task.Factory.StartNew(action, token, TaskCreationOptions.None, TaskScheduler).Unwrap();

    ~MessageLoopApartment()
    {
        Dispose(false);
    }

    #region IDisposable
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    [SuppressMessage("Usage", "VSTHRD002:Avoid problematic synchronous waits")]
    protected virtual void Dispose(bool disposing)
    {
        if (_isDisposed)
            return;

        if (disposing)
            if (TaskScheduler is not null)
            {
                TaskScheduler taskScheduler = TaskScheduler;
                TaskScheduler = null;

                // execute Application.ExitThread() on the STA thread
                Task.Factory
                    .StartNew(Application.ExitThread, CancellationToken.None, TaskCreationOptions.None, taskScheduler)
                    .GetAwaiter()
                    .GetResult();

                _thread.Join();
                _thread = null;
            }

        _isDisposed = true;
    }
    #endregion
}