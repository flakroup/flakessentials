﻿using FEx.Basics.Extensions;
using FEx.Common.Utilities;
using FEx.MVVM;
using FEx.MVVM.Abstractions.Enums;
using FEx.Platforms;
using FEx.Platforms.Abstractions.Interfaces;
using FlakEssentials.IE.Enums;
using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;

// Configuring the emulation mode of an Internet Explorer WebBrowser control
// http://cyotek.com/blog/configuring-the-emulation-mode-of-an-internet-explorer-webbrowser-control

// Additional Resources:
// http://msdn.microsoft.com/en-us/library/ee330730%28v=vs.85%29.aspx#browser_emulation

namespace FlakEssentials.IE;

/// <summary>
///     Helper methods for working with Internet Explorer browser emulation
/// </summary>
public static class InternetExplorerBrowserEmulation
{
    public static int InternetExplorerMajorVersion { get; set; }
    private static IRegistryService RegistrySrv => FExPlatforms.RegistryService;
    private static bool Is64BitOperatingSystem => PlatformInfoProvider.Is64BitOperatingSystem;

    private static string SvcVersion { get; } = "svcVersion";

    private static string ProgramName { get; } = Path.GetFileName(Environment.GetCommandLineArgs()[0]);

    private static string InternetExplorer86RootKey { get; } = @"SOFTWARE\Microsoft\Internet Explorer";

    private static string InternetExplorer64RootKey { get; } = Is64BitOperatingSystem
        ? @"SOFTWARE\Wow6432Node\Microsoft\Internet Explorer"
        : null;

    private static string Browser86EmulationKey { get; } =
        $@"{InternetExplorer86RootKey}\Main\FeatureControl\FEATURE_BROWSER_EMULATION";

    private static string Browser64EmulationKey { get; } =
        $@"{InternetExplorer64RootKey}\Main\FeatureControl\FEATURE_BROWSER_EMULATION";

    /// <summary>
    ///     Gets the browser emulation version for the application.
    /// </summary>
    /// <returns>The browser emulation version for the application.</returns>
    public static BrowserEmulationVersion GetBrowserEmulationVersion(bool global = false)
    {
        BrowserEmulationVersion version = BrowserEmulationVersion.Default;
        RegistryKey key = null;

        try
        {
            key = global
                ? GetLocalMachineEmulationKey(false)
                : GetCurrentUserEmulationKey(false);

            if (key?.GetValueNames().Contains(ProgramName) == true)
                version = (BrowserEmulationVersion)Convert.ToInt32(key.GetValue(ProgramName, null) ?? 0);
        }
        catch (SecurityException ex)
        {
            ex.HandleException(custom: ("additionalInfo",
                "The user does not have the permissions required to read from the registry key."));
        }
        catch (UnauthorizedAccessException ex)
        {
            ex.HandleException(custom: ("additionalInfo", "The user does not have the necessary registry rights."));
        }
        catch (Exception ex)
        {
            ex.HandleException();
        }
        finally
        {
            key?.Dispose();
        }

        return version;
    }

    /// <summary>
    ///     Gets the major Internet Explorer version
    /// </summary>
    /// <returns>The major digit of the Internet Explorer version</returns>
    public static void EnsureInternetExplorerMajorVersion()
    {
        int? result = null;

        using RegistryKey key = GetLocalMachineRootKey(false);

        if (key.GetValueNames().Contains(SvcVersion))
        {
            object value = key.GetValue(SvcVersion, null);

            if (value is not null)
            {
                var version = new Version(value.ToString());
                result = version.Major;
            }
        }

        if (result.HasValue)
            InternetExplorerMajorVersion = result.Value;
        else
            throw new InvalidOperationException($"svcVersion entry in {key.Name} key is not present");
    }

    /// <summary>
    ///     Determines whether a browser emulation version is set for the application.
    /// </summary>
    /// <returns><c>true</c> if a specific browser emulation version has been set for the application; otherwise, <c>false</c>.</returns>
    public static bool IsBrowserEmulationSet() => GetBrowserEmulationVersion() != BrowserEmulationVersion.Default;

    /// <summary>
    ///     Sets the browser emulation version for the application.
    /// </summary>
    /// <param name="browserEmulationVersion">The browser emulation version to set.</param>
    /// <param name="global">if set to <c>true</c> [global].</param>
    /// <param name="ownerWindow">The owner window.</param>
    /// <returns>
    ///     <c>true</c> the browser emulation version was updated, <c>false</c> otherwise.
    /// </returns>
    public static bool SetBrowserEmulationVersion(BrowserEmulationVersion browserEmulationVersion,
                                                  bool global = false,
                                                  ISupportInitialize ownerWindow = null)
    {
        bool result;
        RegistryKey key = null;

        try
        {
            key = global
                ? GetLocalMachineEmulationKey()
                : GetCurrentUserEmulationKey();

            result = SetKeyValue(browserEmulationVersion, key);
        }
        catch
        {
            result = false;
        }
        finally
        {
            key?.Dispose();
        }

        if (!result
            && FExMvvm.MessagePopupService.ShowMessage(
                $"Application was unable to automatically set current WebBrowser version.{Environment.NewLine}You will be asked to agree for adding needed keys to registry.",
                "Message",
                MessageIcon.Asterisk,
                FExMessageButton.OK,
                ownerWindow)
            == MessageResult.OK)
            SetBrowserEmulationVersionByRegFile(browserEmulationVersion);

        BrowserEmulationVersion currEmulationCode = GetBrowserEmulationVersion(global);
        BrowserEmulationVersion maxVersion = GetInternetExplorerMaxEmulationVersion();

        return currEmulationCode == maxVersion;
    }

    /// <summary>
    ///     Sets the browser emulation version for the application to the highest default mode for the version of Internet
    ///     Explorer installed on the system
    /// </summary>
    /// <returns><c>true</c> the browser emulation version was updated, <c>false</c> otherwise.</returns>
    public static bool SetBrowserEmulationVersion(bool global = false, ISupportInitialize ownerWindow = null)
    {
        try
        {
            BrowserEmulationVersion currEmulationCode = GetBrowserEmulationVersion(global);
            BrowserEmulationVersion maxVersion = GetInternetExplorerMaxEmulationVersion();

            return currEmulationCode == maxVersion || SetBrowserEmulationVersion(maxVersion, false, ownerWindow);
        }
        catch (Exception ex)
        {
            ex.HandleException(false);
        }

        return false;
    }

    public static string UpdateWebBrowser(ISupportInitialize ownerWindow = null)
    {
        try
        {
            EnsureInternetExplorerMajorVersion();

            if (SetBrowserEmulationVersion(false, ownerWindow))
                return $" IE{InternetExplorerMajorVersion}";
        }
        catch (Exception ex)
        {
            ex.HandleException();
        }

        return string.Empty;
    }

    private static RegistryKey GetLocalMachineEmulationKey(bool writable = true) =>
        RegistrySrv.GetLocalMachineSubKey(Is64BitOperatingSystem
                ? Browser64EmulationKey
                : Browser86EmulationKey,
            writable);

    private static RegistryKey GetCurrentUserEmulationKey(bool writable = true) =>
        RegistrySrv.GetCurrentUserSubKey(Is64BitOperatingSystem
                ? Browser64EmulationKey
                : Browser86EmulationKey,
            writable);

    private static RegistryKey GetLocalMachineRootKey(bool writable = true) =>
        RegistrySrv.GetLocalMachineSubKey(Is64BitOperatingSystem
                ? InternetExplorer64RootKey
                : InternetExplorer86RootKey,
            writable);

    // ReSharper disable UnusedMember.Local
    private static RegistryKey GetCurrentUserRootKey(bool writable = true) //todo move to registry service
                                                                           // ReSharper restore UnusedMember.Local
        =>
            RegistrySrv.GetCurrentUserSubKey(Is64BitOperatingSystem
                    ? InternetExplorer64RootKey
                    : InternetExplorer86RootKey,
                writable);

    private static bool SetKeyValue(BrowserEmulationVersion browserEmulationVersion, RegistryKey key)
    {
        if (key is not null)
        {
            if (browserEmulationVersion != BrowserEmulationVersion.Default)
                key.SetValue(ProgramName, (int)browserEmulationVersion, RegistryValueKind.DWord);
            else
                key.DeleteValue(ProgramName, false);

            return true;
        }

        return false;
    }

    private static void SetBrowserEmulationVersionByRegFile(BrowserEmulationVersion browserEmulationVersion)
    {
        string dwordValue = GetDwordValue((int)browserEmulationVersion);
        var content = new StringBuilder();
        content.AppendLine("Windows Registry Editor Version 5.00");
        content.AppendLine("");
        content.AppendLine($@"[HKEY_CURRENT_USER\{Browser64EmulationKey}]");
        content.AppendLine($"\"{ProgramName}\"=dword:{dwordValue}");
        content.AppendLine("");
        content.AppendLine($@"[HKEY_LOCAL_MACHINE\{Browser64EmulationKey}]");
        content.AppendLine($"\"{ProgramName}\"=dword:{dwordValue}");
        content.AppendLine("");
        content.AppendLine($@"[HKEY_CURRENT_USER\{Browser86EmulationKey}]");
        content.AppendLine($"\"{ProgramName}\"=dword:{dwordValue}");
        content.AppendLine("");
        content.AppendLine($@"[HKEY_LOCAL_MACHINE\{Browser86EmulationKey}]");
        content.AppendLine($"\"{ProgramName}\"=dword:{dwordValue}");
        content.AppendLine("");
        content.AppendLine("");
        string regPath = Path.Combine(Path.GetTempPath(), "IE_BrowserEmulation.reg");
        File.WriteAllText(regPath, content.ToString());
        var prc = Process.Start(new ProcessStartInfo("regedit", regPath));
        prc?.WaitForExit();
        prc?.Dispose();
    }

    private static string GetDwordValue(int browserEmulationVersion)
    {
        string hex = browserEmulationVersion.ToString("X").ToLower();

        while (hex.Length < 8)
            hex = "0" + hex;

        return hex;
    }

    private static BrowserEmulationVersion GetInternetExplorerMaxEmulationVersion()
    {
        EnsureInternetExplorerMajorVersion();

        return InternetExplorerMajorVersion >= 11
            ? BrowserEmulationVersion.Version11Edge
            : InternetExplorerMajorVersion switch
            {
                10 => BrowserEmulationVersion.Version10,
                9 => BrowserEmulationVersion.Version9,
                8 => BrowserEmulationVersion.Version8,
                _ => BrowserEmulationVersion.Version7
            };
    }
}