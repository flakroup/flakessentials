﻿namespace FlakEssentials.IE;

public static class IEModule
{
    public static bool Activate() => InternetExplorerBrowserEmulation.SetBrowserEmulationVersion();
}