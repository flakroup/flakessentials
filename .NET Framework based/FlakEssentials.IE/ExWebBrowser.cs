﻿using FEx.Extensions.Helpers;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
#if NETFULL
using HtmlDocument = HtmlAgilityPack.HtmlDocument;
#endif

namespace FlakEssentials.IE;

public class ExWebBrowser : WebBrowser
{
    private bool _navigating;
    private bool _fullyLoaded;
    private TaskCompletionSource<bool> _tcs;

    public ExWebBrowser()
    {
        ScriptErrorsSuppressed = true;
        _navigating = false;
        _fullyLoaded = false;
    }

    public async Task<HtmlDocument> NavigateAsync(string url) //, CancellationToken token)
    {
        _navigating = false;
        _fullyLoaded = false;
        _tcs = new();
        var uri = new Uri(url);
#if NETFULL
            var doc = new HtmlDocument();
#endif
        //using (token.Register(() => { webBrowser.Stop(); tcs.TrySetCanceled(); }, true))
        //{
        Navigate(uri);

        await StaticAsyncHelper.DelayUntilAsync(() => ReadyState != WebBrowserReadyState.Complete || IsBusy,
            Application.DoEvents,
            100);

        await _tcs.Task;
        await StaticAsyncHelper.DelayUntilAsync(() => _navigating || !_fullyLoaded, Application.DoEvents, 100);
        //}

        //token.ThrowIfCancellationRequested();

        //if (isBrowserScriptCompleted is not null)
        //{
        //    // LOOP until the user say script are completed
        //    while (!isBrowserScriptCompleted(webBrowser))
        //    {
        //        // ENSURE we didn't reach the timeout
        //        if (BrowserTimeout.TotalMilliseconds != 0 && clock.ElapsedMilliseconds > BrowserTimeout.TotalMilliseconds)
        //        {
        //            var documentTextError = WebBrowserOuterHtml(webBrowser);
        //            throw new Exception(timeoutError);
        //        }

        //        doEventsMethod.Invoke(null, new object[0]);
        //        Thread.Sleep(_browserDelay);
        //    }
        //}

#if NETFULL
            doc.LoadHtml(WebBrowserOuterHtml());
            return doc;
#endif
        return null;
    }

    protected override void OnDocumentCompleted(WebBrowserDocumentCompletedEventArgs e)
    {
        base.OnDocumentCompleted(e);

        if (!_navigating && _fullyLoaded)
            _tcs.TrySetResult(true);
    }

    protected override void OnNavigating(WebBrowserNavigatingEventArgs e)
    {
        base.OnNavigating(e);
        _navigating = true;
    }

    protected override void OnNavigated(WebBrowserNavigatedEventArgs e)
    {
        base.OnNavigated(e);
        _navigating = false;
    }

    protected override void OnProgressChanged(WebBrowserProgressChangedEventArgs e)
    {
        base.OnProgressChanged(e);
        _fullyLoaded = e.CurrentProgress == e.MaximumProgress;
    }

    internal string WebBrowserOuterHtml() =>
        Document?.GetElementsByTagName("HTML").Cast<HtmlElement>().FirstOrDefault()?.OuterHtml;
}