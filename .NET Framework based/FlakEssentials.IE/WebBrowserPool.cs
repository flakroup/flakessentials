﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
#if NETFULL
using HtmlDocument = HtmlAgilityPack.HtmlDocument;
#endif

namespace FlakEssentials.IE;

/// <summary>
///     WebBrowserPool the pool of WebBrowser objects sharing the same message loop
/// </summary>
public class WebBrowserPool : IDisposable
{
    private readonly SemaphoreSlim _semaphore; // regulate available browsers
    private readonly ConcurrentQueue<ExWebBrowser> _browsers; // the pool of available browsers
    private readonly HashSet<Task> _pendingTasks; // keep track of pending tasks for proper cancellation
    private readonly CancellationTokenSource _cts; // global cancellation (for Dispose)
    private MessageLoopApartment _apartment; // a WinFroms STA thread with message loop

    private bool _isDisposed;

    public WebBrowserPool(int maxParallel, CancellationToken token)
    {
        if (maxParallel < 1)
            throw new ArgumentException("maxParallel");

        _cts = CancellationTokenSource.CreateLinkedTokenSource(token);
        _apartment = new();
        _semaphore = new(maxParallel);
        _browsers = new();
        _pendingTasks = [];

        // init the pool of WebBrowser objects
        _apartment.Invoke(() =>
        {
            while (--maxParallel >= 0)
                _browsers.Enqueue(new());
        });
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

#if NETFULL
        // Navigate to a site and get a snapshot of its DOM HTML
        public async Task<(string, HtmlDocument)> ScrapSiteAsync(string url,
                                                                 int timeout,
                                                                 CancellationToken token = default)
        {
            using var navigationCts = CancellationTokenSource.CreateLinkedTokenSource(token, _cts.Token);
            CancellationToken combinedToken = navigationCts.Token;

            // we have a limited number of WebBrowser objects available, so await the semaphore
            await _semaphore.WaitAsync(combinedToken);
            try
            {
                if (timeout != Timeout.Infinite)
                    navigationCts.CancelAfter(timeout);

                // run the main logic on the STA thread
                HtmlDocument html = await _apartment.RunAsync(async () =>
                {
                    // acquire the 1st available WebBrowser from the pool
                    if (_browsers.TryDequeue(out ExWebBrowser webBrowser))
                        try
                        {
                            Task<HtmlDocument> task = webBrowser.NavigateAsync(url);
                            _pendingTasks.Add(task); // register the pending task
                            try
                            {
                                return await task;
                            }
                            finally
                            {
                                // unregister the completed task
                                _pendingTasks.Remove(task);
                            }
                        }
                        finally
                        {
                            // return the WebBrowser to the pool
                            _browsers.Enqueue(webBrowser);
                        }

                    return default;
                }, combinedToken);
                return (url, html);
            }
            finally
            {
                _semaphore.Release();
            }
        }
#endif

    ~WebBrowserPool()
    {
        Dispose(false);
    }

    #region IDisposable
    [SuppressMessage("Usage", "VSTHRD002:Avoid problematic synchronous waits")]
    protected virtual void Dispose(bool disposing)
    {
        if (_isDisposed)
            return;

        if (disposing)
        {
            if (_apartment is null)
                throw new ObjectDisposedException(GetType().Name);

            // cancel and wait for all pending tasks
            _cts.Cancel();
            Task task = _apartment.RunAsync(() => Task.WhenAll([.. _pendingTasks]));

            try
            {
                task.Wait();
            }
            catch
            {
                if (!task.IsCanceled)
                    throw;
            }

            // dispose of WebBrowser objects
            _ = _apartment.RunAsync(() =>
            {
                while (!_browsers.IsEmpty)
                {
                    _browsers.TryDequeue(out ExWebBrowser webBrowser);
                    webBrowser?.Dispose();
                }
            });

            _apartment.Dispose();
            _apartment = null;
            _cts.Dispose();
            _semaphore?.Dispose();
        }

        _isDisposed = true;
    }
    #endregion
}