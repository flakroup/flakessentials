﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace FlakEssentials.IE;

public static class WebBrowserBehavior
{
    private static readonly Type OwnerType = typeof(WebBrowserBehavior);

    #region BindableSource
    public static readonly DependencyProperty BindableSourceProperty =
        DependencyProperty.RegisterAttached("BindableSource", typeof(string), OwnerType,
            new UIPropertyMetadata(OnBindableSourcePropertyChanged));

    [AttachedPropertyBrowsableForType(typeof(WebBrowser))]
    public static string GetBindableSource(DependencyObject obj) => (string)obj.GetValue(BindableSourceProperty);

    [AttachedPropertyBrowsableForType(typeof(WebBrowser))]
    public static void SetBindableSource(DependencyObject obj, string value)
    {
        obj.SetValue(BindableSourceProperty, value);
    }

    public static void OnBindableSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        if (d is not WebBrowser browser)
            return;

        browser.Source = e.NewValue is not null
            ? new Uri(e.NewValue.ToString())
            : null;
    }
    #endregion

    #region DisableJavascriptErrors
    #region SilentJavascriptErrorsContext (private DP)
    private static readonly DependencyPropertyKey SilentJavascriptErrorsContextKey =
        DependencyProperty.RegisterAttachedReadOnly("SilentJavascriptErrorsContext",
            typeof(SilentJavascriptErrorsContext), OwnerType, new FrameworkPropertyMetadata(null));

    private static void SetSilentJavascriptErrorsContext(DependencyObject depObj, SilentJavascriptErrorsContext value)
    {
        depObj.SetValue(SilentJavascriptErrorsContextKey, value);
    }

    private static SilentJavascriptErrorsContext GetSilentJavascriptErrorsContext(DependencyObject depObj) =>
        (SilentJavascriptErrorsContext)depObj.GetValue(SilentJavascriptErrorsContextKey.DependencyProperty);
    #endregion

    public static readonly DependencyProperty DisableJavascriptErrorsProperty =
        DependencyProperty.RegisterAttached("DisableJavascriptErrors", typeof(bool), OwnerType,
            new FrameworkPropertyMetadata(OnDisableJavascriptErrorsChangedCallback));

    [AttachedPropertyBrowsableForType(typeof(WebBrowser))]
    public static void SetDisableJavascriptErrors(DependencyObject depObj, bool value)
    {
        depObj.SetValue(DisableJavascriptErrorsProperty, value);
    }

    [AttachedPropertyBrowsableForType(typeof(WebBrowser))]
    public static bool GetDisableJavascriptErrors(DependencyObject depObj) =>
        (bool)depObj.GetValue(DisableJavascriptErrorsProperty);

    private static void OnDisableJavascriptErrorsChangedCallback(DependencyObject d,
                                                                 DependencyPropertyChangedEventArgs e)
    {
        if (d is not WebBrowser webBrowser)
            return;

        if (Equals(e.OldValue, e.NewValue))
            return;

        SilentJavascriptErrorsContext context = GetSilentJavascriptErrorsContext(webBrowser);
        context?.Dispose();

        if (e.NewValue is not null)
        {
            context = new(webBrowser);
            SetSilentJavascriptErrorsContext(webBrowser, context);
        }
        else
        {
            SetSilentJavascriptErrorsContext(webBrowser, null);
        }
    }
    #endregion
}