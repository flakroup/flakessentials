﻿using FEx.Extensions.Helpers;
using FEx.WebScraping.Abstractions.Interfaces;
using HtmlAgilityPack;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FlakEssentials.IE;

public class IEHtmlWebHelper : IWebBrowserScraper
{
    public async Task<HtmlDocument> LoadHtmlDocumentAsync(Uri pageLink,
                                                                     Action<HtmlWeb> configWeb = null,
                                                                     Func<string, bool> isBrowserScriptCompleted = null)
    {
        return await LoadAsync(pageLink, (_, _, doc) => doc, configWeb, isBrowserScriptCompleted);
    }

    public async Task LoadAsync(Uri pageLink,
                                               Action<Uri, HtmlWeb, HtmlDocument> action,
                                               Action<HtmlWeb> configWeb = null,
                                               Func<string, bool> isBrowserScriptCompleted = null)
    {
        await LoadAsync(pageLink,
            (url, web, doc) =>
            {
                action(url, web, doc);

                return true;
            },
            configWeb,
            isBrowserScriptCompleted);
    }

    public async Task<T> LoadAsync<T>(Uri pageLink,
                                                     Func<Uri, HtmlWeb, HtmlDocument, T> action,
                                                     Action<HtmlWeb> configWeb = null,
                                                     Func<string, bool> isBrowserScriptCompleted = null)
    {
        var tcs = new TaskCompletionSource<T>();

        StaticAsyncHelper.RunAsThread(
            () => tcs.SetResult(Load(pageLink, action, configWeb, isBrowserScriptCompleted)),
            ApartmentState.STA);

        return await tcs.Task;
    }

    public T Load<T>(Uri pageLink,
                                Func<Uri, HtmlWeb, HtmlDocument, T> action,
                                Action<HtmlWeb> configWeb = null,
                                Func<string, bool> isBrowserScriptCompleted = null)
    {
        (HtmlWeb web, HtmlDocument doc) = Load(pageLink, configWeb, isBrowserScriptCompleted);

        return action(web.ResponseUri, web, doc);
    }

    public (HtmlWeb web, HtmlDocument doc) Load(Uri pageLink,
                                                           Action<HtmlWeb> configWeb = null,
                                                           Func<string, bool> isBrowserScriptCompleted = null)
    {
        var web = new HtmlWeb();
        configWeb?.Invoke(web);
#if NETFULL
        return (web, web.LoadFromBrowser(pageLink.AbsoluteUri, isBrowserScriptCompleted));
#else
        return (web, web.Load(pageLink.AbsoluteUri));
#endif
    }
}