﻿namespace FlakEssentials.IE.Enums;

/// <summary>
///     Internet Explorer browser emulation versions
/// </summary>
public enum BrowserEmulationVersion
{
    /// <summary>
    ///     Default
    /// </summary>
    Default = 0,

    /// <summary>
    ///     Internet Explorer 7 Standards Mode
    /// </summary>
    Version7 = 7000,

    /// <summary>
    ///     Internet Explorer 8
    /// </summary>
    Version8 = 8000,

    /// <summary>
    ///     Internet Explorer 8 Standards Mode
    /// </summary>
    Version8Standards = 8888,

    /// <summary>
    ///     Internet Explorer 9
    /// </summary>
    Version9 = 9000,

    /// <summary>
    ///     Internet Explorer 9 Standards Mode
    /// </summary>
    Version9Standards = 9999,

    /// <summary>
    ///     Internet Explorer 10
    /// </summary>
    Version10 = 10000,

    /// <summary>
    ///     Internet Explorer 10 Standards Mode
    /// </summary>
    Version10Standards = 10001,

    /// <summary>
    ///     Internet Explorer 11
    /// </summary>
    Version11 = 11000,

    /// <summary>
    ///     Internet Explorer 11 Edge Mode
    /// </summary>
    Version11Edge = 11001
}