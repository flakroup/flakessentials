﻿using RestSharp;

namespace FlakEssentials.TfsEx
{
    public class FlakRestRequest : RestRequest
    {
        public FlakRestRequest()
            : this(Method.GET)
        {
        }

        public FlakRestRequest(Method method)
            : base(method)
        {
            OnBeforeDeserialization = resp => { resp.ContentType = FuncProcessor.ExpectedContentType; };
        }
    }
}