﻿using FEx.Extensions;
using FEx.Extensions.Collections.Enumerables;
using FEx.Extensions.Collections.Lists;
using FEx.Json;
using FlakEssentials.Rest.Controls;
using FlakEssentials.TfsEx.Responses;
using FlakEssentials.TfsEx.Services;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace FlakEssentials.TfsEx
{
    public class FuncProcessor
    {
        public static ConcurrentDictionary<string, SemaphoreSlim> RateLimits { get; } = new ConcurrentDictionary<string, SemaphoreSlim>();
        public static string ExpectedContentType { get; } = "application/json";

        public FlakRestRequest RestRequest { get; }

        //private SemaphoreSlim ConnectionsSemaphore { get; }
        public CancellationTokenSource CancellationTokenSource { get; }
        private string RequestUrl { get; }
        private IDictionary<string, object> Args { get; }
        private CancellationToken CancellationToken { get; }
        private ICredentials Credentials { get; }

        public FuncProcessor(string serverUrl, string scriptPath, ICredentials credentials, string environmentId, IDictionary<string, object> args = null, Method method = Method.GET)
            : this(serverUrl + scriptPath, credentials, environmentId, args, method)
        {
        }

        public FuncProcessor(string serverUrl, string scriptPath, ICredentials credentials, IDictionary<string, object> args = null, Method method = Method.GET)
            : this(serverUrl + scriptPath, credentials, TfsService.GetEnvironmentId(serverUrl, credentials), args, method)
        {
        }

        public FuncProcessor(string requestUrl, ICredentials credentials, IDictionary<string, object> args = null, Method method = Method.GET)
            : this(requestUrl, credentials, TfsService.GetEnvironmentId(requestUrl, credentials), args, method)
        {
        }

        public FuncProcessor(string requestUrl, ICredentials credentials, string environmentId, IDictionary<string, object> args = null, Method method = Method.GET)
        {
            if (requestUrl.IsNullOrWhiteSpace()
                || environmentId.IsNullOrWhiteSpace())
                throw new ArgumentNullException(requestUrl.IsNullOrWhiteSpace()
                    ? nameof(requestUrl)
                    : nameof(environmentId));

            RequestUrl = requestUrl;
            Args = args;
            RestRequest = new FlakRestRequest(method);
            //ConnectionsSemaphore = GetConnectionsSemaphore(environmentId);
            AddParametersFromObjectProperties(Args, RestRequest);
            RestRequest.AddParameter("User-Agent", "VSTSAuthSample-AuthenticateADALNonInteractive", ParameterType.HttpHeader);
            RestRequest.AddParameter("X-TFS-FedAuthRedirect", "Suppress", ParameterType.HttpHeader);
            CancellationTokenSource = new CancellationTokenSource();
            CancellationToken = CancellationTokenSource.Token;
            Credentials = credentials;
        }

        public static TResponse ProcessResponse<TResponse>(string response, JsonSerializerSettings settings = null) where TResponse : BaseTfsResponse, new()
        {
            return response.FromJson<TResponse>(settings);
        }

        private static FlakRestClient GetClient(Uri address, ICredentials credentials)
        {
            return new FlakRestClient(address) { Authenticator = new NtlmAuthenticator(credentials) };
            //client.Proxy = client.BaseUrl.GetDefaultWebProxy();
        }

        private static FlakRestClient GetClient(string address, ICredentials credentials)
        {
            return GetClient(new Uri(address), credentials);
        }

        //public static SemaphoreSlim GetConnectionsSemaphore(string environmentId, int max = 100)
        //{
        //    return RateLimits.GetOrAdd(environmentId, x => new SemaphoreSlim(max, max));
        //}

        public async Task<TResponse> RunAsync<TResponse>(JsonSerializerSettings settings = null, IList<HttpStatusCode> ommitCodes = null) where TResponse : BaseTfsResponse, new()
        {
            string res = await RunRawAsync(ommitCodes);
            return res != null
                ? ProcessResponse<TResponse>(res, settings)
                : default;
        }

        public async Task<string> RunRawAsync(IList<HttpStatusCode> ommitCodes = null)
        {
            //await ConnectionsSemaphore.WaitAsync(CancellationToken);
            IRestClient client = GetClient(RequestUrl, Credentials);
            IRestResponse response = null;
            while (response == null)
            {
#pragma warning disable 618
                response = await client.ExecuteTaskAsync(RestRequest, CancellationToken);
#pragma warning restore 618
                if (response != null
                    && (ommitCodes.IsNullOrEmptyList() || !ommitCodes.Contains(response.StatusCode)))
                {
                    if (response.ContentType == null
                        || response.ContentType.Split(';')
                            .FindInEnumerable()
                        == ExpectedContentType)
                        return response.Content;

                    throw new ArgumentException($"Unexpected response type. {response.ContentType} instead of {ExpectedContentType}. Response was of {response.StatusCode} status.");
                }
            }

            return null;
            //ConnectionsSemaphore.Release();
        }

        private void AddParametersFromObjectProperties(IDictionary<string, object> src, IRestRequest req)
        {
            if (src != null)
                foreach (KeyValuePair<string, object> prop in src)
                    req.AddParameter(prop.Key, prop.Value);
        }
    }
}