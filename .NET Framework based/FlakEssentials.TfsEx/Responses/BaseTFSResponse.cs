﻿using Newtonsoft.Json;

namespace FlakEssentials.TfsEx.Responses
{
    public class BaseTfsResponse
    {
        [JsonProperty("count")]
        public long Count { get; set; }
    }
}