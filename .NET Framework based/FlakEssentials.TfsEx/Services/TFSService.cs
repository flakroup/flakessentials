﻿using FEx.Extensions.Collections.Enumerables;
using FlakEssentials.Extensions.Collections.Concurrent;
using FlakEssentials.MVVM.Abstractions;
using FlakEssentials.MVVM.ViewModels;
using FlakEssentials.TfsEx.Responses;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace FlakEssentials.TfsEx.Services
{
    public sealed class TfsService : ThreadingAwareViewModel
    {
        private TfsEnvironment _selectedEnvironment;

        public static string ImagesCacheDirPath { get; set; }

        public EventHandler<EventArgs> EnvironmentSet { get; set; }
        public EventHandler<EventArgs> TfsUriChange { get; set; }

        public ObservableConcurrentCollection<TfsEnvironment> TfsEnvironments { get; }

        public TfsEnvironment SelectedEnvironment
        {
            get => _selectedEnvironment;
            set
            {
                var isSet = false;
                ExecuteActionInUIContext(() =>
                {
                    if (_selectedEnvironment?.Name != value?.Name
                        || _selectedEnvironment?.ServerUri != value?.ServerUri)
                    {
                        if (SelectedEnvironment != null)
                            SelectedEnvironment.SuccessfullyAuthenticated -= OnUriChanged; //todo change to weak events pattern

                        if (SetProperty(ref _selectedEnvironment, value))
                        {
                            if (SelectedEnvironment != null)
                                SelectedEnvironment.SuccessfullyAuthenticated += OnUriChanged;

                            isSet = true;
                        }
                    }
                });
                if (isSet)
                    SelectedEnvironmentHasChanged?.Invoke(_selectedEnvironment, EventArgs.Empty);
            }
        }

        private EventHandler<EventArgs> SelectedEnvironmentHasChanged { get; }

        public static void SetEnvironments(IDictionary<string, Uri> environments, IProgressStatusContainer mainViewModel)
        {
            Instance.TfsEnvironments.Clear();
            Instance.TfsEnvironments.AddRange(environments.Select(x => new TfsEnvironment(x.Key, x.Value, mainViewModel, ImagesCacheDirPath)));
        }

        public static string GetEnvironmentId(string requestUrl, ICredentials credentials)
        {
            return Instance.TfsEnvironments.FindInEnumerable(x => x.GetCredentials() == credentials && requestUrl.StartsWith(x.Server.Uri.AbsoluteUri))
                ?.EnvironmentId;
        }

        public Task<TResponse> RunProcAsync<TResponse>(string requestUrl, string environmentId, IDictionary<string, object> args = null, IList<HttpStatusCode> ommitCodes = null, JsonSerializerSettings settings = null, Method method = Method.GET) where TResponse : BaseTfsResponse, new()
        {
            TfsEnvironment env = TfsEnvironments.First(x => x.EnvironmentId == environmentId);
            return env.RunProcAsync<TResponse>(requestUrl, args, settings, ommitCodes, method);
        }

        public Task<string> RunRawAsync(string requestUrl, string environmentId, IDictionary<string, object> args = null, IList<HttpStatusCode> ommitCodes = null, Method method = Method.GET)
        {
            TfsEnvironment env = TfsEnvironments.First(x => x.EnvironmentId == environmentId);
            return env.RunRawAsync(requestUrl, args, ommitCodes, method);
        }

        private void OnSelectedEnvironmentHasChanged(object sender, EventArgs eventArgs)
        {
            if (SelectedEnvironment != null)
                EnvironmentSet.Invoke(_selectedEnvironment, EventArgs.Empty);
        }

        private void OnUriChanged(object sender, EventArgs e)
        {
            TfsUriChange.Invoke(null, null); // new EventArgs(SelectedEnvironment.ServerUri.AbsoluteUri, nameof(SelectedEnvironment.ServerUri)));
        }

        #region Singleton

        private static volatile TfsService _instance;
        private static object SyncRoot { get; } = new object();

        public static TfsService Instance
        {
            get
            {
                if (_instance == null)
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                            _instance = new TfsService();
                    }

                return _instance;
            }
        }

        private TfsService()
        {
            TfsEnvironments = new ObservableConcurrentCollection<TfsEnvironment>();
            TfsEnvironments.CollectionChanged += TfsEnvironments_CollectionChanged;
            SelectedEnvironmentHasChanged += OnSelectedEnvironmentHasChanged;
        }

        private void TfsEnvironments_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (SelectedEnvironment == null
                || TfsEnvironments.All(x => x.ServerUri != SelectedEnvironment.ServerUri))
                SelectedEnvironment = TfsEnvironments.FindInEnumerable();
        }

        #endregion
    }
}