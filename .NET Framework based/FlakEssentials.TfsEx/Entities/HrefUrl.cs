﻿using Newtonsoft.Json;
using System;

namespace FlakEssentials.TfsEx.Entities
{
    public class HrefUrl
    {
        [JsonProperty("href")]
        public Uri Href { get; set; }
    }
}