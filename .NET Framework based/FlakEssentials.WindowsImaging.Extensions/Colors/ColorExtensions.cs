﻿using System.Globalization;
using System.Windows.Media;
using DColor = System.Drawing.Color;
using MColor = System.Windows.Media.Color;

namespace FlakEssentials.WindowsImaging.Extensions.Colors;

/// <summary>
///     Extension methods for  System.Windows.Media.Color and System.Drawing.Color
/// </summary>
public static class ColorExtensions
{
    /// <summary>
    ///     Gets <see cref="Color" /> from ARGB string.
    /// </summary>
    /// <param name="colorcode">The colorcode.</param>
    /// <returns>
    ///     <see cref="Color" />
    /// </returns>
    public static MColor FromArgbString(string colorcode)
    {
        colorcode = colorcode.TrimStart('#');
        var a = byte.Parse(colorcode.Substring(0, 2), NumberStyles.HexNumber);
        var r = byte.Parse(colorcode.Substring(2, 2), NumberStyles.HexNumber);
        var g = byte.Parse(colorcode.Substring(4, 2), NumberStyles.HexNumber);
        var b = byte.Parse(colorcode.Substring(6, 2), NumberStyles.HexNumber);

        return MColor.FromArgb(a, r, g, b);
    }

    /// <summary>
    ///     Froms the RGB string.
    /// </summary>
    /// <param name="colorcode">The colorcode.</param>
    /// <returns></returns>
    public static MColor FromRgbString(string colorcode)
    {
        colorcode = colorcode.TrimStart('#');
        var r = byte.Parse(colorcode.Substring(0, 2), NumberStyles.HexNumber);
        var g = byte.Parse(colorcode.Substring(2, 2), NumberStyles.HexNumber);
        var b = byte.Parse(colorcode.Substring(4, 2), NumberStyles.HexNumber);

        return MColor.FromRgb(r, g, b);
    }

    /// <summary>
    ///     Converts to mediacolor.
    /// </summary>
    /// <param name="color">The color.</param>
    /// <returns></returns>
    public static MColor ToMediaColor(this DColor color) => MColor.FromArgb(color.A, color.R, color.G, color.B);
}