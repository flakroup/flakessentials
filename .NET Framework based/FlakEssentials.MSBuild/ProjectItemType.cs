﻿using System.ComponentModel;

namespace FlakEssentials.MSBuild;

public enum ProjectItemType
{
    [Description("None")] None,

    [Description("EmbeddedResource")] EmbeddedResource
}