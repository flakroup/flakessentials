using FEx.Abstractions.Flow;
using FEx.Abstractions.Flow.Errors;
using FEx.Asyncx.Helpers;
using FEx.AzureStorage;
using FEx.Basics.Collections.Concurrent;
using FEx.Basics.IO;
using FEx.Downloader;
using FEx.Downloader.Enums;
using FEx.Extensions;
using FEx.Extensions.Collections;
using FEx.Webx;
using FlakEssentials.Flurl;
using FlakEssentials.KeyVault;
using Flurl.Http;
using Microsoft.Azure.Storage.Blob;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace FlakEssentials.Tests;

public class TestClass
{
    private ITestOutputHelper OutputHelper { get; }

    public TestClass(ITestOutputHelper outputHelper)
    {
        OutputHelper = outputHelper;
    }

    [Fact]
    public void CheckMappings()
    {
        Assert.True(MimeTypesUtility.Mappings.IsNotNullOrEmptyReadOnlyCollection());
        Assert.Equal("application/octet-stream", MimeTypesUtility.GetDefaultMimeType(".*"));
        Assert.Contains(".*", MimeTypesUtility.GetDefaultExtensions("application/octet-stream"));
    }

    [Fact]
    public void CheckConcurrentCollection()
    {
        var coll = new ConcurrentList<SpecialDirectory>();
        coll.AddRange(SpecialDirectory.SpecialDirectories.Values);
        Assert.Contains(coll, x => x is not null);
    }

    [Fact]
    public async Task CheckDownloadSpeedAsync()
    {
        ServicePointManager.DefaultConnectionLimit = 100;
        IReadOnlyDictionary<string, string> mappings = MimeTypesUtility.Mappings;

        var target =
            new Uri(
                "https://flakstorage.blob.core.windows.net/flakblob/flakapps/FlakApps.Updater/Content/FlakWebHelper.Updater.exe");

        var file = new FileInfo(Path.Combine(Path.GetTempPath(), target.Segments.Last()));

        try
        {
            DownloadItem di = await DownloadItem.CreateAsync(target, file.FullName, true);
            await di.DownloadFileAsync();
            di?.Dispose();
            Assert.True(mappings.IsNotNullOrEmptyReadOnlyCollection());
            Assert.Equal(DownloadState.Finished, di.DState);
            Console.WriteLine($"{di.MaxOpenConnections} {di.ElapsedTime}");
            Assert.True(di.MaxOpenConnections > 1);
        }
        finally
        {
            file.Refresh();

            if (file.Exists)
                file.Delete();
        }
    }

    [Fact]
    public void CheckConcurrentUniqueCollection()
    {
        var coll = new ConcurrentList<SpecialDirectory>();
        coll.AddUniqueRange(SpecialDirectory.SpecialDirectories.Values);
        Assert.Contains(coll, x => x is not null);
    }

    [Fact]
    public async Task UploadStreamToAzureStorageAsync()
    {
        using ServiceProvider serviceProvider = new ServiceCollection().AddLogging().BuildServiceProvider();
        ILoggerFactory factory = serviceProvider.GetService<ILoggerFactory>();

        ILogger<AzureStorageService> logger = factory.CreateLogger<AzureStorageService>();
        var srv = new AzureStorageService(logger);
        srv.Configure(CredentialsService.Creds.ConnStr);
        using var stream = "HakunaMatata".ToStream();

        (string _, CloudBlockBlob blob) =
            await srv.UploadStreamAsync("", true, "HakunaMatata.txt", stream, "flakbackup");

        var blobInfo = new CloudBlockBlobInfo(blob);
        await blobInfo.EnsureExistsAsync();
        await srv.DeleteBlobAsync(blob);
        Assert.True(blobInfo.Exists);
    }

    [Fact]
    public void ResultFromStringIsNotSuccessful()
    {
        var result = new Result<object, StackError>("Test error");
        Assert.False(result.IsSuccess);
    }

    [Fact]
    public async Task TestAsyncQueueAsync()
    {
        ServiceProvider serviceProvider = new ServiceCollection().AddLogging(loggingBuilder =>
            {
                loggingBuilder.SetMinimumLevel(LogLevel.Trace);
                loggingBuilder.AddXUnit(OutputHelper);
            })
            .BuildServiceProvider();

        ILoggerFactory factory = serviceProvider.GetService<ILoggerFactory>();
        ILogger<AsyncQueue<string, IFlurlResult>> logger = factory.CreateLogger<AsyncQueue<string, IFlurlResult>>();
        ILogger<TestClass> tcLogger = factory.CreateLogger<TestClass>();
        var flurlSrv = new TestFlurlClientService(new(Environment.GetEnvironmentVariable("WebApi")), 3);
        flurlSrv.SetPoolLogger(logger);
        await flurlSrv.InitializeAsync();
        var cts = new CancellationTokenSource();

        List<Task<string>> tasks = RunTasks(flurlSrv, 300, TimeSpan.FromMilliseconds(150), cts.Token);

        await Task.Delay(1000, CancellationToken.None);
        await cts.CancelAsync();
        string[] results = await Task.WhenAll(tasks);
        cts.Dispose();

        Assert.False(results.All(x => x is not null));
        tasks.Clear();

        tcLogger.LogDebug("Finished tasks cancellation");

        tasks = RunTasks(flurlSrv, 5, TimeSpan.Zero, CancellationToken.None);
        results = await Task.WhenAll(tasks);

        Assert.True(results.All(x => x is not null));
        await serviceProvider.DisposeAsync();
    }

    private static List<Task<string>> RunTasks(FlurlClientService flurlSrv,
                                               int limit,
                                               TimeSpan delay,
                                               CancellationToken cancellationToken = default)
    {
        var tasks = new List<Task<string>>();
        const string apiPath = "query.cgi";

        var queryParams = new
        {
            method = "query",
            query = "all",
            api = "SYNO.API.Info",
            version = 1
        };

        Func<IFlurlRequest, IFlurlRequest> requestFunc = r => r.SetQueryParams(queryParams);

        for (var i = 0; i < limit; i++)
        {
            Task<string> task = flurlSrv.RunFlurlFuncAsync(apiPath,
                requestFunc,
                cli => QueueTaskAsync(delay, cli, apiPath, requestFunc, cancellationToken));

            tasks.Add(task);
        }

        return tasks;
    }

    private static async Task<string> QueueTaskAsync(TimeSpan delay,
                                                     IFlurlClient client,
                                                     string apiPath,
                                                     Func<IFlurlRequest, IFlurlRequest> requestFunc,
                                                     CancellationToken cancellationToken)
    {
        try
        {
            switch (cancellationToken.IsCancellationRequested)
            {
                case true:
                    return null;
                case false:
                    await Task.Delay(delay, cancellationToken);

                    break;
            }

            IFlurlRequest request = client.Request(apiPath);
            IFlurlRequest query = requestFunc(request);
            string result = await query.GetStringAsync(cancellationToken: cancellationToken);

            return result;
        }
        catch
        {
            //ignored
        }

        return null;
    }
}