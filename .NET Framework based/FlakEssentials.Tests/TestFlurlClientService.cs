﻿using FlakEssentials.Flurl;
using Flurl.Http.Configuration;
using System;

namespace FlakEssentials.Tests;

public class TestFlurlClientService : FlurlClientService
{
    public TestFlurlClientService(Uri url, int poolSize, Action<FlurlHttpSettings> action = null)
        : base(url, poolSize, action)
    {
    }
}