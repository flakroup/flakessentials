﻿using FEx.Basics.Abstractions;
using FEx.Extensions.Collections.Lists;
using FEx.WPFx;
using FEx.WPFx.Abstractions.Interfaces;
using FEx.WPFx.Models;
using FlakEssentials.TelerikEx.Enums;
using FlakEssentials.WindowsImaging.Extensions.Colors;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using Telerik.Windows.Controls;

namespace FlakEssentials.TelerikEx;

public class TelerikControlsHandler : NotifyPropertyChanged
{
    private static TelerikControlsHandler _instance;

    private TelerikTheme _currentTheme;

    public static TelerikControlsHandler Instance => _instance ??= new();

    public ImmutableHashSet<TelerikTheme> Themes { get; }
    public bool ImplicitStylesEnabled { get; set; }

    public TelerikTheme CurrentTheme
    {
        get => _currentTheme;
        set =>
            SetProperty(ref _currentTheme,
                value,
                _ =>
                {
                    _currentTheme = value;
                    RefreshTheme();
                });
    }

    public IViewDesign Design { get; private set; }

    private IDictionary<string, Assembly> Assemblies { get; }

    private TelerikControlsHandler()
    {
        Assemblies = new ConcurrentDictionary<string, Assembly>();
        Themes = ImmutableHashSet.Create(Enum.GetValues(typeof(TelerikTheme)).Cast<TelerikTheme>().ToArray());
    }

    /// <summary>
    ///     Loads the current theme.
    /// </summary>
    /// <param name="value">The value.</param>
    public void LoadCurrentTheme(string value) => LoadCurrentTheme(GetTelerikTheme(value));

    /// <summary>
    ///     Loads the current theme.
    /// </summary>
    /// <param name="theme">The theme.</param>
    public void LoadCurrentTheme(TelerikTheme theme)
    {
        if (!ImplicitStylesEnabled)
        {
            switch (theme)
            {
                case TelerikTheme.VisualStudio2013Light:
                    StyleManager.ApplicationTheme =
                        new VisualStudio2013Theme(VisualStudio2013Palette.ColorVariation.Light);

                    break;
                case TelerikTheme.VisualStudio2013Blue:
                    StyleManager.ApplicationTheme =
                        new VisualStudio2013Theme(VisualStudio2013Palette.ColorVariation.Blue);

                    break;
                case TelerikTheme.VisualStudio2013Dark:
                    StyleManager.ApplicationTheme =
                        new VisualStudio2013Theme(VisualStudio2013Palette.ColorVariation.Dark);

                    break;
                case TelerikTheme.ExpressionDark:
                    StyleManager.ApplicationTheme = new Expression_DarkTheme();

                    break;
                case TelerikTheme.GreenLight:
                    StyleManager.ApplicationTheme = new GreenTheme(GreenPalette.ColorVariation.Light);

                    break;
                case TelerikTheme.GreenDark:
                    StyleManager.ApplicationTheme = new GreenTheme(GreenPalette.ColorVariation.Dark);

                    break;
                case TelerikTheme.Material:
                    StyleManager.ApplicationTheme = new MaterialTheme();

                    break;
                case TelerikTheme.FluentDark:
                    FluentPalette.LoadPreset(FluentPalette.ColorVariation.Dark);
                    StyleManager.ApplicationTheme = new FluentTheme();

                    break;
                case TelerikTheme.FluentLight:
                    FluentPalette.LoadPreset(FluentPalette.ColorVariation.Light);
                    StyleManager.ApplicationTheme = new FluentTheme();

                    break;
                case TelerikTheme.Office2013:
                    StyleManager.ApplicationTheme = new Office2013Theme(Office2013Palette.ColorVariation.White);

                    break;
                case TelerikTheme.Office2013DarkGray:
                    StyleManager.ApplicationTheme = new Office2013Theme(Office2013Palette.ColorVariation.DarkGray);

                    break;
                case TelerikTheme.Office2013LightGray:
                    StyleManager.ApplicationTheme = new Office2013Theme(Office2013Palette.ColorVariation.LightGray);

                    break;
                case TelerikTheme.Office2016:
                    StyleManager.ApplicationTheme = new Office2016Theme();

                    break;
                case TelerikTheme.Office2016Touch:
                    StyleManager.ApplicationTheme = new Office2016TouchTheme();

                    break;
                case TelerikTheme.OfficeBlue:
                    StyleManager.ApplicationTheme = new Office_BlueTheme();

                    break;
                case TelerikTheme.OfficeSilver:
                    StyleManager.ApplicationTheme = new Office_SilverTheme();

                    break;
                case TelerikTheme.Summer:
                    StyleManager.ApplicationTheme = new SummerTheme();

                    break;
                case TelerikTheme.Transparent:
                    StyleManager.ApplicationTheme = new TransparentTheme();

                    break;
                case TelerikTheme.Vista:
                    StyleManager.ApplicationTheme = new VistaTheme();

                    break;
                case TelerikTheme.Windows7:
                    StyleManager.ApplicationTheme = new Windows7Theme();

                    break;
                case TelerikTheme.Windows8:
                    StyleManager.ApplicationTheme = new Windows8Theme();

                    break;
                case TelerikTheme.Windows8Touch:
                    StyleManager.ApplicationTheme = new Windows8TouchTheme();

                    break;
                default:
                    StyleManager.ApplicationTheme = new Office_BlackTheme();

                    break;
            }

            ApplyTheme(theme);
        }
    }

    /// <summary>
    ///     Gets the available themes.
    /// </summary>
    /// <returns></returns>
    public string[] GetAvailableThemes() => Enum.GetNames(typeof(TelerikTheme));

    /// <summary>
    ///     Gets the background brush.
    /// </summary>
    /// <param name="theme">The theme.</param>
    /// <returns></returns>
    public Brush GetBackgroundBrush(string theme) => GetBackgroundBrush(GetTelerikTheme(theme));

    /// <summary>
    ///     Gets the background brush.
    /// </summary>
    /// <param name="theme">The theme.</param>
    /// <returns></returns>
    public static Brush GetBackgroundBrush(TelerikTheme theme) =>
        theme switch
        {
            TelerikTheme.VisualStudio2013Light or TelerikTheme.VisualStudio2013Blue or TelerikTheme.VisualStudio2013Dark
                => (SolidColorBrush)Application.Current.TryFindResource(VisualStudio2013ResourceKey.PrimaryBrush),
            TelerikTheme.GreenLight or TelerikTheme.GreenDark => (SolidColorBrush)Application.Current.TryFindResource(
                GreenResourceKey.MainBrush),
            TelerikTheme.ExpressionDark => new(ColorExtensions.FromArgbString("#FF212224")),
            TelerikTheme.Material => (SolidColorBrush)Application.Current.TryFindResource(MaterialResourceKey
                .PrimaryBrush),
            TelerikTheme.FluentDark or TelerikTheme.FluentLight => (SolidColorBrush)Application.Current.TryFindResource(
                FluentResourceKey.PrimaryBrush),
            TelerikTheme.Office2013 or TelerikTheme.Office2013DarkGray or TelerikTheme.Office2013LightGray =>
                (SolidColorBrush)Application.Current.TryFindResource(Office2013Palette.MainColorProperty),
            TelerikTheme.Office2016 => (SolidColorBrush)Application.Current.TryFindResource(Office2016ResourceKey
                .PrimaryBrush),
            TelerikTheme.Office2016Touch => (SolidColorBrush)Application.Current.TryFindResource(
                Office2016TouchResourceKey.PrimaryBrush),
            TelerikTheme.Windows8 =>
                (SolidColorBrush)Application.Current.TryFindResource(Windows8ResourceKey.MainBrush),
            TelerikTheme.Windows8Touch => (SolidColorBrush)Application.Current.TryFindResource(Windows8TouchResourceKey
                .MainBrush),
            _ => new(ColorExtensions.FromArgbString("#FFBFBFBF"))
        };

    /// <summary>
    ///     Gets the foreground brush.
    /// </summary>
    /// <param name="theme">The theme.</param>
    /// <returns></returns>
    public Brush GetForegroundBrush(string theme) => GetForegroundBrush(GetTelerikTheme(theme));

    /// <summary>
    ///     Gets the foreground brush.
    /// </summary>
    /// <param name="theme">The theme.</param>
    /// <returns></returns>
    public static Brush GetForegroundBrush(TelerikTheme theme) =>
        theme switch
        {
            TelerikTheme.VisualStudio2013Light or TelerikTheme.VisualStudio2013Blue or TelerikTheme.VisualStudio2013Dark
                => (SolidColorBrush)Application.Current.TryFindResource(VisualStudio2013ResourceKey.MarkerBrush),
            TelerikTheme.GreenLight or TelerikTheme.GreenDark => (SolidColorBrush)Application.Current.TryFindResource(
                GreenResourceKey.MarkerBrush),
            TelerikTheme.ExpressionDark => new(ColorExtensions.FromArgbString("#FFDDDDDD")),
            TelerikTheme.Material => (SolidColorBrush)Application.Current.TryFindResource(MaterialResourceKey
                .MarkerBrush),
            TelerikTheme.FluentDark or TelerikTheme.FluentLight => (SolidColorBrush)Application.Current.TryFindResource(
                FluentResourceKey.MarkerBrush),
            TelerikTheme.Office2013 or TelerikTheme.Office2013DarkGray or TelerikTheme.Office2013LightGray =>
                (SolidColorBrush)Application.Current.TryFindResource(Office2013Palette.BasicColorProperty),
            TelerikTheme.Office2016 => (SolidColorBrush)Application.Current.TryFindResource(Office2016ResourceKey
                .MarkerBrush),
            TelerikTheme.Office2016Touch => (SolidColorBrush)Application.Current.TryFindResource(
                Office2016TouchResourceKey.MarkerBrush),
            TelerikTheme.Windows8 => (SolidColorBrush)Application.Current.TryFindResource(
                Windows8ResourceKey.BasicBrush),
            TelerikTheme.Windows8Touch => (SolidColorBrush)Application.Current.TryFindResource(Windows8TouchResourceKey
                .MainForegroundBrush),
            _ => new(Colors.Black)
        };

    /// <summary>
    ///     Gets the header background brush.
    /// </summary>
    /// <param name="theme">The theme.</param>
    /// <returns></returns>
    public static Brush GetHeaderBackgroundBrush(TelerikTheme theme) =>
        theme switch
        {
            TelerikTheme.VisualStudio2013Light or TelerikTheme.VisualStudio2013Blue or TelerikTheme.VisualStudio2013Dark
                => (SolidColorBrush)Application.Current.TryFindResource(VisualStudio2013ResourceKey.HeaderBrush),
            TelerikTheme.GreenLight or TelerikTheme.GreenDark => (SolidColorBrush)Application.Current.TryFindResource(
                GreenResourceKey.PrimaryBrush),
            TelerikTheme.ExpressionDark => new SolidColorBrush(ColorExtensions.FromArgbString("#FF212224")),
            TelerikTheme.Material => (SolidColorBrush)Application.Current.TryFindResource(MaterialResourceKey
                .PrimaryBrush),
            _ => new LinearGradientBrush(new([
                new(ColorExtensions.FromArgbString("#FF5B5B5B"), 1),
                new(ColorExtensions.FromArgbString("#FF868686"), 0),
                new(ColorExtensions.FromArgbString("#FF4F4F4F"), 0.42),
                new(ColorExtensions.FromArgbString("#FF0E0E0E"), 0.43)
            ]))
        };

    /// <summary>
    ///     Gets the control background brush.
    /// </summary>
    /// <param name="theme">The theme.</param>
    /// <returns></returns>
    public Brush GetControlBackgroundBrush(string theme) => GetControlBackgroundBrush(GetTelerikTheme(theme));

    /// <summary>
    ///     Gets the control background brush.
    /// </summary>
    /// <param name="theme">The theme.</param>
    /// <returns></returns>
    public static Brush GetControlBackgroundBrush(TelerikTheme theme) =>
        theme switch
        {
            TelerikTheme.VisualStudio2013Light or TelerikTheme.VisualStudio2013Blue or TelerikTheme.VisualStudio2013Dark
                => (SolidColorBrush)Application.Current.TryFindResource(VisualStudio2013ResourceKey.MainBrush),
            TelerikTheme.GreenLight or TelerikTheme.GreenDark => (SolidColorBrush)Application.Current.TryFindResource(
                GreenResourceKey.MainBrush),
            TelerikTheme.ExpressionDark => new(ColorExtensions.FromArgbString("#FF3D3D3D")),
            TelerikTheme.Material =>
                (SolidColorBrush)Application.Current.TryFindResource(MaterialResourceKey.MainBrush),
            _ => new(Colors.White)
        };

    /// <summary>
    ///     Gets the border brush.
    /// </summary>
    /// <param name="theme">The theme.</param>
    /// <returns></returns>
    public static Brush GetBorderBrush(TelerikTheme theme) =>
        theme switch
        {
            TelerikTheme.VisualStudio2013Light or TelerikTheme.VisualStudio2013Blue or TelerikTheme.VisualStudio2013Dark
                => (SolidColorBrush)Application.Current.TryFindResource(VisualStudio2013ResourceKey.ComplementaryBrush),
            TelerikTheme.GreenLight or TelerikTheme.GreenDark => (SolidColorBrush)Application.Current.TryFindResource(
                GreenResourceKey.ComplementaryBrush),
            TelerikTheme.ExpressionDark => new(ColorExtensions.FromArgbString("#FFFFFFFF")),
            TelerikTheme.Material => (SolidColorBrush)Application.Current.TryFindResource(MaterialResourceKey
                .DividerBrush),
            _ => new(Colors.White)
        };

    public void RefreshTheme()
    {
        LoadCurrentTheme(CurrentTheme);
        Design = GetViewDesign();
    }

    public IViewDesign GetViewDesign() =>
        new ViewDesign(() => GetBackgroundBrush(CurrentTheme),
            () => GetControlBackgroundBrush(CurrentTheme),
            () => GetForegroundBrush(CurrentTheme),
            () => GetHeaderBackgroundBrush(CurrentTheme),
            () => GetBorderBrush(CurrentTheme),
            FExWpfx.AppConfig?.MainDesign?.FontFamily);

    /// <summary>
    ///     Gets the telerik theme.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    private TelerikTheme GetTelerikTheme(string value) => Themes.FirstOrDefault(theme => value == theme.ToString());

    private void ApplyTheme(TelerikTheme theme)
    {
        foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            GetAssemblies(assembly);

        Assembly[] telerikAssemblyNames = Assemblies.Values.Where(IsTelerikAssembly).ToArray();

        string[] themedAssemblyNames = telerikAssemblyNames.Select(x => x.GetName().Name).ToArray();

        Application.Current.Resources.MergedDictionaries.RemoveFromListWhere(x =>
            SourceMatchesModuleName(x.Source, themedAssemblyNames));

        Application.Current.Resources.MergedDictionaries.AddRangeToList(GetThemeSources(theme, themedAssemblyNames));
    }

    private void GetAssemblies(Assembly assembly)
    {
        if (!Assemblies.ContainsKey(assembly.FullName))
        {
            Assemblies.Add(assembly.FullName, assembly);

            Assembly[] referencedAssemblies = assembly.GetReferencedAssemblies()
                .Where(x => !Assemblies.ContainsKey(x.FullName))
                .Select(x =>
                {
                    try
                    {
                        return Assembly.Load(x);
                    }
                    catch
                    {
                        //ignore
                    }

                    return null;
                })
                .Where(x => x is not null)
                .ToArray();

            foreach (Assembly referencedAssembly in referencedAssemblies)
                GetAssemblies(referencedAssembly);
        }
    }

    private static bool IsTelerikAssembly(Assembly assembly) => assembly.FullName.StartsWith("Telerik.Windows.Controls");

    private static IList<ResourceDictionary> GetThemeSources(TelerikTheme theme, params string[] themedAssemblyNames)
    {
        string dictionary = GetDictionaryName(theme);

        return themedAssemblyNames.Select(name => new ResourceDictionary
            {
                Source = new($"/{name};component/Themes/{dictionary}.xaml", UriKind.RelativeOrAbsolute)
            })
            .ToArray();
    }

    private static string GetDictionaryName(TelerikTheme theme) =>
        theme switch
        {
            TelerikTheme.VisualStudio2013Light or TelerikTheme.VisualStudio2013Blue or TelerikTheme.VisualStudio2013Dark
                => "GenericVisualStudio2013",
            TelerikTheme.ExpressionDark => "GenericExpressionDark",
            TelerikTheme.GreenLight or TelerikTheme.GreenDark => "GenericGreen",
            TelerikTheme.Material => "GenericMaterial",
            TelerikTheme.Vista => "GenericVista",
            TelerikTheme.Summer => "GenericSummer",
            TelerikTheme.OfficeBlue => "GenericOfficeBlue",
            TelerikTheme.OfficeSilver => "GenericOfficeSilver",
            TelerikTheme.Windows7 => "GenericWindows7",
            TelerikTheme.Transparent => "GenericTransparent",
            TelerikTheme.Windows8 => "GenericWindows8",
            TelerikTheme.Windows8Touch => "GenericWindows8Touch",
            TelerikTheme.Office2016 => "GenericOffice2016",
            TelerikTheme.Office2016Touch => "GenericOffice2016Touch",
            TelerikTheme.Office2013 or TelerikTheme.Office2013DarkGray or TelerikTheme.Office2013LightGray =>
                "GenericOffice2013",
            TelerikTheme.FluentDark or TelerikTheme.FluentLight => "GenericFluent",
            _ => "GenericOfficeBlack"
        };

    private static bool SourceMatchesModuleName(Uri source, params string[] themedAssemblyNames) =>
        source is not null && themedAssemblyNames.Select(t => $"/{t};").Any(source.OriginalString.StartsWith);
}