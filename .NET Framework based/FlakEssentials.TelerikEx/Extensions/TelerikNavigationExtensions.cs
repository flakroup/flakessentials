﻿using FEx.Abstractions;
using System.Windows.Controls;
using System.Windows.Media;
using Telerik.Windows.Controls;

namespace FlakEssentials.TelerikEx.Extensions;

public static class TelerikNavigationExtensions
{
    /// <summary>
    ///     Creates the alert.
    /// </summary>
    /// <param name="manager">The manager.</param>
    /// <param name="header">The header.</param>
    /// <param name="content">The content.</param>
    /// <param name="alertIcon">The alert icon.</param>
    /// <param name="showDuration">Duration of the show.</param>
    public static void CreateAlert(this RadDesktopAlertManager manager,
                                   string header,
                                   string content,
                                   ImageSource alertIcon,
                                   int showDuration = 5000) => FExFoundation.Dispatcher.InvokeOnMainThread(() =>
                                                                    {
                                                                        var alert = new RadDesktopAlert
                                                                        {
                                                                            Header = header,
                                                                            Content = content,
                                                                            ShowDuration = showDuration
                                                                        };

                                                                        if (alertIcon is not null)
                                                                        {
                                                                            alert.Icon = new Image
                                                                            {
                                                                                Source = alertIcon,
                                                                                Width = 48,
                                                                                Height = 48,
                                                                                Stretch = Stretch.Uniform,
                                                                                StretchDirection = StretchDirection.Both
                                                                            };

                                                                            alert.IconColumnWidth = 48;
                                                                            alert.IconMargin = new(10, 0, 20, 0);
                                                                        }

                                                                        manager.ShowAlert(alert);
                                                                    });
}