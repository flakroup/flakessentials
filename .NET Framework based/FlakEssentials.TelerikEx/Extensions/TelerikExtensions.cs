﻿using FEx.Abstractions;
using FEx.Basics.Extensions;
using FEx.Extensions;
using FEx.Extensions.Collections.Enumerables;
using FlakEssentials.WpfEx.Controls;
using FlakEssentials.WpfEx.Extensions;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace FlakEssentials.TelerikEx.Extensions;

public static class TelerikExtensions
{
    /// <summary>
    ///     Applies the sort direction.
    /// </summary>
    /// <param name="gridViewColumn">The grid view column.</param>
    /// <param name="listSortDirection">The list sort direction.</param>
    /// <param name="clear">if set to <c>true</c> [clear].</param>
    public static void ApplySortDirection(this GridViewColumn gridViewColumn,
                                          SortingState listSortDirection,
                                          bool clear)
    {
        if (clear)
        {
            RadGridView grid = GetParent<RadGridView>(gridViewColumn);

            if (grid is not null)
                grid.ClearSortDescriptions();
            else
                throw new InvalidOperationException("grid is null");
        }

        gridViewColumn.InvokeOnDispatcherContext(() => gridViewColumn.SortingState = listSortDirection);
    }

    /// <summary>
    ///     Gets the parent.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="frameworkContentElement">The framework content element.</param>
    /// <returns></returns>
    public static T GetParent<T>(this FrameworkContentElement frameworkContentElement) where T : DependencyObject
    {
        var res = frameworkContentElement.Parent as T;

        return res;
    }

    /// <summary>
    ///     Gets the parents.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="dependencyObject">The dependency object.</param>
    /// <returns></returns>
    public static List<T> GetParents<T>(this DependencyObject dependencyObject) where T : DependencyObject => dependencyObject.GetParents().Select(x => x as T).Where(x => x is not null).Distinct().ToList();

    /// <summary>
    ///     Gets the name of the sort property.
    /// </summary>
    /// <param name="col">The col.</param>
    /// <returns></returns>
    public static string GetSortPropertyName(this GridViewColumn col) => col.SortMemberPath;

    /// <summary>
    ///     Clears the sort directions.
    /// </summary>
    /// <param name="grid">The grid.</param>
    public static void ClearSortDescriptions(this DataControl grid) => grid.InvokeOnDispatcherContext(grid.Items.SortDescriptors.Clear);

    /// <summary>
    ///     Gets the data grid rows.
    /// </summary>
    /// <param name="dataGrid">The data grid.</param>
    /// <returns></returns>
    public static IEnumerable<GridViewRow> GetDataGridRows(this BaseItemsControl dataGrid)
    {
        DataItemCollection itemsSource = dataGrid.Items;

        if (itemsSource is null)
            yield return null;

        if (itemsSource is null)
            yield break;

        foreach (object item in itemsSource)
        {
            if (dataGrid.ItemContainerGenerator.ContainerFromItem(item) is GridViewRow row)
                yield return row;
        }
    }

    /// <summary>
    ///     Exports to excel.
    /// </summary>
    /// <param name="grid">The grid.</param>
    public static void ExportToExcel(this GridViewDataControl grid)
    {
        const string extension = "xlsx";

        var dialog = new SaveFileDialog
        {
            DefaultExt = extension,
            Filter = string.Format($"Excel files (*.{extension})|*.{extension}|All files (*.*)|*.*"),
            FilterIndex = 1
        };

        if (dialog.ShowDialog() == true)
        {
            using Stream stream = dialog.OpenFile();

            grid.Export(stream,
                new()
                {
                    Format = ExportFormat.Html,
                    ShowColumnHeaders = true,
                    ShowColumnFooters = true,
                    ShowGroupFooters = false,
                    Culture = CultureInfo.CurrentCulture,
                    Encoding = Encoding.UTF8
                });
        }
    }

    /// <summary>
    ///     Applies the sort descriptions.
    /// </summary>
    /// <param name="grid">The grid.</param>
    /// <param name="columnUniqueName">Name of the column unique.</param>
    /// <param name="listSortDirection">The list sort direction.</param>
    /// <param name="clear">if set to <c>true</c> [clear].</param>
    public static void ApplySortDescriptions(this RadGridView grid,
                                             string columnUniqueName,
                                             ListSortDirection listSortDirection,
                                             bool clear = true)
    {
        int idx = grid.GetColumnIndex(columnUniqueName);

        if (idx > -1)
        {
            GridViewColumn column = grid.Columns[idx];
            grid.ApplySortDescriptions(column, listSortDirection, clear);
        }
        else
        {
            new Exception($"There is no {columnUniqueName} on {grid.Name} RadGridView control").HandleException(false);
        }
    }

    /// <summary>
    ///     Applies the sort descriptions.
    /// </summary>
    /// <param name="grid">The grid.</param>
    /// <param name="gridViewColumn">The grid view column.</param>
    /// <param name="listSortDirection">The list sort direction.</param>
    /// <param name="clear">if set to <c>true</c> [clear].</param>
    public static void ApplySortDescriptions(this DataControl grid,
                                             GridViewColumn gridViewColumn,
                                             ListSortDirection listSortDirection,
                                             bool clear = true)
    {
        //grid.ExecuteActionInDispatcherContext(() =>
        //{
        if (grid is not null)
        {
            if (clear && grid.Items.SortDescriptors.Any())
                grid.Items.SortDescriptors.Clear();

            grid.Items.SortDescriptors.Add(new ColumnSortDescriptor
            {
                Column = gridViewColumn,
                SortDirection = listSortDirection
            });
        }

        //});
        //grid.ExecuteActionInDispatcherContext({
        grid.Items.Refresh();
        //});
    }

    /// <summary>
    ///     Gets the index of the column.
    /// </summary>
    /// <param name="grid">The grid.</param>
    /// <param name="columnUniqueName">Unique name of the column.</param>
    /// <returns></returns>
    public static int GetColumnIndex(this RadGridView grid, string columnUniqueName) => grid.Columns.Cast<GridViewColumn>().IndexWhere(x => x.UniqueName == columnUniqueName);

    /// <summary>
    ///     Gets the column header.
    /// </summary>
    /// <param name="colHeader">The col header.</param>
    /// <returns></returns>
    public static string GetColumnHeader(this GridViewColumn colHeader)
    {
        string header = ControlsHandler.GetColumnHeader(colHeader);

        return header.IsNullOrWhiteSpace() && colHeader is GridViewBoundColumnBase column
            ? column.Header?.ToString()
            : header;
    }

    /// <summary>
    ///     Sets the column visibility.
    /// </summary>
    /// <param name="grid">The grid.</param>
    /// <param name="columnUniqueName">Name of the column unique.</param>
    /// <param name="visible">if set to <c>true</c> [visible].</param>
    public static void SetColumnVisibility(this RadGridView grid, string columnUniqueName, bool visible)
    {
        int idx = grid.GetColumnIndex(columnUniqueName);

        if (grid is not null
            && (idx <= 0 || grid.Columns[idx] is not null))
            grid.Columns[idx].IsVisible = visible;
    }

    /// <summary>
    ///     Sets the selected cell.
    /// </summary>
    /// <param name="gridView">The grid view.</param>
    /// <param name="row">The row.</param>
    /// <param name="column">The column.</param>
    public static void SetSelectedCell(this RadGridView gridView, int row, int column)
    {
        gridView.SelectedItem = gridView.Items[row];
        gridView.CurrentItem = gridView.Items[row];

        //focus grid to enable keyboard navigation(e.g. F2)
        if (!gridView.IsFocused)
            gridView.Focus();

        if (gridView.Columns.Count > column
            && column > -1)
            gridView.ScrollIntoViewAsync(gridView.SelectedItem, gridView.Columns[column], c => c?.Focus());
    }

    public static void SelectAndScrollToFirstItem(this GridViewDataControl dataGrid) => FExFoundation.Dispatcher.InvokeOnMainThread(() =>
                                                                                             {
                                                                                                 if (dataGrid?.Items?.Count > 0)
                                                                                                 {
                                                                                                     dataGrid.SelectedItem = dataGrid.Items[0];
                                                                                                     dataGrid.ScrollIntoView(dataGrid.SelectedItem);
                                                                                                 }
                                                                                             });

    public static List<GridViewCell> GetSelectedGridViewCells(this RadGridView dataGrid)
    {
        var res = new List<GridViewCell>();

        foreach (GridViewCellInfo cellInfo in dataGrid.SelectedCells)
        {
            if (dataGrid.ItemContainerGenerator.ContainerFromItem(cellInfo.Item) is GridViewRow row)
                res.AddRange(row.Cells.Cast<GridViewCell>().Where(cell => Equals(cell.Column, cellInfo.Column)));
        }

        return res;
    }

    public static bool CopyGridCellValueToClipboard(this RadGridView grid)
    {
        var value = grid.CurrentCell.Value?.ToString();

        if (value is not null)
        {
            Clipboard.SetText(value);

            return true;
        }

        return false;
    }

    public static bool CopyPropertyGridCellValueToClipboard(this RadPropertyGrid propertyGrid)
    {
        var value = propertyGrid.SelectedPropertyDefinition.Value?.ToString();

        if (value is not null)
        {
            Clipboard.SetText(value);

            return true;
        }

        return false;
    }

    /// <summary>
    ///     Applies the sort direction.
    /// </summary>
    /// <param name="radGridView">The RAD grid view.</param>
    /// <param name="gridViewColumn">The grid view column.</param>
    /// <param name="listSortDirection">The list sort direction.</param>
    /// <param name="clear">if set to <c>true</c> [clear].</param>
    public static void ApplySortDirection(this RadGridView radGridView,
                                          GridViewColumn gridViewColumn,
                                          SortingState listSortDirection,
                                          bool clear)
    {
        if (clear)
            ClearSortDirections(radGridView);

        FExFoundation.Dispatcher.InvokeOnMainThread(() => { gridViewColumn.SortingState = listSortDirection; });
    }

    /// <summary>
    ///     Clears the sort directions.
    /// </summary>
    public static void ClearSortDirections(this RadGridView radGridView) => FExFoundation.Dispatcher.InvokeOnMainThread(() => { radGridView?.Items.SortDescriptors.Clear(); });

    /// <summary>
    ///     Gets the data grid rows.
    /// </summary>
    /// <param name="dataGrid">The data grid.</param>
    /// <returns></returns>
    public static IEnumerable<GridViewRow> GetDataGridRows(this RadGridView dataGrid)
    {
        DataItemCollection itemsSource = dataGrid.Items;

        if (itemsSource is null)
            yield return null;

        if (itemsSource is null)
            yield break;

        foreach (object item in itemsSource)
        {
            if (dataGrid.ItemContainerGenerator.ContainerFromItem(item) is GridViewRow row)
                yield return row;
        }
    }
}