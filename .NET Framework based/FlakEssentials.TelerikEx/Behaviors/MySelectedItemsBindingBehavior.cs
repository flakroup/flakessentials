﻿using System.Collections;
using System.Collections.Specialized;
using System.Windows;
using Telerik.Windows.Controls;

namespace FlakEssentials.TelerikEx.Behaviors;

public class MySelectedItemsBindingBehavior : ViewModelBase
{
    private static bool _isAttached;

    public static readonly DependencyProperty SelectedItemsProperty =
        DependencyProperty.RegisterAttached("SelectedItems",
            typeof(INotifyCollectionChanged),
            typeof(MySelectedItemsBindingBehavior),
            new(OnSelectedItemsPropertyChanged));

    private readonly RadGridView _grid;
    private readonly INotifyCollectionChanged _selectedItems;

    private bool _isSubscribedToEvents;

    public MySelectedItemsBindingBehavior(RadGridView grid, INotifyCollectionChanged selectedItems)
    {
        _grid = grid;
        _selectedItems = selectedItems;
    }

    public static void SetSelectedItems(DependencyObject dependencyObject, INotifyCollectionChanged selectedItems) => dependencyObject.SetValue(SelectedItemsProperty, selectedItems);

    public static INotifyCollectionChanged GetSelectedItems(DependencyObject dependencyObject) =>
        (INotifyCollectionChanged)dependencyObject.GetValue(SelectedItemsProperty);

    public static void Transfer(IList source, IList target)
    {
        if (source is null
            || target is null)
            return;

        target.Clear();

        foreach (object o in source)
            target.Add(o);
    }

    private static void OnSelectedItemsPropertyChanged(DependencyObject dependencyObject,
                                                       DependencyPropertyChangedEventArgs e)
    {
        if (dependencyObject is not RadGridView grid
            || e.NewValue is not INotifyCollectionChanged selectedItems
            || _isAttached)
            return;

        var behavior = new MySelectedItemsBindingBehavior(grid, selectedItems);
        behavior.Attach();
        _isAttached = true;
    }

    private void Attach()
    {
        if (_grid is not null
            && _selectedItems is not null)
        {
            Transfer(GetSelectedItems(_grid) as IList, _grid.SelectedItems);
            SubscribeToEvents();
        }
    }

    private void ContextSelectedItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
        UnsubscribeFromEvents();

        Transfer(GetSelectedItems(_grid) as IList, _grid.SelectedItems);

        SubscribeToEvents();
    }

    private void GridSelectedItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
        UnsubscribeFromEvents();

        Transfer(_grid.SelectedItems, GetSelectedItems(_grid) as IList);

        SubscribeToEvents();
    }

    private void SubscribeToEvents()
    {
        if (!_isSubscribedToEvents)
        {
            _grid.SelectedItems.CollectionChanged += GridSelectedItems_CollectionChanged;

            if (GetSelectedItems(_grid) is not null)
                GetSelectedItems(_grid).CollectionChanged += ContextSelectedItems_CollectionChanged;

            _isSubscribedToEvents = true;
        }
    }

    private void UnsubscribeFromEvents()
    {
        if (_isSubscribedToEvents)
        {
            _grid.SelectedItems.CollectionChanged -= GridSelectedItems_CollectionChanged;

            if (GetSelectedItems(_grid) is not null)
                GetSelectedItems(_grid).CollectionChanged -= ContextSelectedItems_CollectionChanged;

            _isSubscribedToEvents = false;
        }
    }
}