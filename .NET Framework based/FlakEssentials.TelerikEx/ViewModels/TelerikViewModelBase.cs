﻿using FlakEssentials.TelerikEx.Controls;
using FlakEssentials.TelerikEx.Enums;
using FlakEssentials.WpfEx.ViewModels;
using System.Collections.Immutable;
using System.Windows.Media;

namespace FlakEssentials.TelerikEx.ViewModels;

public class TelerikViewModelBase : WpfProgressListenerViewModel
{
    private TelerikTheme _currentTheme;
    private bool _showDataGridGroupPanel;
    private string _windowHeader;

    public static ImmutableHashSet<TelerikTheme> Themes => TelerikControlsHandler.Themes;

    public TelerikTheme CurrentTheme
    {
        get => _currentTheme;
        set =>
            SetProperty(ref _currentTheme,
                value,
                _ =>
                {
                    TelerikControlsHandler.CurrentTheme = CurrentTheme;
                    OnThemeChanged();
                });
    }

    public bool ShowDataGridGroupPanel
    {
        get => _showDataGridGroupPanel;
        set => Dispatcher.InvokeOnMainThread(() => SetProperty(ref _showDataGridGroupPanel, value));
    }

    public string WindowHeader
    {
        get => _windowHeader;
        set => SetProperty(ref _windowHeader, value);
    }

    protected static TelerikControlsHandler TelerikControlsHandler => TelerikControlsHandler.Instance;

    public TelerikViewModelBase(bool useMainProgressContainer = false)
        : base(useMainProgressContainer)
    {
        if (!IsInDesignMode)
            CurrentTheme = TelerikControlsHandler.CurrentTheme;
    }

    /// <summary>
    ///     Creates the alert.
    /// </summary>
    /// <param name="header">The header.</param>
    /// <param name="content">The content.</param>
    /// <param name="useApplicationLogo">if set to <c>true</c> [use application logo].</param>
    /// <param name="imageSource">The image source.</param>
    public void CreateAlert(string header,
                            string content,
                            bool useApplicationLogo = true,
                            ImageSource imageSource = null) =>
        FlakTelerikWindow.CreateAlert(header, content, useApplicationLogo, imageSource);

    protected virtual void OnThemeChanged() => Design = TelerikControlsHandler.Design;
}