﻿namespace FlakEssentials.TelerikEx.Enums;

public enum TelerikTheme
{
    None,
    Default,
    ExpressionDark,
    FluentDark,
    FluentLight,
    GreenDark,
    GreenLight,
    Material,
    Office2013,
    Office2013DarkGray,
    Office2013LightGray,
    Office2016,
    Office2016Touch,
    OfficeBlue,
    OfficeSilver,
    Summer,
    Transparent,
    Vista,
    VisualStudio2013Blue,
    VisualStudio2013Dark,
    Windows7,
    Windows8,
    Windows8Touch,
    VisualStudio2013Light
}