﻿using FlakEssentials.WpfEx.Attributes;

namespace FlakEssentials.TelerikEx.Enums;

/// <summary>
///     http://docs.telerik.com/devtools/wpf/styling-and-appearance/glyphs/common-styles-appearance-glyphs-reference-sheet
/// </summary>
public enum FontGlyphs
{
    None,

    [IconDescriptor('\ue000')] GlyphArrow45UpRight,

    [IconDescriptor('\ue001')] GlyphArrow45DownRight,

    [IconDescriptor('\ue002')] GlyphArrow45DownLeft,

    [IconDescriptor('\ue003')] GlyphArrow45UpLeft,

    [IconDescriptor('\ue004')] GlyphArrow60Up,

    [IconDescriptor('\ue005')] GlyphArrow60Right,

    [IconDescriptor('\ue006')] GlyphArrow60Down,

    [IconDescriptor('\ue007')] GlyphArrow60Left,

    [IconDescriptor('\ue008')] GlyphArrowUpward,

    [IconDescriptor('\ue009')] GlyphArrowForward,

    [IconDescriptor('\ue00A')] GlyphArrowDownward,

    [IconDescriptor('\ue00B')] GlyphArrowBackward,

    [IconDescriptor('\ue00C')] GlyphArrowDouble60Up,

    [IconDescriptor('\ue00D')] GlyphArrowDouble60Right,

    [IconDescriptor('\ue00E')] GlyphArrowDouble60Down,

    [IconDescriptor('\ue00F')] GlyphArrowDouble60Left,

    [IconDescriptor('\ue010')] GlyphArrowsKpi,

    [IconDescriptor('\ue011')] GlyphArrowsNoChange,

    [IconDescriptor('\ue012')] GlyphArrowOverflowDown,

    [IconDescriptor('\ue013')] GlyphArrowChevronUp,

    [IconDescriptor('\ue014')] GlyphArrowChevronRight,

    [IconDescriptor('\ue015')] GlyphArrowChevronDown,

    [IconDescriptor('\ue016')] GlyphArrowChevronLeft,

    [IconDescriptor('\ue017')] GlyphArrowUp,

    [IconDescriptor('\ue018')] GlyphArrowRight,

    [IconDescriptor('\ue019')] GlyphArrowDown,

    [IconDescriptor('\ue01A')] GlyphArrowLeft,

    [IconDescriptor('\ue01B')] GlyphArrowDrill,

    [IconDescriptor('\ue01C')] GlyphArrowParent,

    [IconDescriptor('\ue01D')] GlyphArrowRoot,

    [IconDescriptor('\ue01E')] GlyphArrowsResizing,

    [IconDescriptor('\ue01F')] GlyphArrowsDimensions,

    [IconDescriptor('\ue020')] GlyphArrowsSwap,

    [IconDescriptor('\ue021')] GlyphDragAndDrop,

    [IconDescriptor('\ue022')] GlyphCategorize,

    [IconDescriptor('\ue023')] GlyphGrid,

    [IconDescriptor('\ue024')] GlyphGridLayout,

    [IconDescriptor('\ue025')] GlyphGroup,

    [IconDescriptor('\ue026')] GlyphUngroup,

    [IconDescriptor('\ue027')] GlyphHandlerDrag,

    [IconDescriptor('\ue028')] GlyphLayout,

    [IconDescriptor('\ue029')] GlyphLayout1By4,

    [IconDescriptor('\ue02A')] GlyphLayout2By2,

    [IconDescriptor('\ue02B')] GlyphLayoutSideBySide,

    [IconDescriptor('\ue02C')] GlyphLayoutStacked,

    [IconDescriptor('\ue02D')] GlyphColumns,

    [IconDescriptor('\ue02E')] GlyphRows,

    [IconDescriptor('\ue02F')] GlyphReorder,

    [IconDescriptor('\ue030')] GlyphMenu,

    [IconDescriptor('\ue031')] GlyphMoreVertical,

    [IconDescriptor('\ue032')] GlyphMoreHorizontal,

    [IconDescriptor('\ue100')] GlyphUndo,

    [IconDescriptor('\ue101')] GlyphRedo,

    [IconDescriptor('\ue102')] GlyphReset,

    [IconDescriptor('\ue103')] GlyphReload,

    [IconDescriptor('\ue104')] GlyphNonRecurrence,

    [IconDescriptor('\ue105')] GlyphResetSmall,

    [IconDescriptor('\ue106')] GlyphReloadSmall,

    [IconDescriptor('\ue107')] GlyphClock,

    [IconDescriptor('\ue108')] GlyphCalendar,

    [IconDescriptor('\ue109')] GlyphSave,

    [IconDescriptor('\ue10A')] GlyphPrint,

    [IconDescriptor('\ue10B')] GlyphEdit,

    [IconDescriptor('\ue10C')] GlyphDelete,

    [IconDescriptor('\ue10D')] GlyphAttachment,

    [IconDescriptor('\ue10E')] GlyphAttachment45,

    [IconDescriptor('\ue10F')] GlyphLinkHorizontal,

    [IconDescriptor('\ue110')] GlyphUnlinkHorizontal,

    [IconDescriptor('\ue111')] GlyphLinkVertical,

    [IconDescriptor('\ue112')] GlyphUnlinkVertical,

    [IconDescriptor('\ue113')] GlyphLock,

    [IconDescriptor('\ue114')] GlyphUnlock,

    [IconDescriptor('\ue115')] GlyphCancel,

    [IconDescriptor('\ue116')] GlyphCancelOutline,

    [IconDescriptor('\ue117')] GlyphCancelCircle,

    [IconDescriptor('\ue118')] GlyphCheck,

    [IconDescriptor('\ue119')] GlyphCheckOutline,

    [IconDescriptor('\ue11A')] GlyphCheckCircle,

    [IconDescriptor('\ue11B')] GlyphClose,

    [IconDescriptor('\ue11C')] GlyphCloseOutline,

    [IconDescriptor('\ue11D')] GlyphCloseCircle,

    [IconDescriptor('\ue11E')] GlyphPlus,

    [IconDescriptor('\ue11F')] GlyphPlusOutline,

    [IconDescriptor('\ue120')] GlyphPlusCircle,

    [IconDescriptor('\ue121')] GlyphMinus,

    [IconDescriptor('\ue122')] GlyphMinusOutline,

    [IconDescriptor('\ue123')] GlyphMinusCircle,

    [IconDescriptor('\ue124')] GlyphSortAsc,

    [IconDescriptor('\ue125')] GlyphSortDesc,

    [IconDescriptor('\ue126')] GlyphUnsort,

    [IconDescriptor('\ue127')] GlyphSortAscSmall,

    [IconDescriptor('\ue128')] GlyphSortDescSmall,

    [IconDescriptor('\ue129')] GlyphFilter,

    [IconDescriptor('\ue12A')] GlyphFilterClear,

    [IconDescriptor('\ue12B')] GlyphFilterSmall,

    [IconDescriptor('\ue12C')] GlyphFilterSortAscSmall,

    [IconDescriptor('\ue12D')] GlyphFilterSortDescSmall,

    [IconDescriptor('\ue12E')] GlyphFilterAddExpression,

    [IconDescriptor('\ue12F')] GlyphFilterAddGroup,

    [IconDescriptor('\ue130')] GlyphLogin,

    [IconDescriptor('\ue131')] GlyphLogout,

    [IconDescriptor('\ue132')] GlyphDownload,

    [IconDescriptor('\ue133')] GlyphUpload,

    [IconDescriptor('\ue134')] GlyphHyperlinkOpen,

    [IconDescriptor('\ue135')] GlyphHyperlinkOpenSmall,

    [IconDescriptor('\ue136')] GlyphLaunch,

    [IconDescriptor('\ue137')] GlyphWindow,

    [IconDescriptor('\ue138')] GlyphWindowCollapse,

    [IconDescriptor('\ue139')] GlyphMinimize,

    [IconDescriptor('\ue13A')] GlyphGear,

    [IconDescriptor('\ue13B')] GlyphGears,

    [IconDescriptor('\ue13C')] GlyphWrench,

    [IconDescriptor('\ue13D')] GlyphPreview,

    [IconDescriptor('\ue13E')] GlyphZoom,

    [IconDescriptor('\ue13F')] GlyphZoomIn,

    [IconDescriptor('\ue140')] GlyphZoomOut,

    [IconDescriptor('\ue141')] GlyphPan,

    [IconDescriptor('\ue142')] GlyphCalculator,

    [IconDescriptor('\ue200')] GlyphPlay,

    [IconDescriptor('\ue201')] GlyphPause,

    [IconDescriptor('\ue202')] GlyphStop,

    [IconDescriptor('\ue203')] GlyphRewind,

    [IconDescriptor('\ue204')] GlyphForward,

    [IconDescriptor('\ue205')] GlyphVolumeDown,

    [IconDescriptor('\ue206')] GlyphVolumeUp,

    [IconDescriptor('\ue207')] GlyphVolumeOff,

    [IconDescriptor('\ue208')] GlyphHd,

    [IconDescriptor('\ue209')] GlyphSubtitles,

    [IconDescriptor('\ue20A')] GlyphPlaylist,

    [IconDescriptor('\ue20B')] GlyphAudio,

    [IconDescriptor('\ue300')] GlyphHeartOutline,

    [IconDescriptor('\ue301')] GlyphHeart,

    [IconDescriptor('\ue302')] GlyphStarOutline,

    [IconDescriptor('\ue303')] GlyphStar,

    [IconDescriptor('\ue304')] GlyphCheckBox,

    [IconDescriptor('\ue305')] GlyphCheckBoxChecked,

    [IconDescriptor('\ue306')] GlyphThreeStateIndeterminate,

    [IconDescriptor('\ue307')] GlyphThreeStateNull,

    [IconDescriptor('\ue308')] GlyphCircle,

    [IconDescriptor('\ue309')] GlyphRadioButton,

    [IconDescriptor('\ue30A')] GlyphRadioButtonChecked,

    [IconDescriptor('\ue400')] GlyphNotification,

    [IconDescriptor('\ue401')] GlyphInformation,

    [IconDescriptor('\ue402')] GlyphQuestion,

    [IconDescriptor('\ue403')] GlyphWarning,

    [IconDescriptor('\ue500')] GlyphPhotoCamera,

    [IconDescriptor('\ue501')] GlyphImage,

    [IconDescriptor('\ue502')] GlyphImageExport,

    [IconDescriptor('\ue503')] GlyphZoomActualSize,

    [IconDescriptor('\ue504')] GlyphZoomBestFit,

    [IconDescriptor('\ue505')] GlyphImageResize,

    [IconDescriptor('\ue506')] GlyphCrop,

    [IconDescriptor('\ue507')] GlyphMirror,

    [IconDescriptor('\ue508')] GlyphFlipHorizontal,

    [IconDescriptor('\ue509')] GlyphFlipVertical,

    [IconDescriptor('\ue50A')] GlyphRotate,

    [IconDescriptor('\ue50B')] GlyphRotateRight,

    [IconDescriptor('\ue50C')] GlyphRotateLeft,

    [IconDescriptor('\ue50D')] GlyphBrush,

    [IconDescriptor('\ue50E')] GlyphPalette,

    [IconDescriptor('\ue50F')] GlyphPaint,

    [IconDescriptor('\ue510')] GlyphLine,

    [IconDescriptor('\ue511')] GlyphBrightnessContrast,

    [IconDescriptor('\ue512')] GlyphSaturation,

    [IconDescriptor('\ue513')] GlyphInvertColors,

    [IconDescriptor('\ue514')] GlyphTransperancy,

    [IconDescriptor('\ue515')] GlyphGreyscale,

    [IconDescriptor('\ue516')] GlyphBlur,

    [IconDescriptor('\ue517')] GlyphSharpen,

    [IconDescriptor('\ue518')] GlyphShape,

    [IconDescriptor('\ue519')] GlyphRoundCorners,

    [IconDescriptor('\ue51A')] GlyphFrontElement,

    [IconDescriptor('\ue51B')] GlyphBackElement,

    [IconDescriptor('\ue51C')] GlyphForwardElement,

    [IconDescriptor('\ue51D')] GlyphBackwardElement,

    [IconDescriptor('\ue51E')] GlyphAlignLeftElement,

    [IconDescriptor('\ue51F')] GlyphAlignCenterElement,

    [IconDescriptor('\ue520')] GlyphAlignRightElement,

    [IconDescriptor('\ue521')] GlyphAlignTopElement,

    [IconDescriptor('\ue522')] GlyphAlignMiddleElement,

    [IconDescriptor('\ue523')] GlyphAlignBottomElement,

    [IconDescriptor('\ue524')] GlyphThumbnailsUp,

    [IconDescriptor('\ue525')] GlyphThumbnailsRight,

    [IconDescriptor('\ue526')] GlyphThumbnailsDown,

    [IconDescriptor('\ue527')] GlyphThumbnailsLeft,

    [IconDescriptor('\ue528')] GlyphFullScreen,

    [IconDescriptor('\ue529')] GlyphFullScreenExit,

    [IconDescriptor('\ue600')] GlyphPageProperties,

    [IconDescriptor('\ue601')] GlyphBold,

    [IconDescriptor('\ue602')] GlyphItalic,

    [IconDescriptor('\ue603')] GlyphUnderline,

    [IconDescriptor('\ue604')] GlyphFontFamily,

    [IconDescriptor('\ue605')] GlyphForegroundColor,

    [IconDescriptor('\ue606')] GlyphConvertLowercase,

    [IconDescriptor('\ue607')] GlyphConvertUppercase,

    [IconDescriptor('\ue608')] GlyphStrikethrough,

    [IconDescriptor('\ue609')] GlyphSubScript,

    [IconDescriptor('\ue60A')] GlyphSupScript,

    [IconDescriptor('\ue60B')] GlyphDiv,

    [IconDescriptor('\ue60C')] GlyphAll,

    [IconDescriptor('\ue60D')] GlyphH1,

    [IconDescriptor('\ue60E')] GlyphH2,

    [IconDescriptor('\ue60F')] GlyphH3,

    [IconDescriptor('\ue610')] GlyphH4,

    [IconDescriptor('\ue611')] GlyphH5,

    [IconDescriptor('\ue612')] GlyphH6,

    [IconDescriptor('\ue613')] GlyphListNumbered,

    [IconDescriptor('\ue614')] GlyphListUnordered,

    [IconDescriptor('\ue615')] GlyphIndentIncrease,

    [IconDescriptor('\ue616')] GlyphIndentDecrease,

    [IconDescriptor('\ue617')] GlyphInsertUp,

    [IconDescriptor('\ue618')] GlyphInsertMiddle,

    [IconDescriptor('\ue619')] GlyphInsertDown,

    [IconDescriptor('\ue61A')] GlyphAlignTop,

    [IconDescriptor('\ue61B')] GlyphAlignMiddle,

    [IconDescriptor('\ue61C')] GlyphAlignBottom,

    [IconDescriptor('\ue61D')] GlyphAlignLeft,

    [IconDescriptor('\ue61E')] GlyphAlignCenter,

    [IconDescriptor('\ue61F')] GlyphAlignRight,

    [IconDescriptor('\ue620')] GlyphAlignJustify,

    [IconDescriptor('\ue621')] GlyphAlignRemove,

    [IconDescriptor('\ue622')] GlyphTextWrap,

    [IconDescriptor('\ue623')] GlyphRuleHorizontal,

    [IconDescriptor('\ue624')] GlyphTableAlignTopLeft,

    [IconDescriptor('\ue625')] GlyphTableAlignTopCenter,

    [IconDescriptor('\ue626')] GlyphTableAlignTopRight,

    [IconDescriptor('\ue627')] GlyphTableAlignMiddleLeft,

    [IconDescriptor('\ue628')] GlyphTableAlignMiddleCenter,

    [IconDescriptor('\ue629')] GlyphTableAlignMiddleRight,

    [IconDescriptor('\ue62A')] GlyphTableAlignBottomLeft,

    [IconDescriptor('\ue62B')] GlyphTableAlignBottomCenter,

    [IconDescriptor('\ue62C')] GlyphTableAlignBottomRight,

    [IconDescriptor('\ue62D')] GlyphTableAlignRemove,

    [IconDescriptor('\ue62E')] GlyphBordersAll,

    [IconDescriptor('\ue62F')] GlyphBordersOutside,

    [IconDescriptor('\ue630')] GlyphBordersInside,

    [IconDescriptor('\ue631')] GlyphBordersInsideHorizontal,

    [IconDescriptor('\ue632')] GlyphBordersInsideVertical,

    [IconDescriptor('\ue633')] GlyphBorderTop,

    [IconDescriptor('\ue634')] GlyphBorderBottom,

    [IconDescriptor('\ue635')] GlyphBorderLeft,

    [IconDescriptor('\ue636')] GlyphBorderRight,

    [IconDescriptor('\ue637')] GlyphBorderNo,

    [IconDescriptor('\ue638')] GlyphBordersShowHide,

    [IconDescriptor('\ue639')] GlyphForm,

    [IconDescriptor('\ue63A')] GlyphFormElement,

    [IconDescriptor('\ue63B')] GlyphCodeSnippet,

    [IconDescriptor('\ue63C')] GlyphSelectAll,

    [IconDescriptor('\ue63D')] GlyphButton,

    [IconDescriptor('\ue63E')] GlyphSelectBox,

    [IconDescriptor('\ue63F')] GlyphCalendarDate,

    [IconDescriptor('\ue640')] GlyphGroupBox,

    [IconDescriptor('\ue641')] GlyphTextarea,

    [IconDescriptor('\ue642')] GlyphTextbox,

    [IconDescriptor('\ue643')] GlyphTextboxHidden,

    [IconDescriptor('\ue644')] GlyphPassword,

    [IconDescriptor('\ue645')] GlyphParagraphAdd,

    [IconDescriptor('\ue646')] GlyphEditTools,

    [IconDescriptor('\ue647')] GlyphTemplateManager,

    [IconDescriptor('\ue648')] GlyphChangeManually,

    [IconDescriptor('\ue649')] GlyphTrackChanges,

    [IconDescriptor('\ue64A')] GlyphTrackChangesEnable,

    [IconDescriptor('\ue64B')] GlyphTrackChangesAccept,

    [IconDescriptor('\ue64C')] GlyphTrackChangesAcceptAll,

    [IconDescriptor('\ue64D')] GlyphTrackChangesReject,

    [IconDescriptor('\ue64E')] GlyphTrackChangesRejectAll,

    [IconDescriptor('\ue64F')] GlyphDocumentManager,

    [IconDescriptor('\ue650')] GlyphCustomIcon,

    [IconDescriptor('\ue651')] GlyphDictionaryAdd,

    [IconDescriptor('\ue652')] GlyphImageLightDialog,

    [IconDescriptor('\ue653')] GlyphImageEdit,

    [IconDescriptor('\ue654')] GlyphImageMapEditor,

    [IconDescriptor('\ue655')] GlyphComment,

    [IconDescriptor('\ue656')] GlyphCommentRemove,

    [IconDescriptor('\ue657')] GlyphCommentsRemoveAll,

    [IconDescriptor('\ue658')] GlyphSilverlight,

    [IconDescriptor('\ue659')] GlyphMediaManager,

    [IconDescriptor('\ue65A')] GlyphVideoExternal,

    [IconDescriptor('\ue65B')] GlyphFlashManager,

    [IconDescriptor('\ue65C')] GlyphFindAndReplace,

    [IconDescriptor('\ue65D')] GlyphCopy,

    [IconDescriptor('\ue65E')] GlyphCut,

    [IconDescriptor('\ue65F')] GlyphPaste,

    [IconDescriptor('\ue660')] GlyphPasteAsHtml,

    [IconDescriptor('\ue661')] GlyphPasteFromWord,

    [IconDescriptor('\ue662')] GlyphPasteFromWordStripFile,

    [IconDescriptor('\ue663')] GlyphPasteHtml,

    [IconDescriptor('\ue664')] GlyphPasteMarkdown,

    [IconDescriptor('\ue665')] GlyphPastePlainText,

    [IconDescriptor('\ue666')] GlyphApplyFormat,

    [IconDescriptor('\ue667')] GlyphClearCss,

    [IconDescriptor('\ue668')] GlyphCopyFormat,

    [IconDescriptor('\ue669')] GlyphStripAllFormating,

    [IconDescriptor('\ue66A')] GlyphStripCssFormat,

    [IconDescriptor('\ue66B')] GlyphStripFontElements,

    [IconDescriptor('\ue66C')] GlyphStripSpanElements,

    [IconDescriptor('\ue66D')] GlyphStripWordFormatting,

    [IconDescriptor('\ue66E')] GlyphFormatCodeBlock,

    [IconDescriptor('\ue66F')] GlyphStyleBuilder,

    [IconDescriptor('\ue670')] GlyphModuleManager,

    [IconDescriptor('\ue671')] GlyphHyperlinkLightDialog,

    [IconDescriptor('\ue672')] GlyphHyperlink,

    [IconDescriptor('\ue673')] GlyphHyperlinkRemove,

    [IconDescriptor('\ue674')] GlyphHyperlinkEmail,

    [IconDescriptor('\ue675')] GlyphAnchor,

    [IconDescriptor('\ue676')] GlyphTableLightDialog,

    [IconDescriptor('\ue677')] GlyphTable,

    [IconDescriptor('\ue678')] GlyphTableProperties,

    [IconDescriptor('\ue679')] GlyphTableCell,

    [IconDescriptor('\ue67A')] GlyphTableCellProperties,

    [IconDescriptor('\ue67B')] GlyphTableColumnInsertLeft,

    [IconDescriptor('\ue67C')] GlyphTableColumnInsertRight,

    [IconDescriptor('\ue67D')] GlyphTableRowInsertAbove,

    [IconDescriptor('\ue67E')] GlyphTableRowInsertBelow,

    [IconDescriptor('\ue67F')] GlyphTableColumnDelete,

    [IconDescriptor('\ue680')] GlyphTableRowDelete,

    [IconDescriptor('\ue681')] GlyphTableCellDelete,

    [IconDescriptor('\ue682')] GlyphTableDelete,

    [IconDescriptor('\ue683')] GlyphCellsMerge,

    [IconDescriptor('\ue684')] GlyphCellsMergeHorizontally,

    [IconDescriptor('\ue685')] GlyphCellsMergeVertically,

    [IconDescriptor('\ue686')] GlyphCellSplitHorizontally,

    [IconDescriptor('\ue687')] GlyphCellSplitVertically,

    [IconDescriptor('\ue688')] GlyphTableUnmerge,

    [IconDescriptor('\ue689')] GlyphPaneFreeze,

    [IconDescriptor('\ue68A')] GlyphRowFreeze,

    [IconDescriptor('\ue68B')] GlyphColumnFreeze,

    [IconDescriptor('\ue68C')] GlyphToolbarFloat,

    [IconDescriptor('\ue68D')] GlyphSpellChecker,

    [IconDescriptor('\ue68E')] GlyphValidationXhtml,

    [IconDescriptor('\ue68F')] GlyphValidationData,

    [IconDescriptor('\ue690')] GlyphToggleFullScreenMode,

    [IconDescriptor('\ue691')] GlyphFormulaFx,

    [IconDescriptor('\ue692')] GlyphSum,

    [IconDescriptor('\ue693')] GlyphSymbol,

    [IconDescriptor('\ue694')] GlyphDollar,

    [IconDescriptor('\ue695')] GlyphPercent,

    [IconDescriptor('\ue696')] GlyphCustomFormat,

    [IconDescriptor('\ue697')] GlyphDecimalIncrease,

    [IconDescriptor('\ue698')] GlyphDecimalDecrease,

    [IconDescriptor('\ue700')] GlyphGlobeOutline,

    [IconDescriptor('\ue701')] GlyphGlobe,

    [IconDescriptor('\ue702')] GlyphMarkerPin,

    [IconDescriptor('\ue703')] GlyphMarkerPinTarget,

    [IconDescriptor('\ue704')] GlyphPin,

    [IconDescriptor('\ue705')] GlyphUnpin,

    [IconDescriptor('\ue800')] GlyphShare,

    [IconDescriptor('\ue801')] GlyphUser,

    [IconDescriptor('\ue802')] GlyphInbox,

    [IconDescriptor('\ue803')] GlyphBlogger,

    [IconDescriptor('\ue804')] GlyphBloggerBox,

    [IconDescriptor('\ue805')] GlyphDelicious,

    [IconDescriptor('\ue806')] GlyphDeliciousBox,

    [IconDescriptor('\ue807')] GlyphDigg,

    [IconDescriptor('\ue808')] GlyphDiggBox,

    [IconDescriptor('\ue809')] GlyphEmail,

    [IconDescriptor('\ue80A')] GlyphEmailBox,

    [IconDescriptor('\ue80B')] GlyphFacebook,

    [IconDescriptor('\ue80C')] GlyphFacebookBox,

    [IconDescriptor('\ue80D')] GlyphGoogleBookmarks,

    [IconDescriptor('\ue80E')] GlyphGoogleBookmarksBox,

    [IconDescriptor('\ue80F')] GlyphGooglePlus,

    [IconDescriptor('\ue810')] GlyphGooglePlusBox,

    [IconDescriptor('\ue811')] GlyphLinkedin,

    [IconDescriptor('\ue812')] GlyphLinkedinBox,

    [IconDescriptor('\ue813')] GlyphMyspace,

    [IconDescriptor('\ue814')] GlyphMyspaceBox,

    [IconDescriptor('\ue815')] GlyphPinterest,

    [IconDescriptor('\ue816')] GlyphPinterestBox,

    [IconDescriptor('\ue817')] GlyphReddit,

    [IconDescriptor('\ue818')] GlyphRedditBox,

    [IconDescriptor('\ue819')] GlyphStumbleUpon,

    [IconDescriptor('\ue81A')] GlyphStumbleUponBox,

    [IconDescriptor('\ue81B')] GlyphTellAFriend,

    [IconDescriptor('\ue81C')] GlyphTellAFriendBox,

    [IconDescriptor('\ue81D')] GlyphTumblr,

    [IconDescriptor('\ue81E')] GlyphTumblrBox,

    [IconDescriptor('\ue81F')] GlyphTwitter,

    [IconDescriptor('\ue820')] GlyphTwitterBox,

    [IconDescriptor('\ue821')] GlyphYammer,

    [IconDescriptor('\ue822')] GlyphYammerBox,

    [IconDescriptor('\ue900')] GlyphFolder,

    [IconDescriptor('\ue901')] GlyphFolderOpen,

    [IconDescriptor('\ue902')] GlyphFolderAdd,

    [IconDescriptor('\ue903')] GlyphFolderUp,

    [IconDescriptor('\ue904')] GlyphFieldsMore,

    [IconDescriptor('\ue905')] GlyphAggregateFields,

    [IconDescriptor('\ue906')] GlyphFile,

    [IconDescriptor('\ue907')] GlyphFileAdd,

    [IconDescriptor('\ue908')] GlyphTxt,

    [IconDescriptor('\ue909')] GlyphCsv,

    [IconDescriptor('\ue90A')] GlyphExcel,

    [IconDescriptor('\ue90B')] GlyphWord,

    [IconDescriptor('\ue90C')] GlyphMdb,

    [IconDescriptor('\ue90D')] GlyphPpt,

    [IconDescriptor('\ue90E')] GlyphPdf,

    [IconDescriptor('\ue90F')] GlyphPsd,

    [IconDescriptor('\ue910')] GlyphFlash,

    [IconDescriptor('\ue911')] GlyphConfig,

    [IconDescriptor('\ue912')] GlyphAscx,

    [IconDescriptor('\ue913')] GlyphBac,

    [IconDescriptor('\ue914')] GlyphZip,

    [IconDescriptor('\ue915')] GlyphFilm,

    [IconDescriptor('\ue916')] GlyphCss3,

    [IconDescriptor('\ue917')] GlyphHtml5,

    [IconDescriptor('\ue918')] GlyphHtml,

    [IconDescriptor('\ue919')] GlyphCss,

    [IconDescriptor('\ue91A')] GlyphJs,

    [IconDescriptor('\ue91B')] GlyphExe,

    [IconDescriptor('\ue91C')] GlyphCsproj,

    [IconDescriptor('\ue91D')] GlyphVbproj,

    [IconDescriptor('\ue91E')] GlyphCs,

    [IconDescriptor('\ue91F')] GlyphVb,

    [IconDescriptor('\ue920')] GlyphSln,

    [IconDescriptor('\ue921')] GlyphCloud
}