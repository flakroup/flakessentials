﻿using System.Collections;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace FlakEssentials.TelerikEx.Controls;

public class RowNumberColumn : GridViewDataColumn
{
    public override FrameworkElement CreateCellElement(GridViewCell cell, object dataItem)
    {
        TextBlock textBlock = cell.Content as TextBlock ?? new TextBlock();
        textBlock.Text = (((IList)DataControl.ItemsSource).IndexOf(dataItem) + 1).ToString();

        return textBlock;
    }
}