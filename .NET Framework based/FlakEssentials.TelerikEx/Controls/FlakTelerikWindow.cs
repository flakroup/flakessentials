﻿using FEx.Abstractions;
using FEx.WPFx;
using FlakEssentials.TelerikEx.Extensions;
using FlakEssentials.TelerikEx.ViewModels;
using FlakEssentials.WpfEx.Controls;
using System.ComponentModel;
using System.Windows.Markup;
using System.Windows.Media;
using Telerik.Windows.Controls;

namespace FlakEssentials.TelerikEx.Controls;

public static class FlakTelerikWindow
{
    private static RadDesktopAlertManager _manager;

    /// <summary>
    ///     The alert manager
    /// </summary>
    public static RadDesktopAlertManager Manager => GetAlertManager(AlertScreenPosition.BottomRight);

    /// <summary>
    ///     Initializes a new instance of the <see cref="T:Telerik.Windows.Controls.RadDesktopAlertManager" /> class.
    /// </summary>
    /// <param name="screenPosition">The position on the screen used to display the alerts on.</param>
    /// <param name="x">Offset of the used x screen position.</param>
    /// <param name="y">Offset of the used y screen position.</param>
    /// <param name="alertsDistance">Distance between the opened alerts.</param>
    public static RadDesktopAlertManager GetAlertManager(AlertScreenPosition screenPosition,
                                                         double x = -5,
                                                         double y = -5,
                                                         double alertsDistance = 5) =>
        _manager ??= new(screenPosition, new(x, y), alertsDistance);

    /// <summary>
    ///     Creates the alert.
    /// </summary>
    /// <param name="header">The header.</param>
    /// <param name="content">The content.</param>
    /// <param name="useApplicationLogo">if set to <c>true</c> [use application logo].</param>
    /// <param name="imageSource">The image source.</param>
    public static void CreateAlert(string header,
                                   string content,
                                   bool useApplicationLogo = true,
                                   ImageSource imageSource = null) =>
        FExFoundation.Dispatcher.InvokeOnMainThread(() =>
        {
            if (useApplicationLogo && imageSource is null)
                imageSource = FExWpfx.AppConfig.ApplicationLogo;

            Manager.CreateAlert(header, content, imageSource);
        });
}

[DefaultProperty("Content")]
[ContentProperty("Content")]
public class FlakTelerikWindow<TViewModel> : FlakWindow<TViewModel>
    where TViewModel : TelerikViewModelBase
{
    public void CreateAlert(string header,
                            string content,
                            bool useApplicationLogo = true,
                            ImageSource imageSource = null) =>
        FlakTelerikWindow.CreateAlert(header, content, useApplicationLogo, imageSource);

    protected override void OnClosing(CancelEventArgs e)
    {
        FlakTelerikWindow.Manager.CloseAllAlerts(false);
        RadWindowManager.Current.CloseAllWindows();

        base.OnClosing(e);
    }

    // ReSharper disable UnusedParameter.Global
    protected void GridView_OnSearchPanelVisibilityChanged(object sender, VisibilityChangedEventArgs e)
    // ReSharper restore UnusedParameter.Global
    {
        var grid = (RadGridView)sender;
        grid.ShowSearchPanel = true;
    }
}