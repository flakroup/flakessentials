﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using Telerik.Windows.Controls;

namespace FlakEssentials.TelerikEx.Converters;

/// <summary>
///     GridView header to string converter.
/// </summary>
public class GridViewHeaderToStringConverter : IValueConverter
{
    /// <summary>
    ///     Converts a value.
    /// </summary>
    /// <param name="value">The value produced by the binding source.</param>
    /// <param name="targetType">The type of the binding target property.</param>
    /// <param name="parameter">The converter parameter to use.</param>
    /// <param name="culture">The culture to use in the converter.</param>
    /// <returns>
    ///     A converted value. If the method returns null, the valid null value is used.
    /// </returns>
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        switch (value)
        {
            case null:
                return null;
            case string simpleHeader when !string.IsNullOrWhiteSpace(simpleHeader):
                return simpleHeader;
        }

        if (value is not TextBlock textBlock)
        {
            var stackPanel = value as StackPanel;
            textBlock = stackPanel.ChildrenOfType<TextBlock>().FirstOrDefault();
        }

        return textBlock is not null
            ? textBlock.Text
            : "???";
    }

    /// <summary>
    ///     Converts a value.
    /// </summary>
    /// <param name="value">The value that is produced by the binding target.</param>
    /// <param name="targetType">The type to convert to.</param>
    /// <param name="parameter">The converter parameter to use.</param>
    /// <param name="culture">The culture to use in the converter.</param>
    /// <returns>
    ///     A converted value. If the method returns null, the valid null value is used.
    /// </returns>
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => null;
}