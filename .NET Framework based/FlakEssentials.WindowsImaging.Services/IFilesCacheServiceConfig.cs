﻿using FlakEssentials.EFCore.Configuration;

namespace FlakEssentials.WindowsImaging.Services;

public interface IFilesCacheServiceConfig : IIndexEntryConfig
{
    IDbServiceConfig DbServiceConfig { get; }
    bool CacheAll { get; }
}