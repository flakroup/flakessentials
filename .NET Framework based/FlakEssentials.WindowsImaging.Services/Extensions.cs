﻿using FlakEssentials.WindowsImaging.Services.Model;

namespace FlakEssentials.WindowsImaging.Services;

public static class Extensions
{
    public static bool DoesCacheExists(this IndexEntry entry)
    {
        entry?.Cache?.Refresh();

        return entry?.Cache?.Exists ?? false;
    }
}