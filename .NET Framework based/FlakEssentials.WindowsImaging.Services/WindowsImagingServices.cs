﻿using FEx.DependencyInjection.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace FlakEssentials.WindowsImaging.Services;

public class WindowsImagingServices : InitializeModule<IWindowsImagingServicesModule>
{
    protected override void AddServices(IWindowsImagingServicesModule container, IServiceCollection services)
    {
        WindowsImagingServicesModule.AddServices(container, services);
    }
}