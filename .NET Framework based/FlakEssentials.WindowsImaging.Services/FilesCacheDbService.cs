﻿using FEx.Abstractions.Interfaces;
using FEx.DI.Abstractions.Interfaces;
using FEx.EFCore.Helpers;
using FEx.EFCore.Interfaces;
using FlakEssentials.EFCore;
using FlakEssentials.WindowsImaging.Services.Model;

namespace FlakEssentials.WindowsImaging.Services;

public class FilesCacheDbService : EFCoreDatabaseBackedService<FilesCacheContext>
{
    public FilesCacheDbService(IScopeProvider scopeProvider,
                               IFilesCacheServiceConfig config,
                               ResilientTransaction resilientTransaction,
                               ISqlDbHelper dbHelper,
                               IAsyncInitializable[] dependencies)
        : base(scopeProvider, config.DbServiceConfig, resilientTransaction, dbHelper, dependencies)
    {
        Initialize();
    }
}