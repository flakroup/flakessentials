﻿using FlakEssentials.EFCore.Interfaces;
using FlakEssentials.Imaging.Abstractions;
using FlakEssentials.WindowsImaging.Services.Model;
using StrongInject;
using System.Diagnostics.CodeAnalysis;

namespace FlakEssentials.WindowsImaging.Services;

[SuppressMessage("ReSharper", "PossibleInterfaceMemberAmbiguity")]
public interface IWindowsImagingServicesModule : IContainer<IFilesCacheServiceConfig>, IContainer<IIndexEntryConfig>,
    IContainer<IFilesCacheService>, IContainer<IEFCoreDatabaseBackedService<FilesCacheContext>>,
    IContainer<IndexEntriesCache>, IContainer<ICachedImageStorage>
{
}