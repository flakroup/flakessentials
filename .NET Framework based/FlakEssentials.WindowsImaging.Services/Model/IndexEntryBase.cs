﻿using FEx.Basics.Abstractions;
using FEx.Extensions;
using FlakEssentials.Imaging.Abstractions;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FlakEssentials.WindowsImaging.Services.Model;

public class IndexEntryBase : NotifyPropertyChanged, IIndexEntryBase
{
    private string _absoluteUri;
    private long _responseContentLength;
    private string _filePath;
    private string _checkSum;
    private int _pixelHeight;
    private int _pixelWidth;
    private Uri _localUri;

    [Key]
    [StringLength(1024)]
    public string AbsoluteUri
    {
        get => _absoluteUri;
        set
        {
            if (SetProperty(ref _absoluteUri, value))
                OnAbsoluteUriChange();
        }
    }

    [StringLength(1024)]
    public string CheckSum
    {
        get => _checkSum;
        set
        {
            if (SetProperty(ref _checkSum, value))
            {
            }
        }
    }

    [StringLength(1024)]
    public string FilePath
    {
        get => _filePath;
        set
        {
            if (SetProperty(ref _filePath, value))
                OnFilePathChange();
        }
    }

    public long ResponseContentLength
    {
        get => _responseContentLength;
        set
        {
            if (SetProperty(ref _responseContentLength, value))
            {
            }
        }
    }

    public int PixelHeight
    {
        get => _pixelHeight;
        set => SetProperty(ref _pixelHeight, value);
    }

    public int PixelWidth
    {
        get => _pixelWidth;
        set => SetProperty(ref _pixelWidth, value);
    }

    [JsonIgnore]
    [NotMapped]
    public Uri LocalUri
    {
        get => _localUri;
        protected set => SetProperty(ref _localUri, value);
    }

    public bool Equals(IIndexEntryBase other) => IsEqual(other);

    public virtual bool IsEqual(IIndexEntryBase val) =>
        AbsoluteUri.IsBothNullOrEqual(val.AbsoluteUri)
        && CheckSum.IsBothNullOrEqual(val.CheckSum)
        && FilePath.IsBothNullOrEqual(val.FilePath)
        && ResponseContentLength == val.ResponseContentLength;

    protected virtual void OnAbsoluteUriChange()
    {
    }

    protected virtual void OnFilePathChange()
    {
    }
}