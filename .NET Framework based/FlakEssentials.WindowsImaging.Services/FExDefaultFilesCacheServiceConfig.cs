﻿using FEx.Abstractions;
using FEx.Abstractions.Interfaces;
using FEx.EFCore.Configuration;
using FEx.Extensions.IO;
using FlakEssentials.EFCore.Configuration;
using System;
using System.IO;

namespace FlakEssentials.WindowsImaging.Services;

public class FExDefaultFilesCacheServiceConfig : FilesCacheServiceConfig
{
    public FExDefaultFilesCacheServiceConfig(IAppInfoProvider appInfoProvider,
                                             DirectoryInfo imageCache = null,
                                             string sqlDbName = null,
                                             string sqliteDbFileName = null,
                                             bool useSqlite = false,
                                             bool useHttpClientService = false,
                                             TimeSpan? cacheValidPeriod = null,
                                             bool cacheAll = true)
        : base(new DbServiceConfig(new FExDbConfig
            {
                SqliteDbFile =
                    (imageCache ?? appInfoProvider.AppData.GetDescendantDirectory("ImageCache")).GetDescendantFile(
                        sqliteDbFileName ?? "IndexEF.db"),
                SqlDbName = sqlDbName ?? $"{FExFoundation.AppInfoProvider.Name}ImageCache",
                UseSqlite = useSqlite,
                DropIfMigrationFailed = true
            }),
            imageCache ?? appInfoProvider.AppData.GetDescendantDirectory("ImageCache"),
            useHttpClientService,
            cacheValidPeriod,
            cacheAll)
    {
    }
}