﻿using System;
using System.IO;

namespace FlakEssentials.WindowsImaging.Services;

public interface IIndexEntryConfig
{
    DirectoryInfo FilesCacheDir { get; }
    string FilesCacheDirPath { get; }
    bool UseHttpClientService { get; }
    TimeSpan? CacheValidPeriod { get; }
}