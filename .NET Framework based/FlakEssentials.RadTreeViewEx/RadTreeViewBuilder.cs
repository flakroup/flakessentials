﻿using FEx.Abstractions.Interfaces;
using FEx.MVVM.Abstractions;
using FEx.WPFx;
using FEx.WPFx.Controls;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Telerik.Windows.Controls;

namespace FlakEssentials.RadTreeViewEx;

public class RadTreeViewBuilder : TreeViewBuilderBase<RadTreeViewItem>
{
    public RadTreeViewBuilder(FileSystemIconsProvider fileSystemIconsProvider, IFExDispatcher dispatcher)
        : base(fileSystemIconsProvider, dispatcher)
    {
    }

    public override async Task<RadTreeViewItem> GetTreeViewItemAsync(FExTreeViewNode nodeStub)
    {
        BitmapSource img = await GetBitmapSourceAsync(nodeStub);

        return await _dispatcher.InvokeOnMainThreadAsync(() =>
        {
            var item = new RadTreeViewItem
            {
                Name = nodeStub.NodeName,
                Header = nodeStub.NodeHeader,
                IsExpanded = nodeStub.IsExpanded
            };

            if (img is not null)
                item.DefaultImageSrc = img;

            //res.SetBinding(Control.ForegroundProperty, new Binding(nameof(Foreground)) { Source = this });
            return item;
        });
    }
}