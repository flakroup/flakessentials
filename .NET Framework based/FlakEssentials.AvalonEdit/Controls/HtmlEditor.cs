﻿using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Highlighting;
using System.Windows;

namespace FlakEssentials.AvalonEdit.Controls;

public class HtmlEditor : TextEditor
{
    public static readonly DependencyProperty TextProperty = DependencyProperty.Register(nameof(Text),
        typeof(string),
        typeof(HtmlEditor),
        new(default(string), TextChanged));

    public new string Text
    {
        get => Document.Text;
        set => SetValue(TextProperty, value);
    }

    public HtmlEditor()
    {
        SyntaxHighlighting = HighlightingManager.Instance.GetDefinition("XML");
        Options.EnableHyperlinks = true;
        Options.EnableEmailHyperlinks = true;

        ShowLineNumbers = true;
    }

    private new static void TextChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
    {
        if (args.NewValue != null)
        {
            var xmlViewer = (HtmlEditor)dependencyObject;
            xmlViewer.Document.Text = (string)args.NewValue;
        }
    }
}