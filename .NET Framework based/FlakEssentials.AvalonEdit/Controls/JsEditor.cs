﻿using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Highlighting;
using System.ComponentModel;
using System.Windows;

namespace FlakEssentials.AvalonEdit.Controls;

public class JsEditor : TextEditor, INotifyPropertyChanged
{
    public static readonly DependencyProperty TextProperty = DependencyProperty.Register(nameof(Text),
        typeof(string),
        typeof(JsEditor),
        new(default(string), TextChanged));

    //private void DocumentTextChanged(object sender, EventArgs e)
    //{
    //    RaisePropertyChanged(nameof(Text));
    //}

    //protected override void OnTextChanged(EventArgs e)
    //{
    //    RaisePropertyChanged(nameof(Text));
    //    base.OnTextChanged(e);
    //}

    public event PropertyChangedEventHandler PropertyChanged;

    public new string Text
    {
        get => Document.Text;
        set => SetValue(TextProperty, value);
    }

    public JsEditor()
    {
        SyntaxHighlighting = HighlightingManager.Instance.GetDefinition("JavaScript");
        //Options.AllowScrollBelowDocument = true;
        Options.EnableHyperlinks = true;
        Options.EnableEmailHyperlinks = true;
        //Options.ShowSpaces = true;
        //Options.ShowTabs = true;
        //Options.ShowEndOfLine = true;              

        ShowLineNumbers = true;

        //_foldingManager = FoldingManager.Install(TextArea);
        //_foldingStrategy = new XmlFoldingStrategy();
        //Document.TextChanged += DocumentTextChanged;
    }

    public void RaisePropertyChanged(string info) => PropertyChanged?.Invoke(this, new(info));

    private new static void TextChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
    {
        if (args.NewValue != null)
        {
            var xmlViewer = (JsEditor)dependencyObject;
            xmlViewer.Document.Text = (string)args.NewValue;
        }
    }

    //private XmlFoldingStrategy _foldingStrategy;
    //private FoldingManager _foldingManager;
    //private volatile bool _updateFoldingRequested;
    //private async void DocumentTextChanged(object sender, EventArgs eventArgs)
    //{
    //    if (!_updateFoldingRequested)
    //    {
    //        _updateFoldingRequested = true;
    //        await Task.Delay(1000);
    //    }
    //    _updateFoldingRequested = false;
    //    _foldingStrategy.UpdateFoldings(_foldingManager, Document);
    //}
}