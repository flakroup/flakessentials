﻿using System.Windows.Shell;

namespace FlakEssentials.WpfEx.ViewModels;

public interface IWpfProgressStatusContainer : IWpfProgressInfoGet
{
    void SetPrgState(TaskbarItemProgressState value);
}