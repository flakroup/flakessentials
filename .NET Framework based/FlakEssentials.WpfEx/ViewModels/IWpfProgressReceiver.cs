﻿using FEx.MVVM.Abstractions.Interfaces;

namespace FlakEssentials.WpfEx.ViewModels;

public interface IWpfProgressReceiver<out T> : IProgressReceiver<T> where T : IWpfProgressStatusContainer
{
}