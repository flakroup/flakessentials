﻿using FEx.Legacy.Mvvm.Abstractions.Interfaces;
using System.Windows.Controls;

namespace FlakEssentials.WpfEx.ViewModels;

public interface IWpfProgressListenerViewModel<out T> : IWpfProgressReceiver<T>, IThreadingAwareViewModel
    where T : IWpfProgressStatusContainer
{
    ContentControl View { get; set; }
    string ViewName { get; set; }
}