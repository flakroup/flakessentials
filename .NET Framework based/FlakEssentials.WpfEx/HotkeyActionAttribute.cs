﻿using System;
using System.Windows.Input;

namespace FlakEssentials.WpfEx;

/// <summary>
///     Specifies a hotkey.
/// </summary>
[AttributeUsage(AttributeTargets.Method)]
public sealed class HotkeyActionAttribute : Attribute
{
    /// <summary>
    ///     Holds the hotkey value.
    /// </summary>
    public Key Key { get; }

    /// <summary>
    ///     Holds the hotkey modifiers (bitfield)
    /// </summary>
    public ModifierKeys Modifiers { get; }

    /// <summary>
    ///     Create the attribute.
    /// </summary>
    /// <param name="key"></param>
    public HotkeyActionAttribute(Key key)
    {
        Key = key;
        Modifiers = ModifierKeys.None;
    }

    /// <summary>
    ///     Creates the attribute.
    /// </summary>
    /// <param name="key"></param>
    /// <param name="modifiers"></param>
    public HotkeyActionAttribute(Key key, ModifierKeys modifiers)
    {
        Key = key;
        Modifiers = modifiers;
    }
}