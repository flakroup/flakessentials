﻿using System.Windows;

namespace FlakEssentials.WpfEx;

/// <summary>
///     Binding proxy.
/// </summary>
public class BindingProxy : Freezable
{
    /// <summary>
    ///     The data dependency property.
    /// </summary>
    public static readonly DependencyProperty DataProperty = DependencyProperty.Register(nameof(Data),
        typeof(object),
        typeof(BindingProxy),
        new UIPropertyMetadata(null));

    /// <summary>
    ///     Gets or sets the data.
    /// </summary>
    public object Data
    {
        get => GetValue(DataProperty);
        set => SetValue(DataProperty, value);
    }

    /// <summary>
    ///     When implemented in a derived class, creates a new instance of the Freezable derived class.
    /// </summary>
    protected override Freezable CreateInstanceCore() => new BindingProxy();
}