﻿using FlakEssentials.WpfEx.Attributes;

namespace FlakEssentials.WpfEx.Enums;

/// <summary>
///     https://getbootstrap.com/docs/3.3/components/#glyphicons-glyphs
/// </summary>
public enum GlyphiconsHalflings
{
    [IconDescriptor('\u0028')] GlyphiconTMDb,

    [IconDescriptor('\u0029')] GlyphiconCast,

    [IconDescriptor('\u002a')] GlyphiconAsterisk,

    [IconDescriptor('\u002b')] GlyphiconPlus,

    [IconDescriptor('\u20ac')] GlyphiconEuro,

    [IconDescriptor('\u2212')] GlyphiconMinus,

    [IconDescriptor('\u2601')] GlyphiconCloud,

    [IconDescriptor('\u2709')] GlyphiconEnvelope,

    [IconDescriptor('\u270f')] GlyphiconPencil,

    [IconDescriptor('\ue001')] GlyphiconGlass,

    [IconDescriptor('\ue002')] GlyphiconMusic,

    [IconDescriptor('\ue003')] GlyphiconSearch,

    [IconDescriptor('\ue005')] GlyphiconHeart,

    [IconDescriptor('\ue006')] GlyphiconStar,

    [IconDescriptor('\ue007')] GlyphiconStarEmpty,

    [IconDescriptor('\ue008')] GlyphiconUser,

    [IconDescriptor('\ue009')] GlyphiconFilm,

    [IconDescriptor('\ue010')] GlyphiconThLarge,

    [IconDescriptor('\ue011')] GlyphiconTh,

    [IconDescriptor('\ue012')] GlyphiconThList,

    [IconDescriptor('\ue013')] GlyphiconOk,

    [IconDescriptor('\ue014')] GlyphiconRemove,

    [IconDescriptor('\ue015')] GlyphiconZoomIn,

    [IconDescriptor('\ue016')] GlyphiconZoomOut,

    [IconDescriptor('\ue017')] GlyphiconOff,

    [IconDescriptor('\ue018')] GlyphiconSignal,

    [IconDescriptor('\ue019')] GlyphiconCog,

    [IconDescriptor('\ue020')] GlyphiconTrash,

    [IconDescriptor('\ue021')] GlyphiconHome,

    [IconDescriptor('\ue022')] GlyphiconFile,

    [IconDescriptor('\ue023')] GlyphiconTime,

    [IconDescriptor('\ue024')] GlyphiconRoad,

    [IconDescriptor('\ue025')] GlyphiconDownloadAlt,

    [IconDescriptor('\ue026')] GlyphiconDownload,

    [IconDescriptor('\ue027')] GlyphiconUpload,

    [IconDescriptor('\ue028')] GlyphiconInbox,

    [IconDescriptor('\ue029')] GlyphiconPlayCircle,

    [IconDescriptor('\ue030')] GlyphiconRepeat,

    [IconDescriptor('\ue031')] GlyphiconRefresh,

    [IconDescriptor('\ue032')] GlyphiconListAlt,

    [IconDescriptor('\ue033')] GlyphiconLock,

    [IconDescriptor('\ue034')] GlyphiconFlag,

    [IconDescriptor('\ue035')] GlyphiconHeadphones,

    [IconDescriptor('\ue036')] GlyphiconVolumeOff,

    [IconDescriptor('\ue037')] GlyphiconVolumeDown,

    [IconDescriptor('\ue038')] GlyphiconVolumeUp,

    [IconDescriptor('\ue039')] GlyphiconQrcode,

    [IconDescriptor('\ue040')] GlyphiconBarcode,

    [IconDescriptor('\ue041')] GlyphiconTag,

    [IconDescriptor('\ue042')] GlyphiconTags,

    [IconDescriptor('\ue043')] GlyphiconBook,

    [IconDescriptor('\ue044')] GlyphiconBookmark,

    [IconDescriptor('\ue045')] GlyphiconPrint,

    [IconDescriptor('\ue046')] GlyphiconCamera,

    [IconDescriptor('\ue047')] GlyphiconFont,

    [IconDescriptor('\ue048')] GlyphiconBold,

    [IconDescriptor('\ue049')] GlyphiconItalic,

    [IconDescriptor('\ue050')] GlyphiconTextHeight,

    [IconDescriptor('\ue051')] GlyphiconTextWidth,

    [IconDescriptor('\ue052')] GlyphiconAlignLeft,

    [IconDescriptor('\ue053')] GlyphiconAlignCenter,

    [IconDescriptor('\ue054')] GlyphiconAlignRight,

    [IconDescriptor('\ue055')] GlyphiconAlignJustify,

    [IconDescriptor('\ue056')] GlyphiconList,

    [IconDescriptor('\ue057')] GlyphiconIndentLeft,

    [IconDescriptor('\ue058')] GlyphiconIndentRight,

    [IconDescriptor('\ue059')] GlyphiconFacetimeVideo,

    [IconDescriptor('\ue060')] GlyphiconPicture,

    [IconDescriptor('\ue062')] GlyphiconMapMarker,

    [IconDescriptor('\ue063')] GlyphiconAdjust,

    [IconDescriptor('\ue064')] GlyphiconTint,

    [IconDescriptor('\ue065')] GlyphiconEdit,

    [IconDescriptor('\ue066')] GlyphiconShare,

    [IconDescriptor('\ue067')] GlyphiconCheck,

    [IconDescriptor('\ue068')] GlyphiconMove,

    [IconDescriptor('\ue069')] GlyphiconStepBackward,

    [IconDescriptor('\ue070')] GlyphiconFastBackward,

    [IconDescriptor('\ue071')] GlyphiconBackward,

    [IconDescriptor('\ue072')] GlyphiconPlay,

    [IconDescriptor('\ue073')] GlyphiconPause,

    [IconDescriptor('\ue074')] GlyphiconStop,

    [IconDescriptor('\ue075')] GlyphiconForward,

    [IconDescriptor('\ue076')] GlyphiconFastForward,

    [IconDescriptor('\ue077')] GlyphiconStepForward,

    [IconDescriptor('\ue078')] GlyphiconEject,

    [IconDescriptor('\ue079')] GlyphiconChevronLeft,

    [IconDescriptor('\ue080')] GlyphiconChevronRight,

    [IconDescriptor('\ue081')] GlyphiconPlusSign,

    [IconDescriptor('\ue082')] GlyphiconMinusSign,

    [IconDescriptor('\ue083')] GlyphiconRemoveSign,

    [IconDescriptor('\ue084')] GlyphiconOkSign,

    [IconDescriptor('\ue085')] GlyphiconQuestionSign,

    [IconDescriptor('\ue086')] GlyphiconInfoSign,

    [IconDescriptor('\ue087')] GlyphiconScreenshot,

    [IconDescriptor('\ue088')] GlyphiconRemoveCircle,

    [IconDescriptor('\ue089')] GlyphiconOkCircle,

    [IconDescriptor('\ue090')] GlyphiconBanCircle,

    [IconDescriptor('\ue091')] GlyphiconArrowLeft,

    [IconDescriptor('\ue092')] GlyphiconArrowRight,

    [IconDescriptor('\ue093')] GlyphiconArrowUp,

    [IconDescriptor('\ue094')] GlyphiconArrowDown,

    [IconDescriptor('\ue095')] GlyphiconShareAlt,

    [IconDescriptor('\ue096')] GlyphiconResizeFull,

    [IconDescriptor('\ue097')] GlyphiconResizeSmall,

    [IconDescriptor('\ue101')] GlyphiconExclamationSign,

    [IconDescriptor('\ue102')] GlyphiconGift,

    [IconDescriptor('\ue103')] GlyphiconLeaf,

    [IconDescriptor('\ue104')] GlyphiconFire,

    [IconDescriptor('\ue105')] GlyphiconEyeOpen,

    [IconDescriptor('\ue106')] GlyphiconEyeClose,

    [IconDescriptor('\ue107')] GlyphiconWarningSign,

    [IconDescriptor('\ue108')] GlyphiconPlane,

    [IconDescriptor('\ue109')] GlyphiconCalendar,

    [IconDescriptor('\ue110')] GlyphiconRandom,

    [IconDescriptor('\ue111')] GlyphiconComment,

    [IconDescriptor('\ue112')] GlyphiconMagnet,

    [IconDescriptor('\ue113')] GlyphiconChevronUp,

    [IconDescriptor('\ue114')] GlyphiconChevronDown,

    [IconDescriptor('\ue115')] GlyphiconRetweet,

    [IconDescriptor('\ue116')] GlyphiconShoppingCart,

    [IconDescriptor('\ue117')] GlyphiconFolderClose,

    [IconDescriptor('\ue118')] GlyphiconFolderOpen,

    [IconDescriptor('\ue119')] GlyphiconResizeVertical,

    [IconDescriptor('\ue120')] GlyphiconResizeHorizontal,

    [IconDescriptor('\ue121')] GlyphiconHdd,

    [IconDescriptor('\ue122')] GlyphiconBullhorn,

    [IconDescriptor('\ue123')] GlyphiconBell,

    [IconDescriptor('\ue124')] GlyphiconCertificate,

    [IconDescriptor('\ue125')] GlyphiconThumbsUp,

    [IconDescriptor('\ue126')] GlyphiconThumbsDown,

    [IconDescriptor('\ue127')] GlyphiconHandRight,

    [IconDescriptor('\ue128')] GlyphiconHandLeft,

    [IconDescriptor('\ue129')] GlyphiconHandUp,

    [IconDescriptor('\ue130')] GlyphiconHandDown,

    [IconDescriptor('\ue131')] GlyphiconCircleArrowRight,

    [IconDescriptor('\ue132')] GlyphiconCircleArrowLeft,

    [IconDescriptor('\ue133')] GlyphiconCircleArrowUp,

    [IconDescriptor('\ue134')] GlyphiconCircleArrowDown,

    [IconDescriptor('\ue135')] GlyphiconGlobe,

    [IconDescriptor('\ue136')] GlyphiconWrench,

    [IconDescriptor('\ue137')] GlyphiconTasks,

    [IconDescriptor('\ue138')] GlyphiconFilter,

    [IconDescriptor('\ue139')] GlyphiconBriefcase,

    [IconDescriptor('\ue140')] GlyphiconFullscreen,

    [IconDescriptor('\ue141')] GlyphiconDashboard,

    [IconDescriptor('\ue142')] GlyphiconPaperclip,

    [IconDescriptor('\ue143')] GlyphiconHeartEmpty,

    [IconDescriptor('\ue144')] GlyphiconLink,

    [IconDescriptor('\ue145')] GlyphiconPhone,

    [IconDescriptor('\ue146')] GlyphiconPushpin,

    [IconDescriptor('\ue148')] GlyphiconUsd,

    [IconDescriptor('\ue149')] GlyphiconGbp,

    [IconDescriptor('\ue150')] GlyphiconSort,

    [IconDescriptor('\ue151')] GlyphiconSortByAlphabet,

    [IconDescriptor('\ue152')] GlyphiconSortByAlphabetAlt,

    [IconDescriptor('\ue153')] GlyphiconSortByOrder,

    [IconDescriptor('\ue154')] GlyphiconSortByOrderAlt,

    [IconDescriptor('\ue155')] GlyphiconSortByAttributes,

    [IconDescriptor('\ue156')] GlyphiconSortByAttributesAlt,

    [IconDescriptor('\ue157')] GlyphiconUnchecked,

    [IconDescriptor('\ue158')] GlyphiconExpand,

    [IconDescriptor('\ue159')] GlyphiconCollapseDown,

    [IconDescriptor('\ue160')] GlyphiconCollapseUp,

    [IconDescriptor('\ue161')] GlyphiconLogIn,

    [IconDescriptor('\ue162')] GlyphiconFlash,

    [IconDescriptor('\ue163')] GlyphiconLogOut,

    [IconDescriptor('\ue164')] GlyphiconNewWindow,

    [IconDescriptor('\ue165')] GlyphiconRecord,

    [IconDescriptor('\ue166')] GlyphiconSave,

    [IconDescriptor('\ue167')] GlyphiconOpen,

    [IconDescriptor('\ue168')] GlyphiconSaved,

    [IconDescriptor('\ue169')] GlyphiconImport,

    [IconDescriptor('\ue170')] GlyphiconExport,

    [IconDescriptor('\ue171')] GlyphiconSend,

    [IconDescriptor('\ue172')] GlyphiconFloppyDisk,

    [IconDescriptor('\ue173')] GlyphiconFloppySaved,

    [IconDescriptor('\ue174')] GlyphiconFloppyRemove,

    [IconDescriptor('\ue175')] GlyphiconFloppySave,

    [IconDescriptor('\ue176')] GlyphiconFloppyOpen,

    [IconDescriptor('\ue177')] GlyphiconCreditCard,

    [IconDescriptor('\ue178')] GlyphiconTransfer,

    [IconDescriptor('\ue179')] GlyphiconCutlery,

    [IconDescriptor('\ue180')] GlyphiconHeader,

    [IconDescriptor('\ue181')] GlyphiconCompressed,

    [IconDescriptor('\ue182')] GlyphiconEarphone,

    [IconDescriptor('\ue183')] GlyphiconPhoneAlt,

    [IconDescriptor('\ue184')] GlyphiconTower,

    [IconDescriptor('\ue185')] GlyphiconStats,

    [IconDescriptor('\ue186')] GlyphiconSdVideo,

    [IconDescriptor('\ue187')] GlyphiconHdVideo,

    [IconDescriptor('\ue188')] GlyphiconSubtitles,

    [IconDescriptor('\ue189')] GlyphiconSoundStereo,

    [IconDescriptor('\ue190')] GlyphiconSoundDolby,

    [IconDescriptor('\ue191')] GlyphiconSound51,

    [IconDescriptor('\ue192')] GlyphiconSound61,

    [IconDescriptor('\ue193')] GlyphiconSound71,

    [IconDescriptor('\ue194')] GlyphiconCopyrightMark,

    [IconDescriptor('\ue195')] GlyphiconRegistrationMark,

    [IconDescriptor('\ue197')] GlyphiconCloudDownload,

    [IconDescriptor('\ue198')] GlyphiconCloudUpload,

    [IconDescriptor('\ue199')] GlyphiconTreeConifer,

    [IconDescriptor('\ue200')] GlyphiconTreeDeciduous,

    [IconDescriptor('\ue201')] GlyphiconCd,

    [IconDescriptor('\ue202')] GlyphiconSaveFile,

    [IconDescriptor('\ue203')] GlyphiconOpenFile,

    [IconDescriptor('\ue204')] GlyphiconLevelUp,

    [IconDescriptor('\ue205')] GlyphiconCopy,

    [IconDescriptor('\ue206')] GlyphiconPaste,

    [IconDescriptor('\ue209')] GlyphiconAlert,

    [IconDescriptor('\ue210')] GlyphiconEqualizer,

    [IconDescriptor('\ue211')] GlyphiconKing,

    [IconDescriptor('\ue212')] GlyphiconQueen,

    [IconDescriptor('\ue213')] GlyphiconPawn,

    [IconDescriptor('\ue214')] GlyphiconBishop,

    [IconDescriptor('\ue215')] GlyphiconKnight,

    [IconDescriptor('\ue216')] GlyphiconBabyFormula,

    [IconDescriptor('\u26fa')] GlyphiconTent,

    [IconDescriptor('\ue218')] GlyphiconBlackboard,

    [IconDescriptor('\ue219')] GlyphiconBed,

    [IconDescriptor('\uf8ff')] GlyphiconApple,

    [IconDescriptor('\ue221')] GlyphiconErase,

    [IconDescriptor('\u231b')] GlyphiconHourglass,

    [IconDescriptor('\ue223')] GlyphiconLamp,

    [IconDescriptor('\ue224')] GlyphiconDuplicate,

    [IconDescriptor('\ue225')] GlyphiconPiggyBank,

    [IconDescriptor('\ue226')] GlyphiconScissors,

    [IconDescriptor('\ue227')] GlyphiconBitcoin,

    [IconDescriptor('\ue227')] GlyphiconBtc,

    [IconDescriptor('\ue227')] GlyphiconXbt,

    [IconDescriptor('\u00a5')] GlyphiconYen,

    [IconDescriptor('\u00a5')] GlyphiconJpy,

    [IconDescriptor('\u20bd')] GlyphiconRuble,

    [IconDescriptor('\u20bd')] GlyphiconRub,

    [IconDescriptor('\ue230')] GlyphiconScale,

    [IconDescriptor('\ue231')] GlyphiconIceLolly,

    [IconDescriptor('\ue232')] GlyphiconIceLollyTasted,

    [IconDescriptor('\ue233')] GlyphiconEducation,

    [IconDescriptor('\ue234')] GlyphiconOptionHorizontal,

    [IconDescriptor('\ue235')] GlyphiconOptionVertical,

    [IconDescriptor('\ue236')] GlyphiconMenuHamburger,

    [IconDescriptor('\ue237')] GlyphiconModalWindow,

    [IconDescriptor('\ue238')] GlyphiconOil,

    [IconDescriptor('\ue239')] GlyphiconGrain,

    [IconDescriptor('\ue240')] GlyphiconSunglasses,

    [IconDescriptor('\ue241')] GlyphiconTextSize,

    [IconDescriptor('\ue242')] GlyphiconTextColor,

    [IconDescriptor('\ue243')] GlyphiconTextBackground,

    [IconDescriptor('\ue244')] GlyphiconObjectAlignTop,

    [IconDescriptor('\ue245')] GlyphiconObjectAlignBottom,

    [IconDescriptor('\ue246')] GlyphiconObjectAlignHorizontal,

    [IconDescriptor('\ue247')] GlyphiconObjectAlignLeft,

    [IconDescriptor('\ue248')] GlyphiconObjectAlignVertical,

    [IconDescriptor('\ue249')] GlyphiconObjectAlignRight,

    [IconDescriptor('\ue250')] GlyphiconTriangleRight,

    [IconDescriptor('\ue251')] GlyphiconTriangleLeft,

    [IconDescriptor('\ue252')] GlyphiconTriangleBottom,

    [IconDescriptor('\ue253')] GlyphiconTriangleTop,

    [IconDescriptor('\ue254')] GlyphiconConsole,

    [IconDescriptor('\ue255')] GlyphiconSuperscript,

    [IconDescriptor('\ue256')] GlyphiconSubscript,

    [IconDescriptor('\ue257')] GlyphiconMenuLeft,

    [IconDescriptor('\ue258')] GlyphiconMenuRight,

    [IconDescriptor('\ue259')] GlyphiconMenuDown,

    [IconDescriptor('\ue260')] GlyphiconMenuUp
}