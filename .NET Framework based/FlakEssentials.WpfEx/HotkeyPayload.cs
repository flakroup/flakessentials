﻿using FEx.MVVM.Abstractions;
using System.Windows.Input;

namespace FlakEssentials.WpfEx;

/// <summary>
///     Payload that carries a key gesture that originates from a specific view.
/// </summary>
public class HotkeyPayload
{
    /// <summary>
    ///     The key gesture.
    /// </summary>
    public KeyGesture KeyGesture { get; }

    /// <summary>
    ///     The view where the key gesture originates from.
    /// </summary>
    public ViewModelBase Origin { get; }

    /// <summary>
    ///     Creates a payload.
    /// </summary>
    /// <param name="keyGesture"></param>
    /// <param name="origin"></param>
    /// <returns></returns>
    public HotkeyPayload(KeyGesture keyGesture, ViewModelBase origin)
    {
        Origin = origin;
        KeyGesture = keyGesture;
    }
}