﻿using FEx.WPFx.Services;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Threading;

namespace FlakEssentials.WpfEx.Extensions;

/// <summary>
///     Dependency object extensions.
/// </summary>
public static class DependencyObjectExtensions
{
    /// <summary>
    ///     Gets the view model from given sender.
    /// </summary>
    /// <typeparam name="TViewModel">The type of the view model.</typeparam>
    /// <param name="view">The sender view.</param>
    /// <returns>
    ///     View model.
    /// </returns>
    public static TViewModel GetViewModel<TViewModel>(this FrameworkElement view) => view.InvokeOnDispatcherContext(() => (TViewModel)view.DataContext);

    /// <summary>
    ///     Checks if action should be invoked by dispatcher, or directly and runs it.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="action">The action to run.</param>
    /// <param name="priority">The priority.</param>
    public static void InvokeOnDispatcherContext(this DependencyObject sender,
                                                 Action action,
                                                 DispatcherPriority priority = DispatcherPriority.Send) => DispatcherService.InvokeOnDispatcherContext(action, sender, priority);

    /// <summary>
    ///     Checks if action should be invoked by dispatcher, or directly and runs it.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="action">The action to run.</param>
    /// <param name="priority">The priority.</param>
    /// <returns></returns>
    public static async Task InvokeOnDispatcherContextAsync(this DependencyObject sender,
                                                            Action action,
                                                            DispatcherPriority priority = DispatcherPriority.Send) => await DispatcherService.InvokeOnDispatcherContextAsync(action, sender, priority);

    /// <summary>
    ///     Checks if action should be invoked by dispatcher, or directly and runs it.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="action">The action to run.</param>
    /// <param name="priority">The priority.</param>
    /// <returns></returns>
    public static async Task<T> InvokeOnDispatcherContextAsync<T>(this DependencyObject sender,
                                                                  Func<T> action,
                                                                  DispatcherPriority priority =
                                                                      DispatcherPriority.Send) =>
        await DispatcherService.InvokeOnDispatcherContextAsync(action, sender, priority);

    /// <summary>
    ///     Checks if action should be invoked by dispatcher, or directly and runs it.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="sender">The sender.</param>
    /// <param name="action">The action to run.</param>
    /// <param name="priority">The priority.</param>
    /// <returns></returns>
    public static T InvokeOnDispatcherContext<T>(this DependencyObject sender,
                                                 Func<T> action,
                                                 DispatcherPriority priority = DispatcherPriority.Send) =>
        DispatcherService.InvokeOnDispatcherContext(action, sender, priority);

    public static void DoUpdateSource(this DependencyProperty property, object source)
    {
        if (property is not null)
        {
            var elt = source as UIElement;

            if (elt is not null)
            {
                BindingExpression binding = BindingOperations.GetBindingExpression(elt, property);

                binding?.UpdateSource();
            }
        }
    }
}