﻿using FEx.Abstractions;
using System;
using System.Windows;

namespace FlakEssentials.WpfEx.Extensions;

public static class UIElementExtensions
{
    public static void ExecuteActionOnUIElement<T>(this T element, Action action, bool inUIContext = false)
        where T : UIElement
    {
        if (element is null
            || action is null)
            return;

        if (!inUIContext)
            action();
        else
            FExFoundation.Dispatcher.InvokeOnIdleMainThread(action);
    }

    public static void EnableUIElement(this UIElement element, bool inUIContext = false) =>
        element?.ExecuteActionOnUIElement(() => element.IsEnabled = true, inUIContext);

    public static void DisableUIElement(this UIElement element, bool inUIContext = false) =>
        element?.ExecuteActionOnUIElement(() => element.IsEnabled = false, inUIContext);
}