using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;
#if ISNETFULL
using System.Security.Permissions;
#endif

namespace FlakEssentials.WpfEx;

/// <summary>
///     Class for WPF related common methods.
/// </summary>
public static class WpfCommon
{
    /// <summary>
    ///     Determines if the Application is in design mode.
    /// </summary>
    public static bool IsInDesignMode =>
        Application.Current is null
        || (bool)DesignerProperties.IsInDesignModeProperty.GetMetadata(typeof(DependencyObject)).DefaultValue;

    /// <summary>
    ///     This method is WPF equivalent of WinForms <see cref="System.Windows.Forms.Application.DoEvents()" />.
    /// </summary>
#if ISNETFULL
    [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
#endif
    public static void DoEvents()
    {
        try
        {
            var frame = new DispatcherFrame();

            Dispatcher.CurrentDispatcher.Invoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(ExitFrame),
                frame);

            Dispatcher.PushFrame(frame);
        }
        catch
        {
            // ignored
        }
    }

    /// <summary>
    ///     Exits the frame.
    /// </summary>
    /// <param name="frame">The f.</param>
    /// <returns><see cref="System.Object" />.</returns>
    public static object ExitFrame(object frame)
    {
        ((DispatcherFrame)frame).Continue = false;

        return null;
    }

    //private static async Task GetFocusedElementAsync()
    //{
    //    while (Application.Current is not null)
    //    {
    //        DispatcherService.InvokeOnDispatcherContext(() =>
    //          {
    //              IInputElement focusedControl = Keyboard.FocusedElement;
    //              yield return focusedControl is not null ? $"{focusedControl.GetType()} {focusedControl.IsMouseDirectlyOver}" : string.Empty;
    //          });
    //        await Task.Delay(100);
    //    }
    //}//todo convert to observable
}