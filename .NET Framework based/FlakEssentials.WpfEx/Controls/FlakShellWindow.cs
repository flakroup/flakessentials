﻿using FlakEssentials.WpfEx.ViewModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FlakEssentials.WpfEx.Controls;

public class FlakShellWindow<TViewModel> : FlakWindow<TViewModel> where TViewModel : WpfProgressListenerViewModel
{
    /// <summary>
    ///     Initializes a new instance of the <see cref="FlakShellWindow{TViewModel}" /> class.
    /// </summary>
    public FlakShellWindow()
    {
        if (!IsInDesignMode)
        {
            CommandBindings.Add(new(SystemCommands.CloseWindowCommand, OnCloseWindow));

            CommandBindings.Add(new(SystemCommands.MaximizeWindowCommand, OnMaximizeWindow, OnCanResizeWindow));

            CommandBindings.Add(new(SystemCommands.MinimizeWindowCommand, OnMinimizeWindow, OnCanMinimizeWindow));

            CommandBindings.Add(new(SystemCommands.RestoreWindowCommand, OnRestoreWindow, OnCanResizeWindow));

            CommandBindings.Add(new(SystemCommands.ShowSystemMenuCommand, OnSystemMenu, OnCanSystemMenu));
        }
    }

    /// <summary>
    ///     Occurs on CanResizeWindow event.
    /// </summary>
    /// <param name="sender">Sender of that event</param>
    /// <param name="e">Arguments related to this event</param>
    private void OnCanResizeWindow(object sender, CanExecuteRoutedEventArgs e) => e.CanExecute = ResizeMode is ResizeMode.CanResize or ResizeMode.CanResizeWithGrip;

    /// <summary>
    ///     Occurs on CanMinimizeWindow event.
    /// </summary>
    /// <param name="sender">Sender of that event</param>
    /// <param name="e">Arguments related to this event</param>
    private void OnCanMinimizeWindow(object sender, CanExecuteRoutedEventArgs e) => e.CanExecute = ResizeMode != ResizeMode.NoResize;

    /// <summary>
    ///     Occurs on CanSystemMenu event.
    /// </summary>
    /// <param name="sender">Sender of that event</param>
    /// <param name="e">Arguments related to this event</param>
    private static void OnCanSystemMenu(object sender, CanExecuteRoutedEventArgs e) => e.CanExecute = true;

    /// <summary>
    ///     Occurs on CloseWindow event.
    /// </summary>
    /// <param name="target">Target of that event</param>
    /// <param name="e">Arguments related to this event</param>
    private void OnCloseWindow(object target, ExecutedRoutedEventArgs e) => SystemCommands.CloseWindow(this);

    /// <summary>
    ///     Occurs on MaximizeWindow event.
    /// </summary>
    /// <param name="target">Target of that event</param>
    /// <param name="e">Arguments related to this event</param>
    private void OnMaximizeWindow(object target, ExecutedRoutedEventArgs e) => SystemCommands.MaximizeWindow(this);

    /// <summary>
    ///     Occurs on MinimizeWindow event.
    /// </summary>
    /// <param name="target">Target of that event</param>
    /// <param name="e">Arguments related to this event</param>
    private void OnMinimizeWindow(object target, ExecutedRoutedEventArgs e) => SystemCommands.MinimizeWindow(this);

    /// <summary>
    ///     Occurs on RestoreWindow event.
    /// </summary>
    /// <param name="target">Target of that event</param>
    /// <param name="e">Arguments related to this event</param>
    private void OnRestoreWindow(object target, ExecutedRoutedEventArgs e) => SystemCommands.RestoreWindow(this);

    /// <summary>
    ///     Occurs on SystemMenu event.
    /// </summary>
    /// <param name="target">Target of that event</param>
    /// <param name="e">Arguments related to this event</param>
    private void OnSystemMenu(object target, ExecutedRoutedEventArgs e)
    {
        var borderHeight = 0;
        var borderWidth = 0;

        if (Template.FindName("WindowsBorder", this) is Border border)
        {
            borderHeight += (int)border.Margin.Top;
            borderWidth += (int)border.Margin.Left;
        }

        if (Template.FindName("InnerBorder", this) is Border inner)
        {
            borderHeight += (int)inner.BorderThickness.Top;
            borderWidth += (int)inner.BorderThickness.Left;
        }

        var titleHeight = 0;

        if (Template.FindName("LayoutRoot", this) is Grid { RowDefinitions.Count: > 0 } borderGrid)
            titleHeight = (int)borderGrid.RowDefinitions[0].Height.Value;

        var currentPosition = new Point(Left + borderWidth, Top + borderHeight + titleHeight);
        SystemCommands.ShowSystemMenu(this, currentPosition);
    }
}