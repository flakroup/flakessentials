﻿using FEx.Abstractions;
using FEx.Basics.Extensions;
using FEx.Extensions.Collections.Dictionaries;
using FEx.Legacy.Asyncx.Enums;
using FEx.Legacy.Mvvm.Abstractions.Interfaces;
using FEx.MVVM;
using FEx.WPFx;
using FEx.WPFx.Extensions;
using FlakEssentials.WpfEx.Extensions;
using FlakEssentials.WpfEx.ViewModels;
using ReactiveUI;
using System;
using System.ComponentModel;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;

namespace FlakEssentials.WpfEx.Controls;

public class FlakWindow<TViewModel> : Window, IViewFor<TViewModel>, IRunAsyncView
    where TViewModel : WpfProgressListenerViewModel
{
    public static DependencyProperty ViewModelProperty { get; } = DependencyProperty.Register(nameof(ViewModel),
        typeof(TViewModel),
        typeof(FlakWindow<TViewModel>));

    public bool IsInDesignMode { get; }
    public Grid MainGrid { get; protected set; }
    public Screen DisplayScreen { get; protected set; }

    public TViewModel ViewModel
    {
        get => (TViewModel)FExFoundation.Dispatcher.InvokeOnMainThread(() => GetValue(ViewModelProperty));
        set => FExFoundation.Dispatcher.InvokeOnMainThread(() => SetValue(ViewModelProperty, value));
    }

    protected double FixedWidth { get; set; }

    object IViewFor.ViewModel
    {
        get => ViewModel;
        set => ViewModel = (TViewModel)value;
    }

    /// <summary>
    ///     Initializes a new instance of the <see cref="FlakWindow" /> class.
    /// </summary>
    public FlakWindow()
    {
        IsInDesignMode = DesignerProperties.GetIsInDesignMode(this);

        if (!IsInDesignMode)
        {
            DataContextChanged += FlakWindow_DataContextChanged;
            Loaded += OnLoaded;
        }
    }

    public virtual async Task RunAsync(Action action, object sender = null, JobSpecs? specs = null) =>
        await ViewModel.RunAsync(action, specs, () => PreAction(sender), isSuccess => PostAction(sender, isSuccess));

    public virtual async Task RunTaskAsync(Func<Task> function, object sender = null, JobSpecs? specs = null) =>
        await ViewModel.RunTaskAsync(function,
            specs,
            () => PreAction(sender),
            isSuccess => PostAction(sender, isSuccess));

    public virtual async Task<TResult> RunTaskAsync<TResult>(Func<Task<TResult>> function,
                                                             object sender = null,
                                                             JobSpecs? specs = null) =>
        await ViewModel.RunTaskAsync(function,
            specs,
            () => PreAction(sender),
            isSuccess => PostAction(sender, isSuccess));

    public virtual async Task<TResult> RunFuncAsync<TResult>(Func<TResult> function,
                                                             object sender = null,
                                                             JobSpecs? specs = null) =>
        await ViewModel.RunFuncAsync(function,
            specs,
            () => PreAction(sender),
            isSuccess => PostAction(sender, isSuccess));

    /// <summary>
    ///     Handles the DataContextChanged event of the FlakWindow control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
    public void FlakWindow_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) =>
        RefreshViewModel(true);

    protected virtual void OnLoaded(object sender, RoutedEventArgs e)
    {
        FExWpfx.WindowLoaded?.Invoke(sender, e);
        this.PlaceToPrimaryMonitor();
    }

    protected virtual void PreAction(object sender) =>
        DisableUIElement(sender); // ReSharper disable UnusedParameter.Global

    protected virtual void PostAction(object sender, bool isSuccess) => EnableUIElement(sender);

    protected virtual void ReScale()
    {
        try
        {
            double height = DisplayScreen.WorkingArea.Height / (double)DisplayScreen.WorkingArea.Width * FixedWidth;
            double w = ActualWidth / FixedWidth;
            double h = ActualHeight / height;

            if (MainGrid.LayoutTransform is not ScaleTransform scaler)
            {
                // Currently no zoom, so go instantly to max zoom.
                MainGrid.LayoutTransform = new ScaleTransform(w, h);
            }
            else
            {
                if (scaler.HasAnimatedProperties)
                {
                    // Remove the animation by assigning a null
                    // AnimationTimeline to the properties.
                    // Note that this causes them to revert to
                    // their most recently assigned "local" values.

                    scaler.BeginAnimation(ScaleTransform.ScaleXProperty, null);
                    scaler.BeginAnimation(ScaleTransform.ScaleYProperty, null);
                }

                scaler.ScaleX = w;
                scaler.ScaleY = h;
                MainGrid.LayoutTransform = scaler;
            }
        }
        catch (Exception ex)
        {
            ex.HandleException(false);
        }
    }

    protected void RefreshViewModel(bool refresh = false)
    {
        if (DataContext is not TViewModel)
            throw new($"{nameof(DataContext)} must inherit {nameof(TViewModel)}");

        if (ViewModel is not null)
            ViewModel.View = null;

        if (ViewModel is null || refresh)
            ViewModel = this.GetViewModel<TViewModel>();

        ViewModel!.View = this;
    }

    protected void EnableRescaling(Grid mainGrid, double fixedWidth = 1920D)
    {
        MainGrid = mainGrid;
        DisplayScreen = this.GetWindowsScreen();
        FixedWidth = fixedWidth;

        ViewModel!.Subscriptions.ReplaceAndDisposeOldValue(nameof(MainGrid),
            () => Observable
                .FromEventPattern<SizeChangedEventHandler,
                    SizeChangedEventArgs>(h => SizeChanged += h, h => SizeChanged -= h)
                .Throttle(FExMvvm.DefaultUIRefreshInterval)
                .ObserveOn(RxApp.MainThreadScheduler)
                .Subscribe(_ => ReScale()));
    }

    private static void EnableUIElement(object sender)
    {
        if (sender is not UIElement element)
            return;

        element.EnableUIElement(true);
    }

    private static void DisableUIElement(object sender)
    {
        if (sender is not UIElement element)
            return;

        element.DisableUIElement(true);
    }
}