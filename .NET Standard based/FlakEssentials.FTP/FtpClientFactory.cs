﻿using FlakEssentials.CommonUtilities.Services;
using FlakEssentials.CommonUtilities.Services.Interfaces;
using FluentFTP;
using FluentFTP.Helpers;
using FluentFTP.Proxy;
using System;
using System.Collections.Concurrent;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace FlakEssentials.FTP
{
    public class FtpClientFactory
    {
        private readonly SemaphoreSlim _semaphore;
        public Uri HostUri { get; }
        public ICredentials ProxyCredentials { get; set; } = CredentialCache.DefaultCredentials;
        public string ProxyHost { get; set; }
        public int ProxyPort { get; set; }

        public Task<FtpClient> CreateAsync(string user = null, string pass = null, bool useProxy = false, int port = 0)
        {
            NetworkCredential credentials = user != null || pass != null
                ? new NetworkCredential(user, pass)
                : null;
            ProxyInfo proxy = GetProxy(useProxy);
            return CreateAsync(credentials, proxy, port);
        }

        public Task<FtpClient> CreateAsync(string user = null, string pass = null, ProxyInfo proxy = null, int port = 0)
        {
            NetworkCredential credentials = user != null || pass != null
                ? new NetworkCredential(user, pass)
                : null;
            return CreateAsync(credentials, proxy, port);
        }

        public Task<FtpClient> CreateAsync(NetworkCredential credentials = null, bool useProxy = false, int port = 0)
        {
            ProxyInfo proxy = GetProxy(useProxy);
            return CreateAsync(credentials, proxy, port);
        }

        public async Task<FtpClient> CreateAsync(NetworkCredential credentials = null, ProxyInfo proxy = null, int port = 0)
        {
            await _semaphore.WaitAsync();
            FtpClient client;
            if (proxy != null)
                client = new FtpClientHttp11Proxy(proxy);
            else
                client = new FtpClient();

            if (HostUri != null)
                client.Host = HostUri.AbsoluteUri;

            if (credentials != null)
                client.Credentials = credentials;

            if (port != 0)
                client.Port = port;
            //client.Encoding = Encoding.UTF8;
            //client.EncryptionMode = FtpEncryptionMode.Explicit;
            //client.SslProtocols = SslProtocols.Tls;
            //client.ValidateCertificate += OnValidateCertificate;
            //client.DataConnectionType = FtpDataConnectionType.PASV;
            FtpTrace.WriteLine($"FTPClient::ConnectionType = \'{client.ConnectionType}\'");

            return client;
        }

        public async Task ReleaseClientAsync(FtpClient client)
        {
            if (!client.IsDisposed)
            {
                if (client.IsConnected)
                    await client.DisconnectAsync();
                client.Dispose();
            }

            _semaphore.Release();
        }

        protected void OnValidateCertificate(FtpSslValidationEventArgs e)
        {
            e.Accept = true;
        }

        internal ProxyInfo GetProxy(bool useProxy)
        {
            if (useProxy)
                return new ProxyInfo
                {
                    Credentials = (NetworkCredential)ProxyCredentials,
                    Host = ProxyHost,
                    Port = ProxyPort
                };

            return null;
        }

        #region Singleton

        private static ILockService LockSrv => CommonServicesModule.LockSrv;
        private static ConcurrentDictionary<string, FtpClientFactory> Instances { get; } = new ConcurrentDictionary<string, FtpClientFactory>();

        public static async Task<FtpClientFactory> GetInstanceAsync(string hostUri, int maxParallel = 5)
        {
            SemaphoreSlim semaphore = LockSrv.EnsureLock($"{hostUri}@{nameof(FtpClientFactory)}_Instance");
            await semaphore.WaitAsync();
            FtpClientFactory res = Instances.GetOrAdd(hostUri, _ => new FtpClientFactory(hostUri, maxParallel));
            semaphore.Release();
            return res;
        }

        private FtpClientFactory(string hostUri, int maxParallel)
        {
            HostUri = new Uri(hostUri);
            var semaphoreKey = $"{HostUri.AbsoluteUri}@{nameof(FtpClientFactory)}";
            _semaphore = LockSrv.EnsureLock(semaphoreKey, maxParallel);
        }

        #endregion
    }
}