﻿using Microsoft.EntityFrameworkCore;

namespace FlakEssentials.EFCore.Interfaces;

public interface IEFCoreDatabaseBackedService<TDbContext> : IDbServiceBase<TDbContext> where TDbContext : DbContext
{
    string DbKey { get; }
}