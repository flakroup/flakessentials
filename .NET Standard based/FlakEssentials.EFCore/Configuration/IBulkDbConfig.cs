﻿using EFCore.BulkExtensions;
using System;

namespace FlakEssentials.EFCore.Configuration;

public interface IBulkDbConfig
{
    Func<BulkConfig> Config { get; }
    int ParallelBulkOperations { get; }
}