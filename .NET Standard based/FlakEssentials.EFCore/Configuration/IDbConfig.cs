﻿namespace FlakEssentials.EFCore.Configuration;

public interface IDbConfig
{
    DbServiceConfig DbConfig { get; }
}