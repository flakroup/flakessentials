using FEx.EFCore.Interfaces;

namespace FlakEssentials.EFCore.Configuration;

public interface IDbServiceConfig
{
    IBulkDbConfig BulkDbConfig { get; }
    IFExDbConfig DbConfig { get; }
}