﻿using System.ComponentModel;

// ReSharper disable CheckNamespace
namespace System.Runtime.CompilerServices;
// ReSharper restore CheckNamespace

[EditorBrowsable(EditorBrowsableState.Never)]
internal static class IsExternalInit
{
}