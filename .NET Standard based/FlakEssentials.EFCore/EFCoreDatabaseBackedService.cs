﻿using FEx.Abstractions.Interfaces;
using FEx.Asyncx.Helpers;
using FEx.Common.Extensions;
using FEx.DI.Abstractions.Interfaces;
using FEx.EFCore.Helpers;
using FEx.EFCore.Interfaces;
using FlakEssentials.EFCore.Configuration;
using FlakEssentials.EFCore.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace FlakEssentials.EFCore;

/// <summary>
/// </summary>
/// <typeparam name="TDbContext"></typeparam>
/// <remarks>Requires <c>Initialize();</c> call in .ctor</remarks>
public abstract class EFCoreDatabaseBackedService<TDbContext> : BulkDbServiceBase<TDbContext>,
    IEFCoreDatabaseBackedService<TDbContext> where TDbContext : DbContext
{
    private readonly ISqlDbHelper _dbHelper;
    public string DbKey { get; protected set; }

    protected EFCoreDatabaseBackedService(IScopeProvider scopeProvider,
                                          IDbServiceConfig config,
                                          ResilientTransaction resilientTransaction,
                                          ISqlDbHelper dbHelper,
                                          IAsyncInitializable[] dependencies)
        : base(scopeProvider, config.BulkDbConfig, resilientTransaction, config.DbConfig, dependencies)
    {
        _dbHelper = dbHelper;
    }

    protected virtual async Task EnsureIsInitializedAsync()
    {
        if (IsInitialized)
            return;

        try
        {
            await InitializeAsync();
        }
        catch
        {
            //ignored
        }
    }

    protected override async Task OnInitializeAsync()
    {
        await base.OnInitializeAsync();

        if (!_dbHelper.IsInitialized)
            await JoinableAsyncHelper.AwaitWithoutDeadlockAsync(() => _dbHelper.InitializeAsync());

        _dbConfig.SqlInstance = _dbHelper.SQLInstance.Guard(nameof(_dbHelper.SQLInstance));

        await base.OnInitializeAsync();
    }
}