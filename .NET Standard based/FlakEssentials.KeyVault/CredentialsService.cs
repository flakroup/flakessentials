﻿using FEx.Basics.Extensions;
using FEx.Extensions.IO;
using FEx.Json.Extensions;
using Newtonsoft.Json;
using System;
using System.IO;

namespace FlakEssentials.KeyVault;

public class CredentialsService
{
    private static readonly object Sync;

    public static FileInfo CredsFile { get; }
    public static Credentials Creds { get; private set; }

    static CredentialsService()
    {
        Sync = new();

        CredsFile = Environment.SpecialFolder.UserProfile.GetSpecialDirectory()
            .Directory.GetDescendantFile("_credentials.cfg");

        SetCredentials(null);
    }

    public static Credentials UpdateAndGetCredentials(Credentials newCredentials = null)
    {
        lock (Sync)
        {
            if (newCredentials is not null)
            {
                CredsFile.SerializeToFile(newCredentials, formatting: Formatting.Indented);
                CredsFile.Refresh();
            }

            return SetCredentials(newCredentials);
        }
    }

    private static Credentials SetCredentials(Credentials newCredentials)
    {
        newCredentials ??= CredsFile.Exists ? CredsFile.DeserializeFromFile<Credentials>() :
            Creds is null ? CreateNewCredentials() : null;

        if (Creds is null)
            Creds = newCredentials;
        else
            Creds.ReplaceWith(newCredentials);

        return Creds;
    }

    private static Credentials CreateNewCredentials()
    {
        var newCredentials = new Credentials();
        CredsFile.SerializeToFile(newCredentials, formatting: Formatting.Indented);
        CredsFile.Refresh();

        return newCredentials;
    }
}