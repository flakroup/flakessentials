﻿using FEx.Basics.Abstractions;
using FEx.Encryption;
using FEx.Extensions;
using Newtonsoft.Json;
using System.Net;

namespace FlakEssentials.KeyVault;

public class Credentials : PropertyChangeAware
{
    private string _password;
    private string _securePassword;
    private string _connStr;
    private string _secureConnStr;
    private string _zulipKey;
    private string _secureZulipKey;
    private string _nuGetApiKey;
    private string _secureNuGetApiKey;
    private string _flakNuGetApiKey;
    private string _secureFlakNuGetApiKey;

    public string UserName { get; set; }

    public string ZulipUser { get; set; }

    [JsonIgnore]
    public string Password
    {
        get => _password;
        set
        {
            if (SetProperty(ref _password, value))
                SecurePassword = Password.ToBase64();
        }
    }

    public string SecurePassword
    {
        get => _securePassword;
        set
        {
            if (SetProperty(ref _securePassword, value))
                Password = SecurePassword.FromBase64();
        }
    }

    [JsonIgnore]
    public string ConnStr
    {
        get => _connStr;
        set
        {
            if (SetProperty(ref _connStr, value))
                SecureConnStr = StringHasher.EncryptString(PassPhrase, ConnStr);
        }
    }

    public string SecureConnStr
    {
        get => _secureConnStr;
        set
        {
            if (SetProperty(ref _secureConnStr, value))
                ConnStr = StringHasher.DecryptString(PassPhrase, SecureConnStr);
        }
    }

    [JsonIgnore]
    public string ZulipKey
    {
        get => _zulipKey;
        set
        {
            if (SetProperty(ref _zulipKey, value))
                SecureZulipKey = StringHasher.EncryptString(PassPhrase, ZulipKey);
        }
    }

    public string SecureZulipKey
    {
        get => _secureZulipKey;
        set
        {
            if (SetProperty(ref _secureZulipKey, value))
                ZulipKey = StringHasher.DecryptString(PassPhrase, SecureZulipKey);
        }
    }

    [JsonIgnore]
    public string NuGetApiKey
    {
        get => _nuGetApiKey;
        set
        {
            if (SetProperty(ref _nuGetApiKey, value))
                SecureNuGetApiKey = StringHasher.EncryptString(PassPhrase, NuGetApiKey);
        }
    }

    public string SecureNuGetApiKey
    {
        get => _secureNuGetApiKey;
        set
        {
            if (SetProperty(ref _secureNuGetApiKey, value))
                NuGetApiKey = StringHasher.DecryptString(PassPhrase, SecureNuGetApiKey);
        }
    }

    [JsonIgnore]
    public string FlakNuGetApiKey
    {
        get => _flakNuGetApiKey;
        set
        {
            if (SetProperty(ref _flakNuGetApiKey, value))
                SecureFlakNuGetApiKey = StringHasher.EncryptString(PassPhrase, FlakNuGetApiKey);
        }
    }

    public string SecureFlakNuGetApiKey
    {
        get => _secureFlakNuGetApiKey;
        set
        {
            if (SetProperty(ref _secureFlakNuGetApiKey, value))
                FlakNuGetApiKey = StringHasher.DecryptString(PassPhrase, SecureFlakNuGetApiKey);
        }
    }

    private static string PassPhrase => FExEncryption.PassPhrase;

    public Credentials(NetworkCredential ftpCredentials, string connStr, string zulipUser, string zulipKey)
        : this(ftpCredentials.UserName, ftpCredentials.Password, connStr, zulipUser, zulipKey)
    {
    }

    public Credentials(string userName, string password, string connStr, string zulipUser, string zulipKey)
    {
        UserName = userName;
        Password = password;
        ConnStr = connStr;
        ZulipUser = zulipUser;
        ZulipKey = zulipKey;
    }

    public Credentials()
    {
    }

    public void ReplaceWith(Credentials newCredentials)
    {
        if (newCredentials is not null)
        {
            if (newCredentials.UserName is not null)
                UserName = newCredentials.UserName;

            if (newCredentials.Password is not null)
                Password = newCredentials.Password;

            if (newCredentials.SecurePassword is not null)
                SecurePassword = newCredentials.SecurePassword;

            if (newCredentials.ConnStr is not null)
                ConnStr = newCredentials.ConnStr;

            if (newCredentials.SecureConnStr is not null)
                SecureConnStr = newCredentials.SecureConnStr;

            if (newCredentials.ZulipUser is not null)
                ZulipUser = newCredentials.ZulipUser;

            if (newCredentials.ZulipKey is not null)
                ZulipKey = newCredentials.ZulipKey;

            if (newCredentials.SecureZulipKey is not null)
                SecureZulipKey = newCredentials.SecureZulipKey;

            if (newCredentials.NuGetApiKey is not null)
                NuGetApiKey = newCredentials.NuGetApiKey;

            if (newCredentials.SecureNuGetApiKey is not null)
                SecureNuGetApiKey = newCredentials.SecureNuGetApiKey;
        }
    }
}