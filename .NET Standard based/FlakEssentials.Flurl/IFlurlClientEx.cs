﻿using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace FlakEssentials.Flurl;

public interface IFlurlClientEx : IFlurlClient
{
    bool IsBusy { get; }
    Task<IFlurlClientEx> CurrentTask { get; }

    Task SetBusyAsync(bool value);
    Task<T> ExecuteAsync<T>(Func<IFlurlClient, Task<T>> func, Action<IFlurlClient, T> postAction = null);
    void ApplyCookies(IDictionary<string, Cookie> cookieJar);
}