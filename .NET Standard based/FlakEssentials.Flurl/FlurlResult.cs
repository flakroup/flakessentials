﻿namespace FlakEssentials.Flurl;

public class FlurlResult<T> : IFlurlResult
{
    public T Data { get; }

    public FlurlResult(T data)
    {
        Data = data;
    }
}