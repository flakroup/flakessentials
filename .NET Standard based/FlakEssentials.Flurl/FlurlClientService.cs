﻿using FEx.Asyncx.Helpers;
using FEx.Asyncx.Services;
using Flurl;
using Flurl.Http;
using Flurl.Http.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace FlakEssentials.Flurl;

public abstract class
    FlurlClientService : AsyncWorkersService<FlurlClientPool, FlurlClientEx, IFlurlResult, FlurlConfig>
{
    public Uri BaseUri
    {
        get => Config.BaseUri;
        protected set => Config.BaseUri = value;
    }

    protected Action<FlurlHttpSettings> Cfg
    {
        get => Config.Cfg;
        set => Config.Cfg = value;
    }

    protected FlurlClientService(Uri url, int poolSize, Action<FlurlHttpSettings> action = null)
        : base(poolSize, new(url, action))
    {
    }

    public async Task<T> RunFlurlFuncAsync<T>(string path,
                                              Func<IFlurlRequest, IFlurlRequest> requestFunc,
                                              Func<IFlurlClient, Task<T>> func,
                                              Action<IFlurlClient, T> postAction = null)
    {
        Url url = GetResponseUrl(path, requestFunc);

        IFlurlResult result =
            await Pool.ExecuteOnPoolAsync((c, id) => c.RunFuncAsync(id, func, postAction), guid => GetId(guid, url));

        return ((FlurlResult<T>)result).Data;
    }

    public async Task WaitForAllClientsAsync()
    {
        if (Pool is not null)
            await Pool?.WaitForAllClientsAsync();
    }

    public async Task WaitForAllRequestsAsync()
    {
        if (Pool is not null)
            await Pool.WaitForAllRequestsAsync();
    }

    public Url GetResponseUrl(string path, Func<IFlurlRequest, IFlurlRequest> requestFunc) =>
        Pool.IdleWorker.GetResponseUrl(path, requestFunc);

    public void SetPoolLogger(ILogger<AsyncQueue<string, IFlurlResult>> logger)
    {
        Pool.SetQueueLogger(logger);
    }

    protected override FlurlClientPool GetNewPool(int poolSize) =>
        BaseUri is not null
            ? new FlurlClientPool(BaseUri, poolSize, Cfg)
            : null;

    protected async Task SetupPoolAsync(int poolSize, Uri baseUri = null, Action<FlurlHttpSettings> cfg = null)
    {
        await WaitForAllClientsAsync();

        if (baseUri is not null)
            BaseUri = baseUri;

        if (cfg is not null)
            Cfg = cfg;

        Pool?.Dispose();
        Pool = GetNewPool(poolSize);
        await InitializeAsync();
    }

    private static string GetId(Guid guid, Url url) => $"{guid}_{url}";
}