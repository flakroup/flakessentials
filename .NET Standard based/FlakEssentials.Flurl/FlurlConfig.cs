﻿using FEx.Asyncx.Interfaces;
using Flurl.Http.Configuration;
using System;

namespace FlakEssentials.Flurl;

public class FlurlConfig : IAsyncWorkerConfig
{
    public Uri BaseUri { get; set; }
    public Action<FlurlHttpSettings> Cfg { get; set; }

    public FlurlConfig(Uri url, Action<FlurlHttpSettings> action = null)
    {
        BaseUri = url;
        Cfg = action;
    }
}