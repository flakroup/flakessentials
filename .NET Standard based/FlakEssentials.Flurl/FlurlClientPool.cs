﻿using FEx.Asyncx.Helpers;
using FEx.Asyncx.Services;
using Flurl.Http.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace FlakEssentials.Flurl;

public class FlurlClientPool : AsyncWorkersPool<FlurlClientEx, IFlurlResult>
{
    private bool _isDisposed;

    public Uri BaseUri { get; protected set; }
    protected Action<FlurlHttpSettings> Cfg { get; set; }

    public FlurlClientPool(Uri url, int poolSize, Action<FlurlHttpSettings> action = null)
        : base(poolSize)
    {
        BaseUri = url;
        Cfg = action;
    }

    public async Task WaitForAllRequestsAsync()
    {
        await Queue.WaitForAllRunningJobsAsync();
    }

    public void SetQueueLogger(ILogger<AsyncQueue<string, IFlurlResult>> logger)
    {
        Queue.SetLogger(logger);
    }

    protected override FlurlClientEx GetNewWorker(int id) => GetNewClient(id, BaseUri, Cfg);

    private static FlurlClientEx GetNewClient(int id, Uri url, Action<FlurlHttpSettings> action)
    {
        var client = new FlurlClientEx(id, url);

        if (action is not null)
            client.Configure(action);

        return client;
    }

    #region IDisposable
    protected override void Dispose(bool disposing)
    {
        if (_isDisposed)
            return;

        if (disposing)
            foreach (FlurlClientEx w in Workers)
                w.Dispose();

        _isDisposed = true;

        base.Dispose(disposing);
    }
    #endregion
}