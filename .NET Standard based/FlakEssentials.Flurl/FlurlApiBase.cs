﻿using FEx.Asyncx.Abstractions;
using FEx.Flurlx.Extensions;
using FEx.Json.Extensions;
using Flurl.Http;
using Flurl.Http.Content;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace FlakEssentials.Flurl;

/// <summary>
/// </summary>
/// <typeparam name="TSrv"></typeparam>
/// <remarks>Doesn't require <c>Initialize();</c> call in .ctor</remarks>
public abstract class FlurlApiBase<TSrv> : AsyncInitializable where TSrv : FlurlClientService
{
    protected TSrv FlurlSrv { get; }

    protected FlurlApiBase(TSrv flurlClientService)
        : base(flurlClientService)
    {
        FlurlSrv = flurlClientService;

        BeginInitialization();
    }

    protected static string GetStatusMessage(HttpStatusCode statusCode) => statusCode.ToString();

    protected static bool IsSuccess(HttpStatusCode statusCode) => (int)statusCode >= 200 && (int)statusCode <= 299;

    protected virtual async Task<T> GetResponseAsync<T, TReq>(string apiPath,
                                                              Func<IFlurlRequest, IFlurlRequest> func = null,
                                                              RequestMethod method = RequestMethod.GET,
                                                              TReq requestContent = default,
                                                              CancellationToken cancellationToken = default)
    {
        return await FlurlSrv.RunFlurlFuncAsync(apiPath,
            func,
            cli => GetResponseAsync<T, TReq>(cli, apiPath, func, method, requestContent, cancellationToken));
    }

    protected virtual async Task<T> GetResponseAsync<T, TReq>(IFlurlClient cli,
                                                              string apiPath,
                                                              Func<IFlurlRequest, IFlurlRequest> func = null,
                                                              RequestMethod method = RequestMethod.GET,
                                                              TReq requestContent = default,
                                                              CancellationToken cancellationToken = default)
    {
        IFlurlRequest req = cli.Request(apiPath);

        if (func is not null)
            req = func(req).FixBooleanQueryParameters();

        req = AddConstantsToRequest(req);

        var responseUri = req.Url.ToUri();
        string content;
        HttpStatusCode statusCode;

        switch (method)
        {
            case RequestMethod.GET:
                {
                    (string content, HttpStatusCode statusCode) postRes =
                        await HandleGetRequestAsync(req, cancellationToken);

                    content = postRes.content;
                    statusCode = postRes.statusCode;

                    break;
                }
            case RequestMethod.POST:
                {
                    (string content, HttpStatusCode statusCode) postRes =
                        await HandlePostRequestAsync(req, requestContent, cancellationToken);

                    content = postRes.content;
                    statusCode = postRes.statusCode;

                    break;
                }
            default:
                throw new NotImplementedException($"{method} is not implemented");
        }

        T res = content.FromJson<T>();

        return !IsSuccess(statusCode)
            ? throw new HttpRequestException(
                $"Request {method} {responseUri} has failed. {GetStatusMessage(statusCode)}{Environment.NewLine}{content.PrettyPrintJson()}")
            : res;
    }

    protected virtual async Task<(string content, HttpStatusCode statusCode)> HandleGetRequestAsync(
        IFlurlRequest req,
        CancellationToken cancellationToken = default)
    {
        string content;
        HttpStatusCode statusCode;

        using (IFlurlResponse response = await req.GetAsync(cancellationToken: cancellationToken))
        {
            statusCode = (HttpStatusCode)response.StatusCode;
            content = await response.GetStringAsync();
        }

        return (content, statusCode);
    }

    protected virtual async Task<(string content, HttpStatusCode statusCode)> HandlePostRequestAsync<TReq>(
        IFlurlRequest req,
        TReq requestContent = default,
        CancellationToken cancellationToken = default)
    {
        string content;
        HttpStatusCode statusCode;

        using (var postContent = new CapturedJsonContent(requestContent.ToJson()))
        {
            using IFlurlResponse response = await req.PostAsync(postContent, cancellationToken: cancellationToken);
            statusCode = (HttpStatusCode)response.StatusCode;
            content = await response.GetStringAsync();
        }

        return (content, statusCode);
    }

    protected virtual IFlurlRequest AddConstantsToRequest(IFlurlRequest req) => req;

    protected async Task<T> GetResponseAsync<T>(string apiPath,
                                                Func<IFlurlRequest, IFlurlRequest> func = null,
                                                RequestMethod method = RequestMethod.GET,
                                                CancellationToken cancellationToken = default) =>
        await GetResponseAsync<T, T>(apiPath, func, method, cancellationToken: cancellationToken);
}