﻿using FEx.Asyncx.Services;
using Flurl;
using Flurl.Http;
using Flurl.Http.Configuration;
using System;
using System.Threading.Tasks;

namespace FlakEssentials.Flurl;

public class FlurlClientEx : AsyncWorker<FlurlClientEx, IFlurlResult>
{
    private bool _isDisposed;

    public string TaskId { get; set; }

    protected IFlurlClient FlurlClient { get; private set; }

    public FlurlClientEx(int id, Uri baseUri)
        : base(id)
    {
        FlurlClient = new FlurlClient(baseUri.AbsoluteUri);
    }

    public void Configure(Action<FlurlHttpSettings> action)
    {
        FlurlClient = FlurlClient.WithSettings(action);
    }

    public async Task<IFlurlResult> RunFuncAsync<T>(string id,
                                                    Func<IFlurlClient, Task<T>> func,
                                                    Action<IFlurlClient, T> postAction = null)
    {
        TaskId = id;
        T res = await func(FlurlClient);
        postAction?.Invoke(FlurlClient, res);

        return new FlurlResult<T>(res);
    }

    public Url GetResponseUrl(string path, Func<IFlurlRequest, IFlurlRequest> func) =>
        func(FlurlClient.Request(path)).Url;

    #region IDisposable
    protected override void Dispose(bool disposing)
    {
        if (_isDisposed)
            return;

        if (disposing)
            FlurlClient?.Dispose();

        _isDisposed = true;

        base.Dispose(disposing);
    }
    #endregion
}