﻿using FEx.Extensions.Collections.Dictionaries;
using SQLite;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace FlakEssentials.SQLite
{
    public class DatabaseContext
    {
        public static ConcurrentDictionary<string, string> PersistentDbPaths { get; } = new ConcurrentDictionary<string, string>();
        public static ConcurrentDictionary<string, ReaderWriterLockSlim> DbLocks { get; } = new ConcurrentDictionary<string, ReaderWriterLockSlim>();

        public static (SQLiteConnection, ReaderWriterLockSlim) CreateSQLiteConnection(string persistentDbKey, string databasePath = null, SQLiteOpenFlags? openFlags = null, bool storeDateTimeAsTicks = true)
        {
            if (PersistentDbPaths[persistentDbKey] != null
                && databasePath == null)
                databasePath = PersistentDbPaths[persistentDbKey];

            if (!DbLocks.ContainsKey(databasePath)
                || DbLocks[databasePath] == null)
                DbLocks.AddOrUpdateValue(databasePath, () => new ReaderWriterLockSlim());

            SQLiteConnection conn = openFlags.HasValue
                ? new SQLiteConnection(databasePath, openFlags.Value, storeDateTimeAsTicks)
                : new SQLiteConnection(databasePath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create | SQLiteOpenFlags.FullMutex, storeDateTimeAsTicks);

            conn.BusyTimeout = TimeSpan.FromSeconds(0.2);

            return (conn, DbLocks[persistentDbKey]);
        }

        public static (SQLiteAsyncConnection, ReaderWriterLockSlim) CreateSQLiteAsyncConnection(string persistentDbKey, string databasePath = null, SQLiteOpenFlags? openFlags = null, bool storeDateTimeAsTicks = true)
        {
            if (PersistentDbPaths[persistentDbKey] != null
                && databasePath == null)
                databasePath = PersistentDbPaths[persistentDbKey];

            if (!DbLocks.ContainsKey(persistentDbKey)
                || DbLocks[persistentDbKey] == null)
                DbLocks.AddOrUpdateValue(persistentDbKey, () => new ReaderWriterLockSlim());

            return (openFlags.HasValue
                ? new SQLiteAsyncConnection(databasePath, openFlags.Value, storeDateTimeAsTicks)
                : new SQLiteAsyncConnection(databasePath, storeDateTimeAsTicks), DbLocks[persistentDbKey]);
        }

        public static void SetPersistentDbPath(string dbKey, string dbPath)
        {
            PersistentDbPaths.AddOrUpdateValue(dbKey, () => dbPath);
            DbLocks.AddOrUpdateValue(dbKey, () => new ReaderWriterLockSlim());
        }

        public static void RunInDbContext(string persistentDbKey, Action<SQLiteConnection> action, AccessMode mode, string databasePath = null, SQLiteOpenFlags? openFlags = null, bool storeDateTimeAsTicks = true)
        {
            if (action != null)
            {
                (SQLiteConnection dbContext, ReaderWriterLockSlim rwLock) = CreateSQLiteConnection(persistentDbKey, databasePath, openFlags, storeDateTimeAsTicks);
                using (dbContext)
                    RunLocked(rwLock, mode, () => dbContext.RunInTransaction(() => action(dbContext)));
            }
        }

        public static T RunInDbContext<T>(string persistentDbKey, Func<SQLiteConnection, T> action, AccessMode mode, string databasePath = null, SQLiteOpenFlags? openFlags = null, bool storeDateTimeAsTicks = true)
        {
            T res = default;

            (SQLiteConnection dbContext, ReaderWriterLockSlim rwLock) = CreateSQLiteConnection(persistentDbKey, databasePath, openFlags, storeDateTimeAsTicks);
            using (dbContext)
                RunLocked(rwLock, mode, () => dbContext.RunInTransaction(() => res = action(dbContext)));

            return res;
        }

        public static async Task RunInDbContextAsync(string persistentDbKey, Action<SQLiteAsyncConnection> action, AccessMode mode, string databasePath = null, SQLiteOpenFlags? openFlags = null, bool storeDateTimeAsTicks = true)
        {
            if (action != null)
            {
                (SQLiteAsyncConnection dbContext, ReaderWriterLockSlim rwLock) = CreateSQLiteAsyncConnection(persistentDbKey, databasePath, openFlags, storeDateTimeAsTicks);
                RunLocked(rwLock, mode, () => action(dbContext));
                await dbContext.CloseAsync();
            }
        }

        public static async Task<T> RunInDbContextAsync<T>(string persistentDbKey, Func<SQLiteAsyncConnection, T> action, AccessMode mode, string databasePath = null, SQLiteOpenFlags? openFlags = null, bool storeDateTimeAsTicks = true)
        {
            T res = default;

            if (action != null)
            {
                (SQLiteAsyncConnection dbContext, ReaderWriterLockSlim rwLock) = CreateSQLiteAsyncConnection(persistentDbKey, databasePath, openFlags, storeDateTimeAsTicks);
                RunLocked(rwLock, mode, () => res = action(dbContext));
                await dbContext.CloseAsync();
            }

            return res;
        }

        public static async Task RunInDbContextAsync(string persistentDbKey, Func<SQLiteAsyncConnection, Task> action, AccessMode mode, string databasePath = null, SQLiteOpenFlags? openFlags = null, bool storeDateTimeAsTicks = true)
        {
            if (action != null)
            {
                (SQLiteAsyncConnection dbContext, ReaderWriterLockSlim rwLock) = CreateSQLiteAsyncConnection(persistentDbKey, databasePath, openFlags, storeDateTimeAsTicks);
                await RunFuncLockedAsync(rwLock, mode, () => action(dbContext));
                await dbContext.CloseAsync();
            }
        }

        public static async Task<T> RunInDbContextAsync<T>(string persistentDbKey, Func<SQLiteAsyncConnection, Task<T>> action, AccessMode mode, string databasePath = null, SQLiteOpenFlags? openFlags = null, bool storeDateTimeAsTicks = true)
        {
            T res = default;

            if (action != null)
            {
                (SQLiteAsyncConnection dbContext, ReaderWriterLockSlim rwLock) = CreateSQLiteAsyncConnection(persistentDbKey, databasePath, openFlags, storeDateTimeAsTicks);
                res = await RunFuncLockedAsync(rwLock, mode, () => action(dbContext));
                await dbContext.CloseAsync();
            }

            return res;
        }

        public static int VacuumDb(string persistentDbKey, string databasePath = null, SQLiteOpenFlags? openFlags = null, bool storeDateTimeAsTicks = true)
        {
            (SQLiteConnection dbContext, ReaderWriterLockSlim rwLock) = CreateSQLiteConnection(persistentDbKey, databasePath, openFlags, storeDateTimeAsTicks);
            using (dbContext)
                return RunFuncLocked(rwLock, AccessMode.Write, () => dbContext.Execute("vacuum"));
        }

        private static void RunLocked(ReaderWriterLockSlim rwLock, AccessMode mode, Action action)
        {
            if (mode == AccessMode.None
                || rwLock == null)
            {
                action?.Invoke();
            }
            else
            {
                if (mode == AccessMode.Read)
                    rwLock.EnterReadLock();
                else
                    rwLock.EnterWriteLock();

                try
                {
                    action?.Invoke();
                }
                finally
                {
                    if (mode == AccessMode.Read)
                        rwLock.ExitReadLock();
                    else
                        rwLock.ExitWriteLock();
                }
            }
        }

        private static T RunFuncLocked<T>(ReaderWriterLockSlim rwLock, AccessMode mode, Func<T> action)
        {
            T res = default;

            if (action != null)
            {
                if (mode == AccessMode.None
                    || rwLock == null)
                {
                    res = action();
                }
                else
                {
                    if (mode == AccessMode.Read)
                        rwLock.EnterReadLock();
                    else
                        rwLock.EnterWriteLock();

                    try
                    {
                        res = action();
                    }
                    finally
                    {
                        if (mode == AccessMode.Read)
                            rwLock.ExitReadLock();
                        else
                            rwLock.ExitWriteLock();
                    }
                }
            }

            return res;
        }

        private static async Task RunFuncLockedAsync(ReaderWriterLockSlim rwLock, AccessMode mode, Func<Task> action)
        {
            if (action != null)
            {
                if (mode == AccessMode.None
                    || rwLock == null)
                {
                    await action();
                }
                else
                {
                    if (mode == AccessMode.Read)
                        rwLock.EnterReadLock();
                    else
                        rwLock.EnterWriteLock();

                    try
                    {
                        await action();
                    }
                    finally
                    {
                        if (mode == AccessMode.Read)
                            rwLock.ExitReadLock();
                        else
                            rwLock.ExitWriteLock();
                    }
                }
            }
        }

        private static async Task<T> RunFuncLockedAsync<T>(ReaderWriterLockSlim rwLock, AccessMode mode, Func<Task<T>> action)
        {
            T res = default;

            if (action != null)
            {
                if (mode == AccessMode.None
                    || rwLock == null)
                {
                    res = await action();
                }
                else
                {
                    if (mode == AccessMode.Read)
                        rwLock.EnterReadLock();
                    else
                        rwLock.EnterWriteLock();

                    try
                    {
                        res = await action();
                    }
                    finally
                    {
                        if (mode == AccessMode.Read)
                            rwLock.ExitReadLock();
                        else
                            rwLock.ExitWriteLock();
                    }
                }
            }

            return res;
        }

        public List<T> ExecuteQuery<T>(string persistentDbKey, string query, AccessMode mode, object[] args = null, string databasePath = null, SQLiteOpenFlags? openFlags = null, bool storeDateTimeAsTicks = true)
        {
            (SQLiteConnection dbContext, ReaderWriterLockSlim rwLock) = CreateSQLiteConnection(persistentDbKey, databasePath, openFlags, storeDateTimeAsTicks);
            using (dbContext)
            {
                return RunFuncLocked(rwLock, mode, () =>
                {
                    SQLiteCommand cmd = dbContext.CreateCommand(query, args);
                    return cmd.ExecuteQuery<T>();
                });
            }
        }
    }
}