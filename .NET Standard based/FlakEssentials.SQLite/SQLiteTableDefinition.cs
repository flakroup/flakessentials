﻿using SQLite;

namespace FlakEssentials.SQLite
{
    public class SQLiteTableDefinition
    {
        [Column("sql")]
        public string SQL { get; set; }
    }
}