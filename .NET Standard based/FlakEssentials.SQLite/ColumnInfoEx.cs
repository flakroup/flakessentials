﻿using SQLite;

namespace FlakEssentials.SQLite
{
    [Preserve(AllMembers = true)]
    public class ColumnInfoEx : SQLiteConnection.ColumnInfo
    {
        [Column("cid")]
        public int Cid { get; set; }

        [Column("type")]
        public string ColumnType { get; set; }

        [Column("dflt_value")]
        public string DfltValue { get; set; }

        [Column("pk")]
        public int PK { get; set; }

        [Ignore]
        public bool IsPK => PK != 0;

        [Ignore]
        public bool IsAutoInc { get; set; }

        [Ignore]
        public bool IsNullable => notnull == 0;

        [Ignore]
        public string Collation { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public string GetColumnDeclaration()
        {
            var decl = $@"""{Name}"" {ColumnType} ";

            if (IsPK)
                decl += "primary key ";
            if (IsAutoInc)
                decl += "autoincrement ";
            if (!IsNullable)
                decl += "not null ";
            if (!string.IsNullOrEmpty(Collation))
                decl += "collate " + Collation + " ";

            return decl;
        }
    }
}