﻿using FEx.Extensions;
using FEx.Extensions.Base;
using FEx.Extensions.Collections.Dictionaries;
using FEx.Extensions.Collections.Enumerables;
using FEx.Extensions.Web;
using FEx.Rx.BaseObjects;
using FlakEssentials.Download;
using FlakEssentials.Extensions.Helpers;
using FlakEssentials.WebEx;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace FlakEssentials.SQLite
{
    public class DatabaseBackedService : ReactiveNotifyPropertyChanged, IDisposable
    {
        public string DbKey { get; protected set; }
        public Task InitializationTask { get; protected set; }
        protected Uri RemoteDbCopy { get; set; }
        protected Uri RemoteDbCopyMD5 { get; set; }
        protected SemaphoreSlim InitializationSemaphore { get; set; }
        protected bool IsInitialized { get; set; }
        protected string PersistentDbPath => DatabaseContext.PersistentDbPaths.TryGetKeyValue(DbKey);

        public virtual void Dispose()
        {
            InitializationTask?.Dispose();
            InitializationSemaphore?.Dispose();
        }

        public static void SetPersistentDbPath(string dbKey, string dbPath)
        {
            DatabaseContext.SetPersistentDbPath(dbKey, dbPath);
        }

        public virtual string GetCheckSum()
        {
            try
            {
                return RunInDbContext(db => db.GetTableInfo(nameof(Checksum))
                                                .Count
                                            > 0
                    ? db.Table<Checksum>()
                        .FirstOrDefault()
                        ?.MD5
                    : null, AccessMode.Read);
            }
            catch
            {
                //ignored
            }

            return null;
        }

        public virtual void RunInDbContext(Action<SQLiteConnection> action, AccessMode mode, string databasePath = null, SQLiteOpenFlags? openFlags = null, bool storeDateTimeAsTicks = true)
        {
            if (action != null
                && (DbKey != null || databasePath != null))
                DatabaseContext.RunInDbContext(DbKey, action, mode, databasePath, openFlags, storeDateTimeAsTicks);
        }

        public virtual T RunInDbContext<T>(Func<SQLiteConnection, T> action, AccessMode mode, string databasePath = null, SQLiteOpenFlags? openFlags = null, bool storeDateTimeAsTicks = true)
        {
            return DatabaseContext.RunInDbContext(DbKey, action, mode, databasePath, openFlags, storeDateTimeAsTicks);
        }

        public virtual async Task RunInDbContextAsync(Action<SQLiteAsyncConnection> action, AccessMode mode, string databasePath = null, SQLiteOpenFlags? openFlags = null, bool storeDateTimeAsTicks = true)
        {
            if (action != null
                && (DbKey != null || databasePath != null))
                await DatabaseContext.RunInDbContextAsync(DbKey, action, mode, databasePath, openFlags, storeDateTimeAsTicks);
        }

        public virtual async Task<T> RunInDbContextAsync<T>(Func<SQLiteAsyncConnection, T> action, AccessMode mode, string databasePath = null, SQLiteOpenFlags? openFlags = null, bool storeDateTimeAsTicks = true)
        {
            T res = default;

            if (action != null
                && (DbKey != null || databasePath != null))
                res = await DatabaseContext.RunInDbContextAsync(DbKey, action, mode, databasePath, openFlags, storeDateTimeAsTicks);

            return res;
        }

        public virtual int VacuumDb(string databasePath = null, SQLiteOpenFlags? openFlags = null, bool storeDateTimeAsTicks = true)
        {
            if (DbKey != null
                && (DbKey != null || databasePath != null))
                return DatabaseContext.VacuumDb(DbKey, databasePath, openFlags, storeDateTimeAsTicks);

            return 0;
        }

        public List<ColumnInfoEx> GetTableInfo<T>()
        {
            var query = $@"pragma table_info(""{typeof(T).Name}"")";
            var aIQuery = $"SELECT sql FROM sqlite_master WHERE tbl_name='{typeof(T).Name}'";

            return RunInDbContext(db =>
            {
                List<ColumnInfoEx> info = db.Query<ColumnInfoEx>(query);
                List<SQLiteTableDefinition> tableDef = db.Query<SQLiteTableDefinition>(aIQuery);
                string[] tableCreation = tableDef[0]
                    .SQL.Split('\n');

                foreach (ColumnInfoEx c in info)
                {
                    string sqlColumnStatement = tableCreation.FindInEnumerable(x => x.StartsWith($"\"{c.Name}\" "));

                    if (sqlColumnStatement != null)
                    {
                        if (c.IsPK)
                            c.IsAutoInc = sqlColumnStatement.Contains(" autoincrement ", StringComparison.OrdinalIgnoreCase);

                        if (sqlColumnStatement.Contains(" COLLATE ", StringComparison.OrdinalIgnoreCase))
                        {
                            string[] parts = sqlColumnStatement.Split(' ');

                            c.Collation = parts[parts.IndexWhere(x => x.Equals("COLLATE", StringComparison.OrdinalIgnoreCase)) + 1];
                        }
                    }
                }

                return info;
            }, AccessMode.Read);
        }

        /// <summary>
        ///     Ensures that table schema matches exactly correlated type.
        /// </summary>
        /// <typeparam name="T">Mapped type</typeparam>
        /// <returns>True if table has been dropped and recreated.</returns>
        public bool EnsureTableMigration<T>()
        {
            var sourceColumns = GetTableInfo<T>()
                .ToDictionary(x => x.Name, x => x.GetColumnDeclaration());

            bool res = RunInDbContext(db =>
            {
                TableMapping map = db.GetMapping<T>();
                var targetColumns = map.Columns.ToDictionary(x => x.Name, x => Orm.SqlDecl(x, db.StoreDateTimeAsTicks, db.StoreTimeSpanAsTicks));
                var matched = targetColumns.ToDictionary(x => x.Value, x => sourceColumns.TryGetKeyValue(x.Key));

                if (matched.Any(x => x.Key != x.Value))
                {
                    db.DropTable<T>();
                    db.CreateTable<T>();

                    return true;
                }

                return false;
            }, AccessMode.Write);

            if (res)
                VacuumDb();

            return res;
        }

        protected virtual async Task<bool> InitializeDbAsync(bool useSemaphore = true, bool refresh = false)
        {
            if (useSemaphore)
                await InitializationSemaphore.WaitAsync();

            var result = false;

            try
            {
                if (PersistentDbPath != null)
                    if (refresh || await DbShouldBeDownloadedAsync())
                    {
                        string targetDirectoryPath = Path.GetDirectoryName(DatabaseContext.PersistentDbPaths[DbKey]) ?? throw new ArgumentException("Path cannot be null");
                        string targetPath = Path.Combine(targetDirectoryPath, RemoteDbCopy.Segments.Last());

                        var retryCount = 3;

                        while (retryCount > 0)
                        {
                            try
                            {
                                using WebResponse res = await RemoteDbCopy.GetHttpRequest()
                                    .GetResponseAsync();

                                using var response = (HttpWebResponse)res;

                                using var di = DownloadItem.CreateFromResponse(response, targetPath, false);

                                await di.DownloadFileAsync();
                            }
                            catch (Exception ex)
                            {
                                ex.HandleException();
                            }

                            try
                            {
                                if (Path.GetExtension(targetPath) == ".zip"
                                    && File.Exists(targetPath))
                                {
                                    CompressionHelper.DecompressZip(targetPath, targetDirectoryPath, true);

                                    if (File.Exists(DatabaseContext.PersistentDbPaths[DbKey]))
                                        File.Delete(targetPath);
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.HandleException();
                            }

                            result = File.Exists(DatabaseContext.PersistentDbPaths[DbKey]) && !await DbShouldBeDownloadedAsync();

                            if (result)
                                break;

                            retryCount--;
                        }
                    }
            }
            finally
            {
                if (useSemaphore)
                    InitializationSemaphore.Release();
            }

            return result;
        }

        protected virtual async Task EnsureCacheAsync()
        {
            if (!IsInitialized)
            {
                await InitializationSemaphore.WaitAsync();
                InitializationSemaphore.Release();
            }
        }

        protected void SetChecksum()
        {
            VacuumDb();
            string dbFileMD5 = FileSystemCommon.GenerateMd5OfFile(DatabaseContext.PersistentDbPaths[DbKey]);
            RunInDbContext(db =>
            {
                db.DeleteAll<Checksum>();
                db.Insert(new Checksum { MD5 = dbFileMD5 });
            }, AccessMode.Write);
        }

        private async Task<bool> DbShouldBeDownloadedAsync()
        {
            if (RemoteDbCopy != null)
            {
                string dbPath = DatabaseContext.PersistentDbPaths[DbKey];

                if (!File.Exists(dbPath))
                    return true;

                if (RemoteDbCopyMD5 != null)
                {
                    string dbFile = Path.GetFileName(dbPath);
                    string cSum = await WebExCommon.DownloadStringAsync(RemoteDbCopyMD5);
                    string md5 = cSum.Split('\n')
                        .Select(x => x.Trim()
                            .Split(' ')
                            .Where(y => y != string.Empty)
                            .ToArray())
                        .FirstOrDefault(x => x[0] == dbFile)?[1]
                        ?.ToLower();

                    if (md5 != null)
                    {
                        string dbFileMD5 = GetCheckSum();

                        if (dbFileMD5 != md5)
                            return true;
                    }
                }
            }

            return false;
        }
    }
}