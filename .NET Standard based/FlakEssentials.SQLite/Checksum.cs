﻿using SQLite;

namespace FlakEssentials.SQLite
{
    public class Checksum
    {
        [PrimaryKey]
        public string MD5 { get; set; }
    }
}