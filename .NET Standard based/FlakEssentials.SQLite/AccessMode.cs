﻿namespace FlakEssentials.SQLite
{
    public enum AccessMode
    {
        None,
        Read,
        Write
    }
}