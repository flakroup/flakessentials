﻿using System.Net;

namespace FlakEssentials.WebEx;

public class WebModule
{
    public static int DefaultConnectionLimit { get; private set; }

    static WebModule()
    {
        DefaultConnectionLimit = ServicePointManager.DefaultConnectionLimit;
    }

    public static void SetDefaultConnectionLimit(int limit = 2)
    {
        DefaultConnectionLimit = limit;
        ServicePointManager.DefaultConnectionLimit = DefaultConnectionLimit;
    }
}