﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace FlakEssentials.WebEx;

public static class WebExCommon
{
    public static async Task<string> DownloadStringAsync(Uri url)
    {
        using var a = new HttpClient();

        return await a.GetStringAsync(url);
    }
}