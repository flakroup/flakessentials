﻿using StrongInject;

namespace FlakEssentials.NuGetEx;

public interface INuGetExModule : IContainer<NuGetEx>, IContainer<NuGetManager>,
    IContainer<NuGetLogger<NuGetManager>>
{
}