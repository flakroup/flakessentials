﻿using FEx.DI.Abstractions.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using StrongInject;
using StrongInject.Extensions.DependencyInjection;

namespace FlakEssentials.NuGetEx;

[Register(typeof(NuGetManager), Scope.SingleInstance)]
[Register(typeof(NuGetLogger<NuGetManager>), Scope.SingleInstance)]
[Register(typeof(NuGetEx), Scope.SingleInstance, typeof(NuGetEx), typeof(IInitializeModule))]
public class NuGetExModule
{
    public static void AddServices(INuGetExModule container, IServiceCollection services)
    {
        services.AddSingletonServiceUsingContainer<NuGetManager>(container);
        services.AddSingletonServiceUsingContainer<NuGetLogger<NuGetManager>>(container);
    }
}