﻿using FEx.DI.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace FlakEssentials.NuGetEx;

public class NuGetEx : InitializeModule<INuGetExModule>
{
    protected override void AddServices(INuGetExModule container, IServiceCollection services) =>
        NuGetExModule.AddServices(container, services);
}