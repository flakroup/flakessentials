﻿using FEx.Abstractions.Models;
using FEx.Basics.Extensions;
using FEx.Downloader.Clients;
using FEx.Extensions;
using FEx.Extensions.Base.Converters;
using FEx.Extensions.Base.Enums;
using FEx.Extensions.Collections;
using FEx.Extensions.Collections.Lists;
using FEx.Extensions.Web;
using FEx.MVVM;
using FEx.MVVM.Abstractions.Interfaces;
using FEx.MVVM.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace FlakEssentials.Web.Services;

/// <summary>
///     Class with common things related to web handling.
/// </summary>
public static class WebCommon
{
    private static int _retryCount;

    public static string StatusDescription { get; set; }

    /// <summary>
    ///     Gets the local ip addresses.
    /// </summary>
    /// <returns></returns>
    public static Dictionary<NetworkInterfaceType, HashSet<string>> GetAllLocalIPv4(bool ommitLoopbacks = true)
    {
        var dictionary = new Dictionary<NetworkInterfaceType, HashSet<string>>();

        foreach (NetworkInterface item in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (item.OperationalStatus == OperationalStatus.Up
                && (!ommitLoopbacks || item.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                && item.NetworkInterfaceType != NetworkInterfaceType.Tunnel)
            {
                if (!dictionary.ContainsKey(item.NetworkInterfaceType))
                    dictionary.Add(item.NetworkInterfaceType,
                    [
                        ..item.GetIPProperties()
                            .UnicastAddresses.Where(x => x.Address.AddressFamily == AddressFamily.InterNetwork)
                            .Select(x => x.Address.ToString())
                    ]);
                else
                    dictionary[item.NetworkInterfaceType]
                        .AddRangeToCollection(item.GetIPProperties()
                            .UnicastAddresses.Where(x => x.Address.AddressFamily == AddressFamily.InterNetwork)
                            .Select(x => x.Address.ToString()));
            }
        }

        return dictionary;
    }

    /// <summary>
    ///     Downloads the string.
    /// </summary>
    /// <param name="url">The URL.</param>
    /// <param name="retry">The retry.</param>
    /// <returns></returns>
    public static async Task<string> DownloadStringAsync(string url, int retry = 20) =>
        await DownloadStringAsync(new Uri(url), retry);

    /// <summary>
    ///     Downloads the string.
    /// </summary>
    /// <param name="url">The URL.</param>
    /// <param name="retry">The retry.</param>
    /// <returns>
    ///     System.String.
    /// </returns>
    public static async Task<string> DownloadStringAsync(Uri url, int retry = 20)
    {
        retry++;
        var res = new List<Exception>();

        while (retry > 0)
        {
            try
            {
                using var a = new HttpClient();

                return await a.GetStringAsync(url);
            }
            catch (Exception ex)
            {
                res.Add(ex);

                if (FlakWebClient.Unreachable.Contains(url.AbsolutePath))
                    retry = 0;
            }

            retry--;
        }

        return res.Count > 0
            ? throw new AggregateException(res)
            : null;
    }

    public static async Task<bool> DownloadFileAsync(string fileName,
                                                     Uri serverUri,
                                                     IProgressAggregator viewModel,
                                                     string username = "",
                                                     string password = "")
    {
        var res = false;

        try
        {
            long offset = 0;

            while (!res)
            {
                if (File.Exists(fileName))
                    offset = new FileInfo(fileName).Length;

                try
                {
                    long offset1 = offset;

                    res = await RestartDownloadFromServerAsync(fileName,
                        serverUri,
                        viewModel,
                        offset1,
                        username,
                        password);
                }
                catch (Exception ex)
                {
                    //ex.HandleException( "", false);
                    viewModel?.IfNotNull(v => v.SetStatusInfo(ex.Message));
                }

                Thread.Sleep(1000);
            }
        }
        catch (Exception ex)
        {
            //ex.HandleException();
            viewModel?.IfNotNull(v => v.SetStatusInfo(ex.Message));
            res = false;
        }

        return res;
    }

    /// <summary>
    ///     Restarts the download from server.
    /// </summary>
    /// <param name="fileName">Name of the file. Identifies the local file.</param>
    /// <param name="serverUri">The server URI. Identifies the remote file.</param>
    /// <param name="viewModel">The view model.</param>
    /// <param name="offset">The offset. Specifies where in the server file to start reading data.</param>
    /// <param name="username">The username.</param>
    /// <param name="password">The password.</param>
    /// <returns></returns>
    public static async Task<bool> RestartDownloadFromServerAsync(string fileName,
                                                                  Uri serverUri,
                                                                  IProgressAggregator viewModel,
                                                                  long offset = 0,
                                                                  string username = "",
                                                                  string password = "")
    {
        if (serverUri.Scheme == Uri.UriSchemeFtp)
        {
            viewModel?.IfNotNull(v => v.SetStatusInfo(fileName));
            var fileSize = (long)await CalculateSizeAsync(serverUri, false, LengthType.Bytes, username, password);
            long localFileSize;

            if (File.Exists(fileName))
            {
                localFileSize = new FileInfo(fileName).Length;

                if (localFileSize >= fileSize)
                    return true;
            }

            KeyValuePair<bool, FtpWebResponse> resp = await TryGetResponseAsync(serverUri, username, password, offset);
            FtpWebResponse response = resp.Value;
            using Stream stream = response.GetResponseStream();
            viewModel?.PrgSetMax(fileSize - offset);
            viewModel?.IfNotNull(v => v.SetIsIndeterminate(true));

            FileMode mode = File.Exists(fileName)
                ? FileMode.Append
                : FileMode.CreateNew;

            using var fs = new FileStream(fileName, mode);
            viewModel?.IfNotNull(v => v.SetStatusInfo($"Downloading: {fileName} "));

            try
            {
                if (stream is not null)
                {
                    var readCount = 1;
                    long prg = 0;
                    viewModel?.SetCurrentDownloadState(prg, fileSize - offset);
                    //viewModel?.ThreadsInfo = $"{(prg + offset) / (double)fileSize * 100}%";
                    const int cacheLength = 1024 * 1024 / 2;
                    var sw = new Stopwatch();

                    while (readCount > 0)
                    {
                        var cache = new List<byte>();
                        var pos = 0;

                        try
                        {
                            //viewModel?.ProgressIsIndeterminate = true;
                            byte[] buffer;
                            sw.Restart();

                            while (readCount > 0
                                   && pos < cacheLength)
                            {
                                buffer = new byte[1];
                                readCount = await stream.ReadAsync(buffer, 0, buffer.Length);
                                _retryCount = 0;

                                if (readCount > 0)
                                {
                                    cache.AddRange(buffer);
                                    pos += readCount;
                                    prg += readCount;

                                    if (prg % 10 == 0)
                                        viewModel?.PrgSet(prg);
                                }
                            }

                            sw.Stop();
                        }
                        catch (Exception ex)
                        {
                            //ex.HandleException( "", false);
                            viewModel?.IfNotNull(v => v.SetStatusInfo(ex.Message));

                            if (cache.IsNotEmpty())
                                await fs.WriteAsync([.. cache], 0, cache.Count);

                            var webEx = ex as WebException;

                            if (webEx is not null)
                            {
                                var ftpResponse = (FtpWebResponse)webEx.Response;

                                if (ftpResponse.StatusCode == FtpStatusCode.ActionAbortedLocalProcessingError)
                                {
                                    if (_retryCount >= 10)
                                    {
                                        //byte[] buffer = new byte[1];
                                        //fs.Write(buffer, 0, buffer.Length);
                                        //Common.LogIt($"{fileName} byte at position {offset + prg + 1}  replaced with 0 due to {retryCount} unsuccessfull read attempts.\n", false);
                                        _retryCount = 0;

                                        long newOffset = await DetectOffsetAsync(serverUri,
                                            offset + prg,
                                            username,
                                            password,
                                            viewModel);

                                        var buffer = new byte[newOffset - (offset + prg)];
                                        await fs.WriteAsync(buffer, 0, buffer.Length);

                                        await FExMvvm.MessagePopupService.ShowMessageAsync(
                                            $"{fileName} bytes at position {offset + prg + 1}-{newOffset} replaced with 0 due to {_retryCount} unsuccessfull read attempts.\n",
                                            wait: false);
                                    }
                                    else
                                    {
                                        _retryCount++;
                                    }
                                }
                            }

                            break;
                        }

                        await fs.WriteAsync([.. cache], 0, cache.Count);
                        viewModel?.PrgSet(prg);
                        viewModel?.SetCurrentDownloadState(prg, fileSize - offset);
                        //viewModel?.ThreadsInfo = $"{(prg + offset) / (double)fileSize * 100}% {sw.GetTime()}";
                    }
                }
            }
            catch (Exception ex)
            {
                //ex.HandleException( "", false);
                viewModel?.IfNotNull(v => v.SetStatusInfo(ex.Message));
            }

            viewModel?.IfNotNull(v => v.SetIsIndeterminate(true));
            StatusDescription = response.StatusDescription;
            fs.Close();
            stream?.Close();
            localFileSize = new FileInfo(fileName).Length;

            return File.Exists(fileName) && localFileSize >= fileSize;
        }

        return false;
    }

    /// <summary>
    ///     Calculates the size.
    /// </summary>
    /// <param name="serverUri">The server URI.</param>
    /// <param name="promptOnError">if set to <c>true</c> [prompt on error].</param>
    /// <param name="unit">The unit.</param>
    /// <param name="username">The username.</param>
    /// <param name="password">The password.</param>
    /// <returns></returns>
    public static async Task<double> CalculateSizeAsync(Uri serverUri,
                                                        bool promptOnError = true,
                                                        LengthType unit = LengthType.Megabytes,
                                                        string username = "",
                                                        string password = "")
    {
        double bytesTotal = 0;

        try
        {
            if (serverUri.Scheme == Uri.UriSchemeHttp
                || serverUri.Scheme == Uri.UriSchemeHttps)
            {
                using var wc = new FlakWebClient();
                wc.OpenRead(serverUri);
                bytesTotal = Convert.ToInt64(wc.ResponseHeaders["Content-Length"]);
            }
            else if (serverUri.Scheme == Uri.UriSchemeFtp)
            {
                var request = (FtpWebRequest)serverUri.GetWebRequest();
                request.Proxy = null;
                request.Credentials = GetCredentials(username, password);
                request.Method = WebRequestMethods.Ftp.GetFileSize;

                using var response = (FtpWebResponse)await request.GetResponseAsync();
                bytesTotal = response.ContentLength;
            }
        }
        catch (Exception ex)
        {
            ex.HandleException(promptOnError);
        }

        return unit == LengthType.Bytes
            ? bytesTotal
            : FileLengthConverter.ConvertFileLength(bytesTotal, LengthType.Bytes, unit).length;
    }

    public static ICredentials GetCredentials(string username = "", string password = "") =>
        username.IsNotNullOrWhiteSpace() && password.IsNotNullOrWhiteSpace()
            ? new NetworkCredential(username, password)
            : (ICredentials)CredentialCache.DefaultNetworkCredentials;

    public static HttpWebRequest GetHttpRequest(string url, WebRequestParams pars = null) =>
        new Uri(url).GetHttpRequest(pars);

    public static WebRequest GetWebRequest(string url, WebRequestParams pars = null) =>
        new Uri(url).GetWebRequest(pars);

    public static CookieContainer GetCookieContainer(IEnumerable<Cookie> cookies)
    {
        var cookieContainer = new CookieContainer();

        if (cookies is not null)
            foreach (Cookie c in cookies)
                cookieContainer.Add(c);

        return cookieContainer;
    }

    private static async Task<KeyValuePair<bool, FtpWebResponse>> TryGetResponseAsync(
        Uri serverUri,
        string username,
        string password,
        long offset)
    {
        // Get the object used to communicate with the server.
        var request = (FtpWebRequest)serverUri.GetWebRequest();
        request.Method = WebRequestMethods.Ftp.DownloadFile;
        request.Credentials = GetCredentials(username, password);
        request.ContentOffset = offset;
        FtpWebResponse response = null;

        try
        {
            response = (FtpWebResponse)await request.GetResponseAsync();

            return new(true, response);
        }
        catch (Exception ex)
        {
            ex.HandleException();
        }

        return new(false, response);
    }

    private static async Task<long> DetectOffsetAsync(Uri serverUri,
                                                      long offset,
                                                      string username,
                                                      string password,
                                                      IProgressAggregator viewModel)
    {
        long newOffset = offset;

        if (serverUri.Scheme == Uri.UriSchemeFtp)
        {
            var fileSize = (long)await CalculateSizeAsync(serverUri, false, LengthType.Bytes, username, password);
            viewModel?.PrgSetMax(fileSize - offset);
            var readCount = 0;

            while (readCount <= 0
                   && newOffset < fileSize)
            {
                KeyValuePair<bool, FtpWebResponse> resp =
                    await TryGetResponseAsync(serverUri, username, password, offset);

                FtpWebResponse response = resp.Value;

                if (!resp.Key)
                    return offset;

                try
                {
                    Stream stream = response.GetResponseStream();

                    try
                    {
                        if (stream is not null)
                        {
                            var buffer = new byte[1];
                            readCount = await stream.ReadAsync(buffer, 0, buffer.Length);

                            try
                            {
                                stream.Close();
                            }
                            catch
                            {
                                //
                            }

                            newOffset--;

                            try
                            {
                                while (readCount > 0)
                                {
                                    resp = await TryGetResponseAsync(serverUri, username, password, offset);
                                    response = resp.Value;

                                    if (!resp.Key)
                                        return newOffset;

                                    stream = response.GetResponseStream();

                                    if (stream is not null)
                                    {
                                        buffer = new byte[1];
                                        readCount = await stream.ReadAsync(buffer, 0, buffer.Length);
                                        viewModel?.PrgSet(newOffset - offset);
                                        viewModel?.SetCurrentDownloadState(newOffset - offset, fileSize - offset);
                                        //viewModel.ThreadsInfo = $"{newOffset / (double)fileSize * 100}%";
                                        newOffset--;
                                    }
                                }
                            }
                            catch
                            {
                                try
                                {
                                    stream?.Close();
                                }
                                catch
                                {
                                    //
                                }

                                return newOffset;
                            }
                        }
                    }
                    catch
                    {
                        //ex.HandleException( "", false);
                        //ViewModel.=ex.Message StatusInfo;
                        //WebException webEx = ex as WebException;
                        //if (webEx is not null)
                        //{
                        //    FtpWebResponse ftpResponse = (FtpWebResponse)webEx.Response;
                        //    if (ftpResponse.StatusCode == FtpStatusCode.ActionAbortedLocalProcessingError)
                        //    {

                        //    }
                        //}
                    }

                    stream?.Close();
                }
                catch
                {
                    //
                }

                viewModel?.PrgSet(newOffset - offset);
                viewModel?.SetCurrentDownloadState(newOffset - offset, fileSize - offset);

                //viewModel.ThreadsInfo = $"{newOffset / (double)fileSize * 100}%";
                if (readCount <= 0)
                    newOffset += 1024 * 1024 / 2;
            }
        }

        return newOffset;
    }
}