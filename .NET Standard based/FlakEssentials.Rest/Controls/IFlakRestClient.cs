﻿using RestSharp;
using System.Threading.Tasks;

namespace FlakEssentials.Rest.Controls;

public interface IFlakRestClient : IRestClient
{
    bool IsBusy { get; }
    Task SetBusyAsync(bool value);
}