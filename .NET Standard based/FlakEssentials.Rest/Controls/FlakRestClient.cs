﻿using RestSharp;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FlakEssentials.Rest.Controls;

public class FlakRestClient : RestClient, IFlakRestClient, IDisposable
{
    public bool IsBusy { get; protected set; }

    protected SemaphoreSlim Semaphore { get; }

    public FlakRestClient()
    {
        Semaphore = new SemaphoreSlim(1, 1);
    }

    public FlakRestClient(Uri baseUrl)
        : base(baseUrl)
    {
        Semaphore = new SemaphoreSlim(1, 1);
    }

    public FlakRestClient(string baseUrl)
        : base(baseUrl)
    {
        Semaphore = new SemaphoreSlim(1, 1);
    }

    public async Task SetBusyAsync(bool value)
    {
        await Semaphore.WaitAsync();
        IsBusy = value;
        Semaphore.Release();
    }

    [Obsolete("This method will be removed soon in favour of the proper async call")]
    public override RestRequestAsyncHandle ExecuteAsync(IRestRequest request,
                                                        Action<IRestResponse, RestRequestAsyncHandle> callback)
    {
        RestRequestAsyncHandle restRequestAsyncHandle = base.ExecuteAsync(request, callback);

        if (restRequestAsyncHandle?.WebRequest is not null)
            restRequestAsyncHandle.WebRequest.AllowWriteStreamBuffering = false;

        return restRequestAsyncHandle;
    }

    #region IDisposable
    public void Dispose()
    {
        Semaphore?.Dispose();
    }
    #endregion
}