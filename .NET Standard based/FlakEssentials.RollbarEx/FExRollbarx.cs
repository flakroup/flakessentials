﻿using FEx.Common.Extensions;
using FEx.DI.Abstractions;
using FEx.Telemetry.Subjects;
using FlakEssentials.RollbarEx.Abstractions.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace FlakEssentials.RollbarEx;

public class FExRollbarx : InitializeModule<IFExRollbarxModule>
{
    private static IRollbarService _rollbarSrv;
    private static TelemetryAccessTokenSubject _telemetryAccessTokenSubject;

    public static IRollbarService RollbarSrv
    {
        get => _rollbarSrv.Guard();
        private set => _rollbarSrv = value.Guard(nameof(value));
    }

    public static TelemetryAccessTokenSubject TelemetryAccessTokenSubject
    {
        get => _telemetryAccessTokenSubject.Guard();
        private set => _telemetryAccessTokenSubject = value.Guard(nameof(value));
    }

    public FExRollbarx(IRollbarService rollbarSrv, TelemetryAccessTokenSubject telemetryAccessTokenSubject)
    {
        RollbarSrv = rollbarSrv;
        TelemetryAccessTokenSubject = telemetryAccessTokenSubject;
    }

    protected override void AddServices(IFExRollbarxModule container, IServiceCollection services) =>
        FExRollbarxModule.AddServices(container, services);
}