﻿using FEx.Telemetry;
using StrongInject;
using System.Diagnostics.CodeAnalysis;

namespace FlakEssentials.RollbarEx.Abstractions.Interfaces;

[SuppressMessage("ReSharper", "PossibleInterfaceMemberAmbiguity")]
public interface IFExRollbarxModule : IFExTelemetryModule, IContainer<IRollbarService>, IContainer<IFExRollbarConfig>,
    IContainer<FExRollbarx>
{
}