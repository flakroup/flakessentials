﻿using FEx.Telemetry;
using Rollbar;
using System;
using System.IO;

namespace FlakEssentials.RollbarEx.Abstractions.Interfaces;

public interface IFExRollbarConfig : IFExTelemetryConfig
{
    DirectoryInfo LocalAppDataDir { get; }
    FileInfo DefaultRollbarStoreDbFile { get; }
    Version AppVersion { get; }
    RollbarInfrastructureConfig RollbarConfig { get; }
    bool IsConfigured { get; }
}