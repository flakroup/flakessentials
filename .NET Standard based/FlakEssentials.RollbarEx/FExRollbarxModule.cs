﻿using FEx.DI.Abstractions.Interfaces;
using FEx.Telemetry;
using FlakEssentials.RollbarEx.Abstractions.Interfaces;
using FlakEssentials.RollbarEx.Services;
using Microsoft.Extensions.DependencyInjection;
using StrongInject;
using StrongInject.Extensions.DependencyInjection;

namespace FlakEssentials.RollbarEx;

[Register(typeof(RollbarService), Scope.SingleInstance, typeof(IRollbarService))]
[Register(typeof(FExRollbarx), Scope.SingleInstance, typeof(FExRollbarx), typeof(IInitializeModule))]
public class FExRollbarxModule : FExTelemetryModule
{
    public static void AddServices(IFExRollbarxModule container, IServiceCollection services)
    {
        services.AddSingletonServiceUsingContainer<IRollbarService>(container);
        services.AddSingletonServiceUsingContainer<IFExRollbarConfig>(container);
        services.AddSingletonServiceUsingContainer<FExRollbarx>(container);
    }
}