﻿using FEx.Basics.Extensions;
using FEx.Common.Extensions;
using FEx.Extensions.Collections.Lists;
using FlakEssentials.Extensions.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;

namespace FlakEssentials.CommonUtilities.Extensions;

public static class FileSystemExtensions
{
    public static bool SetEverybodyFullControl(this DirectoryInfo dInfo, params string[] excludes)
    {
        dInfo.Create();
        SecurityIdentifier user;

        using (var current = WindowsIdentity.GetCurrent())
            user = current.User;

        return EnsureAccess(dInfo, new(WellKnownSidType.WorldSid, null), user, excludes);
    }

    private static bool EnsureAccess(DirectoryInfo dInfo,
                                     SecurityIdentifier sid,
                                     SecurityIdentifier cuSid,
                                     params string[] excludes)
    {
        var account = (NTAccount)sid.Translate(typeof(NTAccount));
        var cuAccount = (NTAccount)cuSid.Translate(typeof(NTAccount));
        bool shouldRun = CheckAccess(dInfo, account, cuAccount, excludes);

        if (!shouldRun)
            return true;

        string everyone = account.Value;
        var argsA = $"icacls \"{dInfo.FullName}\" /T /C /setowner {everyone}";
        var argsB = $"icacls \"{dInfo.FullName}\" /grant {everyone}:(OI)(CI)F /T";
        bool isSuccess;

        using (var c = new Cmd())
        {
            c.Run(argsA);
            isSuccess = c.ErrOut.ToString().IsNullOrEmptyString() && c.Code == 0;

            if (isSuccess)
            {
                c.Run(argsB);
                isSuccess = c.ErrOut.ToString().IsNullOrEmptyString() && c.Code == 0;
            }
        }

        if (!isSuccess)
        {
            using var c = new ElevatedCmd();

            try
            {
                RunIcacls(argsA, c);
                RunIcacls(argsB, c);
                //isSuccess = true;
            }
            catch (Exception ex)
            {
                //isSuccess = false;
                ex.HandleException();
            }
        }

        return !CheckAccess(dInfo, account, cuAccount, excludes);
    }

    private static bool CheckAccess(DirectoryInfo dInfo,
                                    NTAccount everyoneAccount,
                                    NTAccount userAccount,
                                    params string[] excludes)
    {
        var args =
            $"Get-ChildItem \'{dInfo.FullName}\' -Recurse | % {{ $path1 = $_.fullname; Get-Acl $_.Fullname}} |  % {{$owner =$_.Owner; ($_.access.IdentityReference | %{{((\'{everyoneAccount.Value}\' -like $_) -or (\'{userAccount.Value}\' -like $_))}}) -contains $true}}| %{{$path1+'|'+$_+'|'+(($owner -like \'{everyoneAccount.Value}\') -or ($owner -like \'{userAccount.Value}\'))}}";

        string errOut;
        string output;

        using (var c = new Cmd("powershell"))
        {
            c.Run(args);
            errOut = c.ErrOut.ToString().Trim();
            output = c.Output.ToString().Trim();
        }

        if (errOut.IsNotNullOrEmptyString())
            return false;

        var wrongOutput = false;
        ACL[] access = null;

        string[] raw = output.IsNotNullOrEmptyString()
            ? output.Split('\n')
            : null;

        HashSet<string> excluded = null;

        if (excludes.IsNotNullOrEmptyList())
            try
            {
                excluded =
                [
                    ..excludes.SelectMany(e => dInfo.GetFileSystemInfos(e, SearchOption.AllDirectories))
                        .Select(x => x.FullName)
                ];
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }

        if (raw.IsNotNullOrEmptyList())
            access = raw.AsParallel()
                .Select(x =>
                {
                    string[] splitted = x.Trim().Split('|');

                    try
                    {
                        if (excludes.IsNullOrEmptyList()
                            || excluded?.Contains(splitted[0]) != true)
                            return new ACL(splitted);
                    }
                    catch
                    {
                        wrongOutput = true;
                    }

                    return null;
                })
                .Where(x => x is not null)
                .ToArray();

        return wrongOutput || (access?.Any(x => x?.HasEveryoneAccess != true || !x.HasEveryoneOwner) ?? false);
    }

    private static void RunIcacls(string argsA, ElevatedCmd c)
    {
        c.Run(argsA);
        string[] lines = c.Output.ToString().Trim().Split('\n');
        string last = lines.Last();
        string failed = last.Split(';')[1];
        var failedCount = int.Parse(failed.Trim().Split(' ')[2]);

        if (c.Code != 0
            || failedCount != 0)
            throw new($"Command has failed: {last}");
    }
}