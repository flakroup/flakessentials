﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FlakEssentials.CommonUtilities.Extensions.Abstractions;

public interface ILoggerState : IDictionary<string, object>, IDictionary, IReadOnlyDictionary<string, object>,
    ISerializable, IDeserializationCallback
{
    void AddOrUpdateLabel(string key, object value);
    void RemoveLabel(string key);
}